#!/bin/bash
#
 
DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages

/bin/systemctl --user kill iperfs
/usr/bin/systemd-run --user --on-calendar=daily --unit=iperfs iperf -s
journalctl -xfu iperfs > /tmp/iperf_server.txt &
 
