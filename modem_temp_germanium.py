#!/usr/bin/python
import argparse
import rrdtool
import syslog
import sys, os
import platform
import time
import psutil
from datetime import datetime
from xvfbwrapper import Xvfb 
from pyvirtualdisplay import Display
from germanium.static import *

network_val = '0'
network_upt_val = '0'
data_link_val   = '0'
satellite_rx_val= '0'
satellite_tx_val= '0'
temperature_val = '0'
uptime_val      = "0"

cpu = '0'
downlink_freq_val = '0'
symbol_rate_val = '0'
rolloff = '0'
beam_id = '0'
rx_comp_pwr_val = '0'
rx_snr_val = '0'
tx_pwr_val = '0'

#---------------------------------------
def zero_lvals():
#---------------------------------------
    network_val = '0'
    network_upt_val = '0'
    data_link_val   = '0'
    satellite_rx_val= '0'
    satellite_tx_val= '0'
    temperature_val = '0'
    uptime_val      = "0"

    cpu = '0'
    downlink_freq_val = '0'
    symbol_rate_val = '0'
    rolloff = '0'
    beam_id = '0'
    rx_comp_pwr_val = '0'
    rx_snr_val = '0'
    tx_pwr_val = '0'

    lvals = [ network_val, network_upt_val, data_link_val, satellite_rx_val,
          satellite_tx_val, temperature_val, uptime_val,
          cpu, downlink_freq_val, symbol_rate_val, rolloff, beam_id,
          rx_comp_pwr_val, rx_snr_val, tx_pwr_val ]
    return lvals

#---------------------------------------
def print_resp(s):
#---------------------------------------
    print (s.body())
    print( "- - - - - - - - - - - - - - - - - - - - - - - ")
    soup = BeautifulSoup( s.body(), "lxml" )
    print( soup.find(id="intro-text") )
    print( "----------------------------------------------")
    print( " ")

#---------------------------------------
def log_rrd(lvals):
#---------------------------------------
    if args.syslog:
        syslog.closelog()

    # IF we have not done an open_browser() yet, then calling close_browser() 
    # throws an exception. However, if we have not done an open_browser() yet,
    # then get_germanium() will return 'None', so we can use it as a check to
    # see if we need to call close_browser()
    if get_germanium():
        close_browser()

    if 'vdisplay' in vars() or 'vdisplay' in globals():
        if vdisplay:
            vdisplay.stop()

    tmpl = 'network:network_upt:data_link:satellite_rx:satellite_tx:temperature:uptime'
    tmpl = tmpl +':cpu:downlink_freq:symbol_rate:rolloff:beam_id:rx_comp_pwr:rx_snr:tx_pwr'

    vals = ':'.join(lvals)
    vals = 'N:' +str(vals)

    if args.verbose:
        print( 'tmpl = ' +tmpl)
        print( 'vals = ' +vals)
        print('rrdtool update Start')
    rrdtool.update(filename, '--template', tmpl, vals)

    if args.verbose:
        print('rrdtool update Done')

#---------------------------------------
def print_time_deltas(a):
#---------------------------------------
    n=0
    for t in a:
        d = t-a[0]
        print(str(n) +" " +str(d.total_seconds()) +" sec")
        n += 1

#---------------------------------------
def wait_el(element, timeout):
    """Implement a timeout on top of germanium's wait(), because anything
       other than wait(element, timeout=10) breaks germanium on the Pi.
    """
#---------------------------------------
    start = datetime.now()
    now = start
    dt = (now - start)
    success = False

    while dt.total_seconds() < timeout:
        try:
            wait(element)
        except:
            # If an exception was thrown, it was a timeout, so continue in our loop
            now = datetime.now()
            dt = (now - start)
        else:
            # If an exception was NOT thrown, then we successfully found the element
            success = True
            break;

    now = datetime.now()
    dt = (now - start)
    if args.verbose:
        print( "wait_el waited " +str(dt.total_seconds()) +" secs")
    return success


#machine = platform.machine()

#url="https://192.168.44.1/#status-modem"
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--ip_addr', dest='ip', action='store', default="192.168.44.1",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-f', '--filename', dest='filename', action='store', default="kmodem_status.rrd",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-b', '--browser', dest='browser', action='store', default="ff",
        help='browser to use [chrome|ff] default: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-d', '--disable_vdisplay', dest='use_vdisplay', action='store_false', default=True, 
        help='Default is to use a virtual display, so the browser is NOT shown. use this option to show the browser interactions.')
parser.add_argument('-t', '--timeout', dest='timeout', action='store', type=int, default=45,
        help="sets 'page_load' and 'script' timeouts in germanium. default: %(default)s")
parser.add_argument('-S', '--sleep', dest='sleep', action='store', type=int, default=0,
        help="Sleep time between 'login' press, and check for data. default: %(default)s")
parser.add_argument('-s', '--skip_syslog', dest='syslog', action='store_false', default=True,
        help='Disable logging to syslog')
parser.add_argument('-z', '--force_zero', dest='force_zero', action='store_true', default=False,
        help='update the RRD database with some all zero data')
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')

args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

if args.syslog:
    syslog.openlog('modem_temp_germanium')

script_name = sys.argv[0].split('/')[-1]
#script_name = script_name[0:15]
if args.verbose:
    print "script_name = " +script_name 
for pid in psutil.pids():
    try:
        p = psutil.Process(pid)
    except psutil.NoSuchProcess:
        if args.verbose:
            print("Note: caught NoSuchProcess exception for pid " +str(pid) )
        continue

    #print p.name()
    #print str(p)
    #if p.name() == "python.exe":
    #    print("Called By Python:"+ str(p.cmdline()) )
    if p.name() in script_name:
        if args.verbose:
            print ("Found python script")
            print str(p)
        if os.getpid() == p.pid:
            if args.verbose:
                print("Found myself: " +str(p.cmdline()) )
        else:
            msg = "Found existing " +script_name +" (pid = " +str(p.pid) +"), skipping this one (" +str(os.getpid()) +")"
            if args.verbose:
                print(msg)
            if args.syslog:
                syslog.syslog(msg)
                syslog.closelog()
            sys.exit()

#sys.exit()        #### DEBUG ##########################################################

url = "http://" +args.ip
filename = os.path.join(args.work_dir, args.filename)

############### DEBUG ################
#args.verbose = 1
#args.use_vdisplay = False
#args.timeout=45
#args.syslog = True
#args.browser = 'f'

if args.verbose:
    print("modem_temp_germanium: url = " +url)
    print("use_vdisplay = " +str(args.use_vdisplay))
    print("filename = " +filename)

if args.force_zero:
    log_rrd( zero_lvals() )
    sys.exit()


user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
usernameStr = 'admin'
#passwordStr = 'P@55w0rd!'
passwordStr = 'iDirect'



vdisplay = None
if args.use_vdisplay:
    if args.browser[0] == "c":
        if args.verbose:
            print "starting Xvfb"
        vdisplay = Xvfb()
        vdisplay.start()
    elif args.browser[0] == 'f':
        if args.verbose:
            print "starting pyvirtualdisplay for firefox"
        #vdisplay = Display(visible=0, size=(800, 600))
        vdisplay = Display()
        vdisplay.start()
    elif args.browser[0] == 'p':
        if args.verbose:
            print "starting pyvirtualdisplay for phantomjs"
        vdisplay = Display(visible=0, size=(800, 600))
        vdisplay.start()

t1 = datetime.now()
open_browser(args.browser)
go_to(url)
wait(Button('Log In'), timeout=args.timeout)
type_keys(usernameStr, Input("user"))
type_keys(passwordStr, Input("password"))
t2 = datetime.now()
click(Button('Log In'))
#close_browser()

#<html lang="en-US">
#  <head>
#     <meta http-equiv="content-type" content="text/html; charset=utf-8">
#  </head>
#  <body style="width:100%;height:100%;margin:0;background-color:#717074;" onload="document.getElementById('user').focus()">
#  <center>
#     <h1 style="padding-top:55px;color:#D0D2D8;">Login</h1>
#     <form method="post" action="/authorize">
#         <input type="hidden" name="success" value="/">
#         <input type="hidden" name="fail" value="/login.html">
#         <table><tbody><tr><td style="padding:20px;background-color:white;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;">
#            <table><tbody>
#               <tr>
#                  <td><strong>User:</strong></td>
#                  <td><input id="user" type="text" name="user"></td>
#               </tr><tr>
#                  <td style="padding-right:1em;"><strong>Password:</strong></td>
#                  <td><input type="password" name="password"></td>
#               </tr><tr>
#                  <td></td>
#                  <td style="padding-top:10px;text-align:right;">
#                  <input type="submit" value="Login"></td>
#               </tr>
#            </tbody></table></td></tr>
#         </tbody></table>
#     </form></center>
#  </body>
#</html>

time.sleep(args.sleep)
pf = wait_el(Text("Temperature"), args.timeout)
if not pf:
    log_rrd( zero_lvals() )
    sys.exit()

pf = wait_el( Element('div', id="Dashboard-temperature"), args.timeout )
if not pf:
    log_rrd( zero_lvals() )
    sys.exit()
#<html lang="en-US">
#<head><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>
#<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
#<title>Web iSite</title>
#<link rel="stylesheet" href="js/app/main.css"/>
#    <script src="js/dojo/dojo.js"></script>
#    <script src="js/app/base.js"></script>
#    <noscript> This page uses Javascript. Your browser either doesn't support Javascript or you have it turned off. To see this page as it is meant to appear please use a Javascript enabled browser. </noscript>
#    </head><body class="claro"><div id="appLayout"></div></body>
#</html>

#---------------------------------------
def get_id_value(s):
#---------------------------------------
    a=Element('input', id=s)
    if not a.exists():
        print ("could not find element " +s)
        return ""
    e = a.element()
    v = e.get_attribute("value")
    return v

#---------------------------------------
def get_id_text(s,t='div'):
#---------------------------------------
    a=Element(t, id=s)
    if not a.exists():
        print ("could not find element " +s)
        return ""
    e = a.element()
    v = str(e.text)
    return v

temperature = get_id_text("Dashboard-temperature")

cpu         = get_id_text("Dashboard-cpu-load", "led-field-wrapper")
memory      = get_id_text("Dashboard-memory-use", "led-field-wrapper")
network     = get_id_text("Dashboard-network-status")
satellite_rx= get_id_text("Dashboard-satellite-receiver")
satellite_tx= get_id_text("Dashboard-satellite-transmitter")
rx_snr      = get_id_text("Dashboard-receive-snr")

#network_upt = get_id_value("dijit_form_TextBox_8")
#data_link   = get_id_value("dijit_form_TextBox_9")
#uptime      = get_id_value("dijit_form_TextBox_15")
network_upt='0'
data_link = '0'
uptime = '0'

if args.verbose:
    print ("network: " +network)
    print ("cpu: " +cpu)
    print ("memory: " +memory)
    print ("satellite_rx: " +satellite_rx)
    print ("satellite_tx: " +satellite_tx)
    print ("temperature: " +temperature)
    print ("rx_snr: " +rx_snr)
    #print ("network_upt: " +network_upt)
    #print ("data_link: " +data_link)
    #print ("uptime: " +uptime)


#sys.exit()
try:
    click(Link('View Satellite Details'))
except Exception as ex:
    print("Exception has been thrown. " + str(ex))
    cpu = '0'
    downlink_freq_val = '0'
    symbol_rate_val = '0'
    #rolloff = '0'
    beam_id = '0'
    rx_comp_pwr_val = '0'
    rx_snr_val = '0'
    tx_pwr_val = '0'

    lvals = [ network_val, network_upt_val, data_link_val, satellite_rx_val,
          satellite_tx_val, temperature_val, uptime_val,
          cpu, downlink_freq_val, symbol_rate_val, rolloff, beam_id,
          rx_comp_pwr_val, rx_snr_val, tx_pwr_val ]

    syslog.syslog( "timeout" )
    log_rrd(lvals)
    sys.exit()


wait_el( Element('div', id="satellite-interface-beam-id-rc1"), args.timeout )

downlink_freq = get_id_text("satellite-interface-downlink-center-frequency-rc1") # in MHz
symbol_rate = get_id_text("satellite-interface-symbol-rate-rc1") # in ksym/s
beam_id = get_id_text("satellite-interface-beam-id-rc1")
rx_comp_pwr = get_id_text("satellite-interface-receive-composite-power-rc1") # in dBm
#rx_snr = get_id_value("dijit_form_TextBox_42") # in dB
tx_pwr = get_id_text("satellite-interface-power-relative-to-the-nominal-carrier") # in dBm

#rolloff = get_id_value("dijit_form_TextBox_32").split('%')[0] # in %
rolloff = '0'

if args.verbose:
    #print ("network: " +network)
    #print ("network_upt: " +network_upt)
    #print ("data_link: " +data_link)
    #print ("satellite_rx: " +satellite_rx)
    #print ("satellite_tx: " +satellite_tx)
    #print ("temperature: " +temperature)
    #print ("uptime: " +uptime)
    #print ("cpu: " +cpu)
    print ("downlink_freq: " +downlink_freq)
    print ("symbol_rate : " +symbol_rate )
    #print ("rolloff: " +rolloff)
    print ("beam_id: " +beam_id)
    print ("rx_comp_pwr: " +rx_comp_pwr)
    print ("tx_pwr: " +tx_pwr)

# network: In Network
# network_upt: 02:29:42
# data_link: Established
# satellite_rx: Locked
# satellite_tx: On
# temperature: 49 C
# uptime: 1 day, 00:54:23

#---------------------------------------
def binary_val(needle, haystack):
#---------------------------------------
    if len(haystack) == 0:
        return '0'
    if 'N/A' in haystack:
        return '0'
    if needle in haystack:
        return '1'
    return '0'

#---------------------------------------
def time_to_sec(tstr):
#---------------------------------------
    if len(tstr) == 0:
        return '0'
    if 'N/A' in tstr:
        return '0'
    tsplit = tstr.split()
    if len(tsplit) == 3:
        days_as_sec = int(tsplit[0]) * 60 * 60 * 24
        hms = tsplit[2]
    else:
        days_as_sec = 0
        hms = tsplit[0]
    hmsplit=hms.split(':')
    val = int(hmsplit[2]) + 60 * int(hmsplit[1]) + 3600 * int(hmsplit[0]) + days_as_sec
    return str(val)

#---------------------------------------
def nonezero(s, all=False):
#---------------------------------------
    if len(s) == 0:
        s = '0'
    elif not all:
        s = s.split()[0]

    if 'N/A' in s:
        s = '0'
    if '-' == s:
        s = '0'
    return s

#---------------------------------------
def strdiv(s, v):
#---------------------------------------
    return str(float(s) / v)


network_val = binary_val('IN_NETWORK', network)
#network_upt_val = time_to_sec(network_upt)
network_upt_val = '0'
data_link_val = binary_val('Established', data_link)
satellite_rx_val = binary_val('Locked', satellite_rx)
satellite_tx_val = binary_val('UNMUTED', satellite_tx)
temperature_val = nonezero(temperature)
#uptime_val = time_to_sec(uptime)
uptime_val = '0'

symbol_rate_val = nonezero(symbol_rate)
symbol_rate_val = strdiv(symbol_rate_val, 1e3)

downlink_freq_val = nonezero(downlink_freq, True).split()[5] # Now in MHz  
downlink_freq_val = strdiv(downlink_freq_val, 1e3) # Now in GHz

rx_comp_pwr_val = nonezero(rx_comp_pwr)
rx_snr_val = nonezero(rx_snr)
tx_pwr_val = nonezero(tx_pwr)
 
lvals = [ network_val, network_upt_val, data_link_val, satellite_rx_val,
          satellite_tx_val, temperature_val, uptime_val,
          cpu, downlink_freq_val, symbol_rate_val, rolloff, beam_id,
          rx_comp_pwr_val, rx_snr_val, tx_pwr_val ]

log_rrd(lvals)

