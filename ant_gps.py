#!/usr/bin/python
import argparse
import sys, os
import requests
import time
import os


parser = argparse.ArgumentParser()
parser.add_argument('-i', '--ip_addr', dest='ip', action='store', default="192.168.44.2",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-f', '--filename', dest='filename', action='store', default="kant_gps.csv",
        help='Filename for status data: %(default)s')
parser.add_argument('-s', '--sleep_time', dest='sleep_time', action='store', default=1,
        help='delay between collecting GPS results: %(default)s seconds')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-T', '--filename_tracking', dest='filename_tracking', action='store', default="kant_tracking.rrd",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-t', '--timeout', dest='timeout', action='store', type=int, default=5,
        help="sets 'page_load' and 'script' timeouts in selenium. default: %(default)s")
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')
args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

filename = os.path.join(args.work_dir, args.filename)
header = "Hour, Minute, Second, latitude, longitude, altitude, gps_fix, gps_fix_quality, status, message"
if os.path.isfile(filename):
    f = open(filename, 'a')
else:
    f = open(filename, 'w')
    f.write(header)

url = "http://" +args.ip
if args.verbose:
    print("url = " +url)
    print( "filename = " +filename)

s = requests.Session()
s.auth = ('admin', '2Cfg^Ant')
s.verify = False
#s.headers.update({'x-test': 'true'})


while True:
    tstr = time.strftime('%H,%M,%S,', time.localtime())
    if args.verbose:
        print( 'tstr = ' +tstr )

    try:
        #curl --user user:pass -X GET --header "Accept: application/json" "http://192.168.44.2/v1/status/position"
        r = s.get(url +"/v1/status/position", verify=False, headers={'Accept':'application/json'}, timeout=args.timeout)
    except:
        if args.verbose:
            print( "s.get(url) error - probably timeout?"); 
        vals = tstr +'0,0,0,0,0,0,0'
        f.write(vals + "\n")
        #sys.exit()

    if args.verbose:
        print( 'r.json() = ' +str(r.json()) )

    # r.json() = {u'status': u'ok', u'gps_fix_quality': 1, 
    #             u'altitude': 1689.3, u'gps_fix': 3, 
    #             u'longitude': -105.11586833333334, u'latitude': 39.89725166666666, 
    #             u'message': u''}
    j = r.json()
    if 'ok' in str(j['status']):
        status = '1'
    else:
        status = '0'
    lvals = [ "{0:.15f}".format(j['latitude']), "{0:.15f}".format(j['longitude']),
              "{0:.1f}".format(j['altitude']),    str(j['gps_fix']), str(j['gps_fix_quality']), 
              status, str(j['message']) ]
    vals = tstr + ','.join(lvals)
    print( vals)
    if args.verbose:
        print( "\n" )

    f.write(vals +"\n")

    time.sleep(args.sleep_time)

f.close
