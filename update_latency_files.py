#!/usr/bin/python
#
#
import argparse
import sys, os
import rrdtool
#import subprocess as sp
#import wget
import requests
import time
import datetime
import syslog

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--ip_addr', dest='ip', action='store',
        help='IP address of Kymeta antenna default: %(default)s',
        default="54.186.205.190")
parser.add_argument('-f', '--filename', dest='filename', action='store',
        default='latency_files.rrd',
        help='Filename for rrd database: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-r', '--remotenames', dest='remotenames', action='store',
        default=['1k.dat', '100k.dat' ],
        type=str, nargs='+',
        help='Filename for status data: %(default)s')
#default=['1k.dat', '10k.dat', '100k.dat', '1M.dat' ],
parser.add_argument('-d', '--dir', dest='dir', action='store', default="kymeta/latency_files",
        help='Remote directory for files: %(default)s')
#parser.add_argument('-t', '--tmpdir', dest='tmpdir', action='store', default="/tmp/",
#        help='tmp directory for outputs: %(default)s')
parser.add_argument('-s', '--skip_syslog', dest='syslog', action='store_false', default=True,
        help='Disable logging to syslog')

choices=['dt', 're']
parser.add_argument('--dt_method', dest='dt_method', action='store', default="dt",
        choices=choices,
        help='Use datetime calls(dt) or requests.elapsed (ee) for calculating download time. Default: %(default)s')
# Note: elapsed measures the time between sending the request and finishing parsing the response headers, not until the full response has been transfered
#       (from :https://stackoverflow.com/questions/30442757/measuring-the-http-response-time-with-requests-library-in-python-am-i-doing-it)

parser.add_argument('-t', '--timeout', dest='timeout', action='store', type=int, default=5,
        help="sets 'page_load' and 'script' timeouts in selenium. default: %(default)s")
parser.add_argument('-z', '--force_zero', dest='force_zero', action='store_true', default=False,
        help='update the RRD database with some all zero data')
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')
args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

###### DEBUG #############
#args.verbose = True

if args.ip == "lucifer":
    args.ip = "10.8.246.142"
elif args.ip == "kant":
    args.ip = "192.168.44.2"
elif args.ip == "kmodem":
    args.ip = "192.168.44.1"
elif args.ip == "aws":
    args.ip = "54.186.205.190"

filename = os.path.join(args.work_dir, args.filename)
if args.verbose:
    print( "filename = " +filename)

if args.syslog:
    syslog.openlog('update_latency_files')

#tmpl = '1k:10k:100k:1M'
tmpl = '1k:100k'

if args.force_zero:
    vals = "0:0"
    if args.syslog:
        syslog.syslog( "forcing zero value update" )
    if args.verbose:
        print ( "update_latency_files forcing zero value update" )

else:
    lvals = []
    for s in args.remotenames:
        err = 0
        url = 'http://' +args.ip +'/' +args.dir +'/' +s
        if args.verbose:
            print( "url = " +url )

        start = time.time()
        try:
            r = requests.get(url, timeout=args.timeout)
        except Exception as ex:
            msg = "Exception has been thrown. " + str(ex)
            print( msg )
            if args.syslog:
                syslog.syslog( msg )
            err = 1
        end = time.time()
        if err:
            delta = 0
        else:
            delta = end - start

        dt = datetime.timedelta(seconds=float(delta))
        if args.dt_method == "dt":
            lvals.append( str(dt.total_seconds()) )
        else:
            lvals.append( str(r.elapsed.total_seconds()) )
        if args.verbose:
            print( "dt = " +str(dt.total_seconds()) )
            print( "re = " +str(r.elapsed.total_seconds()) )
            print( "" )

    vals = ':'.join(lvals)

if args.verbose:
    print ("vals = " +str(vals))
vals = 'N:' +vals
rrdtool.update(filename, '--template', tmpl, vals)

if args.syslog:
    syslog.closelog()

