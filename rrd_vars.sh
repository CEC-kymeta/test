#!/bin/bash
#
gawk="/usr/bin/gawk"
rrdtool="/usr/bin/rrdtool"
logger="/usr/bin/logger"
kmodem="192.168.44.1"
kant="192.168.44.2"
aws_iperf="54.186.205.190"
htmldir="/var/www/html"

# A list of all *.rrd files. Used by rrd_dump.sh and rrd_restore.sh
declare -a rrd_list=("aws_ping.rrd" "iperf.rrd" \
                     "latency_files.rrd" \
                     "kmodem_ping.rrd" "kmodem_status.rrd" \
                     "kant_ping.rrd" "kant_status.rrd" "kant_tracking.rrd" \
                     "psmon.rrd" "uptime.rrd" "pi_temp.rrd" \
                     "weather.rrd")

