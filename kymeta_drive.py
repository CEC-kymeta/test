#!/usr/bin/python
import argparse
import sys, os
import requests
import time
import sys, os
import platform
from datetime import datetime
import psutil                             # sudo apt install python-psutil
from xvfbwrapper import Xvfb              # sudo apt install python-xvfbwrapper
from pyvirtualdisplay import Display      # pip install pyvirtualdisplay 
from germanium.static import *            # pip install germanium

#---------------------------------------
def wait_el(element, timeout):
    """Implement a timeout on top of germanium's wait(), because anything
       other than wait(element, timeout=10) breaks germanium on the Pi.
    """
#---------------------------------------
    start = datetime.now()
    now = start
    dt = (now - start)
    success = False

    while dt.total_seconds() < timeout:
        try:
            wait(element)
        except:
            # If an exception was thrown, it was a timeout, so continue in our loop
            now = datetime.now()
            dt = (now - start)
        else:
            # If an exception was NOT thrown, then we successfully found the element
            success = True
            break;

    now = datetime.now()
    dt = (now - start)
    if args.verbose:
        print( "wait_el waited " +str(dt.total_seconds()) +" secs")
    return success

#---------------------------------------
def get_id_value(s):
#---------------------------------------
    a=Element('input', id=s)
    if not a.exists():
        print ("could not find element " +s)
        return ""
    e = a.element()
    v = e.get_attribute("value")
    return v

#---------------------------------------
def get_id_text(s,t='div'):
#---------------------------------------
    a=Element(t, id=s)
    if not a.exists():
        print ("could not find element " +s)
        return ""
    e = a.element()
    v = str(e.text)
    return v

#---------------------------------------
def browser_cleanup():
#---------------------------------------
    # IF we have not done an open_browser() yet, then calling close_browser() 
    # throws an exception. However, if we have not done an open_browser() yet,
    # then get_germanium() will return 'None', so we can use it as a check to
    # see if we need to call close_browser()
    if get_germanium():
        close_browser()

parser = argparse.ArgumentParser()
parser.add_argument('--modem_ip_addr', dest='modem_ip', action='store', default="192.168.44.1",
        help='IP address of Kymeta modem default: %(default)s')
parser.add_argument('--ant_ip_addr', dest='ant_ip', action='store', default="192.168.44.2",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-f', '--filename', dest='filename', action='store', default="kdrive.csv",
        help='Filename for status data: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-s', '--sleep_time', dest='sleep_time', action='store', default=1,
        help='delay between collecting results: %(default)s seconds')
parser.add_argument('-z', '--force_zero', dest='force_zero', action='store_true', default=False,
        help='update the RRD database with some all zero data')

parser.add_argument('-b', '--browser', dest='browser', action='store', default="chrome",
        help='browser to use [chrome|ff] default: %(default)s')
parser.add_argument('-t', '--timeout', dest='timeout', action='store', type=int, default=45,
        help="sets 'page_load' and 'script' timeouts in germanium. default: %(default)s")
parser.add_argument('-d', '--disable_vdisplay', dest='use_vdisplay', action='store_false', default=True, 
        help='Default is to use a virtual display, so the browser is NOT shown. use this option to show the browser interactions.')
parser.add_argument('-S', '--sleep', dest='sleep', action='store', type=int, default=0,
        help="Sleep time between 'login' press, and check for data. default: %(default)s")

parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')







args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

filename = os.path.join(args.work_dir, args.filename)
header  = "Hour, Minute, Second, latitude, longitude, altitude, gps_fix, gps_fix_quality, status, "
header += "RX_SNR, RX_status, TX_status, network, Sat_Longitude"
if os.path.isfile(filename):
    f = open(filename, 'a')
else:
    f = open(filename, 'w')
    f.write(header)

ant_url = "http://" +args.ant_ip +"/v1/status/position"
ant_url2 = "http://" +args.ant_ip +"/v1/setup/satellite"
modem_url = "http://" +args.modem_ip
if args.verbose:
    print("ant_url = " +ant_url)
    print("modem_url = " +modem_url)
    print("use_vdisplay = " +str(args.use_vdisplay))
    print( "filename = " +filename)

s = requests.Session()
s.auth = ('admin', '2Cfg^Ant')
s.verify = False
#s.headers.update({'x-test': 'true'})


user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'
usernameStr = 'admin'
#passwordStr = 'P@55w0rd!'
passwordStr = 'iDirect'


vdisplay = None
if args.use_vdisplay:
    if args.browser[0] == "c":
        if args.verbose:
            print "starting Xvfb"
        vdisplay = Xvfb()
        vdisplay.start()
    elif args.browser[0] == 'f':
        if args.verbose:
            print "starting pyvirtualdisplay for firefox"
        #vdisplay = Display(visible=0, size=(800, 600))
        vdisplay = Display()
        vdisplay.start()
    elif args.browser[0] == 'p':
        if args.verbose:
            print "starting pyvirtualdisplay for phantomjs"
        vdisplay = Display(visible=0, size=(800, 600))
        vdisplay.start()

t1 = datetime.now()
open_browser(args.browser)
go_to(modem_url)
wait(Button('Log In'), timeout=args.timeout)
type_keys(usernameStr, Input("user"))
type_keys(passwordStr, Input("password"))
t2 = datetime.now()
click(Button('Log In'))
#<html lang="en-US">
#  <head>
#     <meta http-equiv="content-type" content="text/html; charset=utf-8">
#  </head>
#  <body style="width:100%;height:100%;margin:0;background-color:#717074;" onload="document.getElementById('user').focus()">
#  <center>
#     <h1 style="padding-top:55px;color:#D0D2D8;">Login</h1>
#     <form method="post" action="/authorize">
#         <input type="hidden" name="success" value="/">
#         <input type="hidden" name="fail" value="/login.html">
#         <table><tbody><tr><td style="padding:20px;background-color:white;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;">
#            <table><tbody>
#               <tr>
#                  <td><strong>User:</strong></td>
#                  <td><input id="user" type="text" name="user"></td>
#               </tr><tr>
#                  <td style="padding-right:1em;"><strong>Password:</strong></td>
#                  <td><input type="password" name="password"></td>
#               </tr><tr>
#                  <td></td>
#                  <td style="padding-top:10px;text-align:right;">
#                  <input type="submit" value="Login"></td>
#               </tr>
#            </tbody></table></td></tr>
#         </tbody></table>
#     </form></center>
#  </body>
#</html>

time.sleep(args.sleep)
pf = wait_el(Text("Temperature"), args.timeout)
if not pf:
    browser_cleanup()
    sys.exit()

pf = wait_el( Element('div', id="Dashboard-temperature"), args.timeout )
if not pf:
    browser_cleanup()
    sys.exit()
#<html lang="en-US">
#<head><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>
#<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
#<title>Web iSite</title>
#<link rel="stylesheet" href="js/app/main.css"/>
#    <script src="js/dojo/dojo.js"></script>
#    <script src="js/app/base.js"></script>
#    <noscript> This page uses Javascript. Your browser either doesn't support Javascript or you have it turned off. To see this page as it is meant to appear please use a Javascript enabled browser. </noscript>
#    </head><body class="claro"><div id="appLayout"></div></body>
#</html>

print(header)
first_time=True
while True:
    #--------------------------------
    # Collect Modem data
    #--------------------------------
    if not first_time:
        driver.refresh()
    time.sleep(args.sleep)
    pf = wait_el(Text("Temperature"), args.timeout)
    if not pf:
        browser_cleanup()
        sys.exit()

    pf = wait_el( Element('div', id="Dashboard-temperature"), args.timeout )
    if not pf:
        browser_cleanup()
        sys.exit()
    #<html lang="en-US">
    #<head><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8"/>
    #<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    #<title>Web iSite</title>
    #<link rel="stylesheet" href="js/app/main.css"/>
    #    <script src="js/dojo/dojo.js"></script>
    #    <script src="js/app/base.js"></script>
    #    <noscript> This page uses Javascript. Your browser either doesn't support Javascript or you have it turned off. To see this page as it is meant to appear please use a Javascript enabled browser. </noscript>
    #    </head><body class="claro"><div id="appLayout"></div></body>
    #</html>
    temperature = get_id_text("Dashboard-temperature")

    cpu         = get_id_text("Dashboard-cpu-load", "led-field-wrapper")
    memory      = get_id_text("Dashboard-memory-use", "led-field-wrapper")
    network     = get_id_text("Dashboard-network-status")
    satellite_rx= get_id_text("Dashboard-satellite-receiver")
    satellite_tx= get_id_text("Dashboard-satellite-transmitter")
    rx_snr      = get_id_text("Dashboard-receive-snr")
    if args.verbose:
        print ("network: " +network)
        print ("cpu: " +cpu)
        print ("memory: " +memory)
        print ("satellite_rx: " +satellite_rx)
        print ("satellite_tx: " +satellite_tx)
        print ("temperature: " +temperature)
        print ("rx_snr: " +rx_snr)
    modem_lvals = [rx_snr, satellite_rx, satellite_tx, network]

    #--------------------------------
    # Create time stamp
    #--------------------------------
    tstr = time.strftime('%H,%M,%S,', time.localtime())
    if args.verbose:
        print( 'tstr = ' +tstr )

    #--------------------------------
    # Get Antenna (ASM) data
    #--------------------------------
    # TODO: http://192.168.44.2/v1/setup/point?select=RX
    # {
    #   "lpa": 239.23837280273438,
    #   "message": "",
    #   "phi": 224.67919921875,
    #   "select": "RX",
    #   "status": "ok",
    #   "theta": 30.184886932373047
    # }
    try:
        #curl --user user:pass -X GET --header "Accept: application/json" "http://192.168.44.2/v1/status/position"
        r = s.get(ant_url, verify=False, headers={'Accept':'application/json'}, timeout=args.timeout)

    except:
        if args.verbose:
            print( "s.get(ant_url) error - probably timeout?"); 
        ant_lvals = '0,0,0,0,0,0,0'

    else:
        if args.verbose:
            print( 'r.json() = ' +str(r.json()) )

        # r.json() = {u'status': u'ok', u'gps_fix_quality': 1, 
        #             u'altitude': 1689.3, u'gps_fix': 3, 
        #             u'longitude': -105.11586833333334, u'latitude': 39.89725166666666, 
        #             u'message': u''}
        j = r.json()
        if 'ok' in str(j['status']):
            status = '1'
        else:
            status = '0'
        ant_lvals = [ "{0:.15f}".format(j['latitude']), "{0:.15f}".format(j['longitude']),
                      "{0:.1f}".format(j['altitude']),    str(j['gps_fix']), str(j['gps_fix_quality']), 
                      status, str(j['message']) ]
    try:
        r = s.get(ant_url2, verify=False, headers={'Accept':'application/json'}, timeout=args.timeout)
    except:
        if args.verbose:
            print( "s.get(ant_url2) error - probably timeout?"); 
        sat_longitude = 0
    else:
        j = r.json()
        # r.json() = {u'longitude': 273, u'message': u'', 
        #             u'polarization_rx': "V", u'polarization_tx': "H", 
        #             u'status': u'ok'}
        if args.verbose:
            print( 'r.json() = ' +str(r.json()) )
        sat_longitude = j['longitude']
    #ant_lvals.append(sat_longitude)

    #--------------------------------
    # Write Data to file
    #--------------------------------
    vals = tstr +','.join(ant_lvals) +','.join(modem_lvals) +',' +str(sat_longitude)
    print( vals)
    if args.verbose:
        print( "\n" )
    f.write(vals +"\n")

    time.sleep(args.sleep_time)

f.close
