# From: https://calomel.org/rrdtool.html

### change to the script directory
#cd /tools/rrdtool/latency/

# http://rrdwizard.appspot.com/rrdcreate.php?

file=${1:-'all'}
steps="2m"
stepm="5m"
stepl="20m"
hbs="4m"
hbm="10m"
hbl="20m"

read -d '' rra_max_min_avg <<- \EOF
   RRA:MAX:0.5:1:3d 
   RRA:MAX:0.5:1h:14d 
   RRA:MAX:0.5:1d:90d 
   RRA:MIN:0.5:1:3d 
   RRA:MIN:0.5:1h:14d 
   RRA:MIN:0.5:1d:90d 
   RRA:AVERAGE:0.5:1:3d 
   RRA:AVERAGE:0.5:1h:14d 
   RRA:AVERAGE:0.5:1d:90d 
EOF

if [ "$file" == "all" -o "$file" == "kmodem_ping" ]; then
   echo "initializing kmodem_ping.rrd"
   rrdtool create kmodem_ping.rrd \
      --step $steps \
      DS:pl:GAUGE:$hbs:0:100 \
      DS:rtt:GAUGE:$hbs:0:10000000 \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "pi_temp" ]; then
   echo "initializing pi_temp.rrd"
   rrdtool create pi_temp.rrd \
      --step $steps \
      DS:temperature:GAUGE:$hbs:-275:500  \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "kmodem_status" ]; then
   echo "initializing kmodem_status.rrd"
   rrdtool create kmodem_status.rrd \
      --step $stepl \
      DS:network:GAUGE:$hbl:0:1 \
      DS:network_upt:GAUGE:$hbl:0:U  \
      DS:data_link:GAUGE:$hbl:0:1 \
      DS:satellite_rx:GAUGE:$hbl:0:1 \
      DS:satellite_tx:GAUGE:$hbl:0:1 \
      DS:temperature:GAUGE:$hbl:-275:500  \
      DS:uptime:GAUGE:$hbl:0:U \
      DS:cpu:GAUGE:$hbl:0:100  \
      DS:downlink_freq:GAUGE:$hbl:0:100000  \
      DS:symbol_rate:GAUGE:$hbl:0:100000  \
      DS:rolloff:GAUGE:$hbl:0:100  \
      DS:beam_id:GAUGE:$hbl:0:100  \
      DS:rx_comp_pwr:GAUGE:$hbl:-200:200  \
      DS:rx_snr:GAUGE:$hbl:-200:200  \
      DS:tx_pwr:GAUGE:$hbl:-200:200  \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "kant_ping" ]; then
   echo "initializing kant_ping.rrd"
   rrdtool create kant_ping.rrd \
      --step $steps \
      DS:pl:GAUGE:$hbs:0:100 \
      DS:rtt:GAUGE:$hbs:0:10000000 \
      RRA:MAX:0.5:1:1500 \
      RRA:MAX:0.5:1h:14d \
      RRA:MAX:0.5:1d:14d 
fi


if [ "$file" == "all" -o "$file" == "kant_status" ]; then
   echo "initializing kant_status.rrd"
   rrdtool create kant_status.rrd \
      --step $stepl \
      DS:int_temp:GAUGE:$hbl:-275:500  \
      DS:int_hum:GAUGE:$hbl:-275:500  \
      DS:aperture_i:GAUGE:$hbl:0:100  \
      DS:aperture_v:GAUGE:$hbl:0:500  \
      DS:in_i:GAUGE:$hbl:0:100  \
      DS:in_v:GAUGE:$hbl:0:500  \
      DS:q1temp:GAUGE:$hbl:-275:500  \
      DS:q2temp:GAUGE:$hbl:-275:500  \
      DS:q3temp:GAUGE:$hbl:-275:500  \
      DS:q4temp:GAUGE:$hbl:-275:500  \
      DS:status:GAUGE:$hbl:0:1 \
      DS:pfwarn:GAUGE:$hbl:0:1 \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "kant_tracking" ]; then
   echo "initializing kant_tracking.rrd"
   rrdtool create kant_tracking.rrd \
      --step $stepl \
      DS:agc:GAUGE:$hbl:0:U \
      DS:pl_es_n0:GAUGE:$hbl:0:U  \
      DS:r_es_n0:GAUGE:$hbl:0:U \
      DS:pl_sync:GAUGE:$hbl:0:U \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "iperf" ]; then
   echo "initializing iperf.rrd"
   rrdtool create iperf.rrd \
      --step $steps \
      DS:rate:GAUGE:$hbs:0:U \
      $rra_max_min_avg
fi


if [ "$file" == "all" -o "$file" == "latency_files" ]; then
   echo "initializing latency_files.rrd"
   rrdtool create latency_files.rrd \
      --step $steps \
      DS:1k:GAUGE:$hbs:0:U \
      DS:10k:GAUGE:$hbs:0:U \
      DS:100k:GAUGE:$hbs:0:U \
      DS:1M:GAUGE:$hbs:0:U \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "psmon" ]; then
   echo "initializing psmon.rrd"
   rrdtool create psmon.rrd \
      --step $steps \
      DS:driver:GAUGE:$hbs:0:U \
      DS:chrome:GAUGE:$hbs:0:U \
      DS:xvfb:GAUGE:$hbs:0:U \
      DS:mem:GAUGE:$hbs:0:U \
      DS:cpu:GAUGE:$hbs:0:U \
      DS:latency:GAUGE:$hbs:0:U \
      DS:ant:GAUGE:$hbs:0:U \
      DS:modem:GAUGE:$hbs:0:U \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "aws_ping" ]; then
   echo "initializing aws_ping.rrd"
   rrdtool create aws_ping.rrd \
      --step $steps \
      DS:pl:GAUGE:$hbs:0:100 \
      DS:rtt:GAUGE:$hbs:0:10000000 \
      RRA:MAX:0.5:1:1500 \
      RRA:MAX:0.5:1h:14d \
      RRA:MAX:0.5:1d:90d 
fi

if [ "$file" == "all" -o "$file" == "uptime" ]; then
   echo "initializing uptime.rrd"
   rrdtool create uptime.rrd \
      --step $steps \
      DS:uptime:GAUGE:$hbs:0:U \
      $rra_max_min_avg
fi

if [ "$file" == "all" -o "$file" == "weather" ]; then
   echo "initializing weather.rrd"
   rrdtool create weather.rrd \
      --step $stepm \
      DS:temp_f:GAUGE:$hbm:-100:200 \
      DS:dp_f:GAUGE:$hbm:-100:200 \
      DS:vis_mi:GAUGE:$hbm:0:U \
      DS:wind_mph:GAUGE:$hbm:0:U \
      DS:rh:GAUGE:$hbm:0:100\
      RRA:MAX:0.5:10m:14d \
      RRA:MAX:0.5:1d:30d \
      RRA:MIN:0.5:10m:14d \
      RRA:MIN:0.5:1d:30d \
      RRA:AVERAGE:0.5:10m:14d \
      RRA:AVERAGE:0.5:1d:90d 
fi


# xff = fraction of unknown data

