behave xterm
if has("win32") || has("win16")
   set ffs=unix,dos
else
   set t_Co=256
endif

set autoindent
set cindent
set cinkeys =0{,0},:,!^F,o,O,e "Turn off # processing, default = 0{,0},:,0#,!^F,o,O,e
set diffopt=iwhite,filler     "default =filler
set grepprg=grep\ -n
filetype indent on
" MSVC Compatible
"set shiftwidth=5
"set tabstop=5
"set noexpandtab

" My Normal settings
set shiftwidth=3
"set tabstop=5
set tabstop=3
set expandtab

set textwidth=80
set colorcolumn=+1

set smartindent
set smarttab
set ruler
set autowrite
set backspace=indent,eol,start
set makeprg=make\ %:t:r
"set makeprg=nmake\ %:t:r
map <F9> :w<CR>:make<CR>
set nowrap
set formatoptions=tcqr
"set foldmethod=indent

"set noncompatible
set modeline
"set modelines=1
"filetype plugin on

if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8
  setglobal fileencoding=utf-8 bomb
  set fileencodings=ucs-bom,utf-8,latin1
endif

"set diffexpr=MyDiff()
"function MyDiff()
"   let opt = ""
"   if &diffopt =~ "icase"
"     let opt = opt . "-i "
"   endif
"   if &diffopt =~ "iwhite"
"     let opt = opt . "-b "
"   endif
"   silent execute "!diff -ad " . opt . v:fname_in . " " . v:fname_new . " > " . v:fname_out
"endfunction


" Make external commands work through a pipe instead of a pseudo-tty
"set noguipty

" set the X11 font to use
" set guifont=-misc-fixed-medium-r-normal--14-130-75-75-c-70-iso8859-1

" Make command line two lines high
"set ch=2

" Make shift-insert work like in Xterm
"map <S-Insert> <MiddleMouse>
"map! <S-Insert> <MiddleMouse>

" Only do this for Vim version 5.0 and later.
if version >= 500

  " I like highlighting strings inside C comments
  let c_comment_strings=1

  " Switch on syntax highlighting.
  syntax on

  " Switch on search pattern highlighting.
  set hlsearch

  " For Win32 version, have "K" lookup the keyword in a help file
  "if has("win32")
  "  let winhelpfile='windows.hlp'
  "  map K :execute "!start winhlp32 -k <cword> " . winhelpfile <CR>
  "endif

  " Hide the mouse pointer while typing
  set mousehide

  " Set nice colors
  " background for normal text is light grey
  " Text below the last line is darker grey
  " Cursor is green
  " Constants are not underlined but have a slightly lighter background
  "highlight Normal guibg=grey90
  highlight Normal guibg=white
  highlight Cursor guibg=Green guifg=NONE
  highlight NonText guibg=NONE
  highlight Constant gui=NONE guibg=NONE
  highlight Special gui=NONE guibg=NONE

  colorscheme morning2
endif

let g:pylint_onwrite = 0

