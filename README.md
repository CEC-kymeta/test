General
============
This is a set of scripts to monitor the performance of a Kymeta system.

## Physical Connections
![Connections](connections.png)

## Data Paths
Data is collected by a variety of `.py` and `.sh` scripts, most of which are 
called by cron, through the `update_rrd.sh` script . The two main exceptions 
are `weather.py` and `graph_rrd.sh`, which have different update frequencies.
These two scripts are called separately by cron.

![Data Paths](data_paths.png)

## RRDTool
This system relies heavily on [RRD Tool](https://oss.oetiker.ch/rrdtool/).
- Many of the scripts are based on examples from : https://calomel.org/rrdtool.html



## Local Setup
Add an entry to your `/etc/hosts` file for the logging computer

It is helpful to setup your local `~/.ssh/config` with something like:
```
Host kyminnow
User kymeta
ForwardX11  yes
IdentityFile ~/.ssh/id_rsa
```

Once the SSH server is installed on the logging computer, copy your key with:
```
ssh-copy-id -i ~/.ssh/id_rsa kyminnow
```

Logging Computer Installation
=============================
Some installs are on a Raspberry Pi, some are on a Turbot [Minnowboard](https://minnowboard.org/).

## Xubuntu Setup (Minnowboard)
1. Start with an Xubuntu 18.04 USB stick. Go through the Xubuntu installer, to
get it installed on an SD card (8GB or 16GB should be large enough).
2. Run a few commands:
    ```
    sudo apt install vim git aptitude openssh-server                # Install some basic stuff
    ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa                        # Create a set of ssh keys
    git clone https://gitlab.com/CEC-kymeta/test.git kymeta/scripts # Grab this repository
    cd ~/kymeta/scripts
    ./install.sh
    ```

## Raspberry Pi Setup

1. Follow the instructions to put [raspian](https://www.raspberrypi.org/downloads/raspbian/) (get the "With Desktop" version)
   on a pi, either with [Etcher](https://www.raspberrypi.org/documentation/installation/installing-images/README.md), or with [command line tools](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md).
   Note: get 'Stretch' or later. Jessie is not compatible with some of the
   commands in `init_rrd.sh`.
2. Set the IP address, with `raspi-config`
3. Set the Hostname, with `raspi-config`
4. Enable [sshd](https://www.raspberrypi.org/documentation/remote-access/ssh/), with `raspi-config`
5. (optional) install testuser
6. (optional) Update and install vim:
   ```
   sudo apt-get update
   sudo apt-get install vim-runtime
   sudo apt-get install vim
   ```
7. Install git (`sudo apt-get install git`), if you didn't install testuser
8. Change the keyboard to US (with `raspi-config`)
9. Change timezone (with `raspi-config`)

### Installing Vim
The default install is vim-tiny on Raspian, and a simple `apt-get install vim` 
doesn't seem to work. instead, you need to do something like:
    
    sudo apt-get update
    sudo apt-get install vim-runtime
    sudo apt-get install vim

## Installing the Logging/Plotting scripts

- Install apache: https://www.maketecheasier.com/install-and-configure-apache-in-ubuntu/

    ```
    apt-get install rrdtool apache2 libqtwebkit4 libwebkitgtk-dev libqtwebkit-dev python-webkit
    ```

- add stuff to crontab:
    ```
    # Sync collect and plot RRD data
    *    *     *   *  *   /home/russell/projects/latency-rrd/update_rrd.sh 2>&1 | /usr/bin/logger -t update_rrd
    */5  *     *   *  *   /home/russell/projects/latency-rrd/graph_rrd.sh 2>&1 | /usr/bin/logger -t graph_rrd
    ```
- Modify the html directory
    ```
    sudo usermod -a -G www-data username      # add user to www-data
    sudo chown root:www-data /var/www/html/
    sudo chmod 775 /var/www/html/
    ```
- Logout (not just close the terminal window), and then log back in, for the above permissions to take hold

- copy an ssh public key to the Antenna's `~/.ssh/authorized_keys`

- Setup a static route for the iperf: `sudo ip route add 54.186.205.190 via 192.168.44.1 dev eth0:4`

- Install gnuplot for the `plot_logs.plt`

- pip install requests, selenium, others

- Add a route to the AWS iperf server, if required

- Do something to make [firefox accept self signed certificates](https://stackoverflow.com/questions/275237/in-firefox-can-i-disable-the-website-certified-by-an-unknown-authority-ssl-ce).



TODO
=======
1. [x] More Hourly Graphs
2. [x] fix the try:except statements in `modem_temp_selenium.py`, so that we NEVER
      leave zombie chromium processes.
3. [ ] Hourly graphs don't seem to have enough data 
4. [ ] Get iperf working
    1. [ ] Get iperf working in the other direction. -r and -d options don't seem to work with the AWS instance.
    2. [ ] Add iperf service restart to server, since it seems to go down daily-ish.
5. [x] time of the download of a set of files
6. [x] netpipe
7. [x] Add -w (workdir) to remaining python scripts, and then modify `update_rrd.sh` to match
8. [ ] Have `install.sh` ask for passwords and insert them into the .py scripts??
9. [ ] Run a connection to Trimble connect or similar, to see if latency causes any issues?
10. [ ] Check for, and delete spurious tmp and lock files in /tmp (see notes in `psmon_kill.sh`)

Latency Files
=============
The latency of files test downloads a series of files. These files, containing 
random data, are created with `create_latency_files.sh`, and then uploaded to
a server (the same one used for iperf).
    
    ./create_latency_files.sh
    scp -r latency_files/ ubuntu@hostname:/var/www/html/

Then, the files are downloaded to `/tmp` with the `update_latency_files.py` script.

Logging Events
==============
So, lets say that you did something to the setup. You can have this logged, by
typing `./log_add.sh 'I did something to the setup'`. This will add an entry to
`log.csv`, and the next time `graph_rrd.sh` runs, it will call the `plot_logs.plt`
script which uses gnuplot to plot the events.

The format of the `log.csv` file is `date, 1, string`. Where:
* date is in the format `%Y/%m/%d:%H:%M:%S`
* The value in the 2nd column is the height of the stem plot line, and the vertical
  location of the label for that row. Only a value of '1' has been tested.
* The string is any string, without a comma.

**Note:** The `plot_logs.plt` script **must** be run with the `htmldir` variable set.
`graph_rrd.sh` takes care of this. But when running it stand alone, do something like:

    `htmldir=html plot_logs.plt`

To review:
1. Add new events with something like `./log_add.sh 'I did something to the setup'`
2. The `plot_logs.plt` will be run by `graph_rrd.sh`, and create graphs in the 
   output directory.

Debugging Hints
===============
If the data looks funny, use rrdtool to check the database:
* `rrdtool lastupdate filename.rrd` to get the last results from an `.rrd` database file
* `rrdtool dump filename.rrd > /tmp/filename.xml`, and then open with a text editor to look at all the data.

If there are gaps in the data, make sure that the processes are not overloading the "Server".

If the iperf data doesn't seem to be getting above ~40kb/s for long periods of 
time, try reseting the iperf server on the AWS instance (`54.186.205.190`). As of
2017-10-09, systemd is being used to run the iperf server. Something like
`systemctl --user restart iperfs` should restart the iperf server.

Check syslog (`cat /var/log/syslog`). Most of the scripts will print messages to syslog. Look

Updating Drawings
=================
The drawings in this README.md were created with [inkscape](https://inkscape.org/en/), an `.svg` editor. To make changes, update the `.svg` files with your favorite editor, and then export to similarly named `.png` files. Then, they should show up in the HTML version of the README.

Meraki
========

    10.243.14.139 (statically assigned)
    Gateway: 10.243.14.129; DNS: 8.8.8.8, 8.8.8.8
    Public IP:    66.205.22.148 (unknown)
    Hostname:    kymeta-hrnmgkndch.dynamic-m.com
    Usage:    2 clients used 278.4 MB in the last day
  
  
    [11:28] I am not sure you can change it remotelt
    [11:28] so connect directly to the Z1
    [11:28] open a browser
    [11:28] got to my.meraki.com
    [11:29] and then you can change the uplink config
  
    kymeta-hrnmgkndch.dynamic-m.com:80 -> 66.205.22.148

Drive Testing
==============

## Installing

To install the data collection script (`kymeta_drive.py`), copy the script to 
the (laptop) computer to be used for processing. Look at the `import` lines of
`kymeta_drive.py` for hints on how to install the prerequisites.

For the post processing software, just copy the script `kymeta_drive.py`. It 
should work with most python installations.

To visualize the resulting KML files, [google earth](https://www.google.com/earth/download/gep/agree.html)
will need to be installed. On Ubuntu 16, insteall the prerequisites with something
like: 

    sudo apt install lsb-invalid-mta lsb-security lsb-core

## Running
1. On a laptop connected to the Kymeta, run `kymeta_drive.py`
2. (optional) Copy the resulting `kdrive.csv` to another computer, that has 
   `kymeta_post.py` installed.
3. Edit the `kymeta_post_list.txt` (on 2nd computer) to include any `kdrive.csv`
   files that need to be processed.
4. Run `kymeta_post.py`
5. Open the KML files created in `results/` with google earth. Note: on linux,
   if you want to open the files from the command line, it is probably something
   like:

        google-earth-pro /full/path/to/kymeta.kml

<!-- vim: sw=4 ts=4
-->
