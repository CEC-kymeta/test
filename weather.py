#!/usr/bin/python
import argparse
import syslog
import sys
from bs4 import BeautifulSoup
import requests
import rrdtool
import urllib2
import os
#import json

# sudo pip install dryscrape
# sudo apt-get install python-rrdtool rrdtool

parser = argparse.ArgumentParser()
parser.add_argument('-u', '--url', dest='url', action='store', default="http://w1.weather.gov/xml/current_obs/KBJC.xml",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='working directory: %(default)s')
parser.add_argument('-f', '--filename', dest='filename', action='store', default="weather.rrd",
        help='Filename for status data: %(default)s')
parser.add_argument('-s', '--skip_syslog', dest='syslog', action='store_false', default=True,
        help='Disable logging to syslog')
parser.add_argument('-v', '--verbose', dest='verbose', action='store', type=int, default=0,
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level = 1.')
args = parser.parse_args()

if args.verbose_on:
    args.verbose = max(1, args.verbose)

########### DEBUG ############
#args.verbose = 1

if args.syslog:
    syslog.openlog('weather_rrd')

filename = os.path.join(args.work_dir, args.filename)
if args.verbose:
    print( "filename = " +filename)
    print( "url = " +args.url)

tmpl = "temp_f:rh:dp_f:vis_mi:wind_mph"

sess = requests.Session()
sess.verify = False
try:
    r = sess.get(args.url)
    s = r.content
    #f = urllib2.urlopen(args.url)
    #s = f.read()
except:
    if args.syslog:
        syslog.syslog("s.get(url) error - probably timeout?")
    if args.verbose:
        print( "s.get(url) error - probably timeout?"); 
    vals = 'N:0:0:0:0:0'
    rrdtool.update(filename, '--template', tmpl, vals)
    sess.close()
    sys.exit()

if args.verbose:
    print(s)
    print ""

soup = BeautifulSoup( s, "lxml" )
temp_f = soup.find(name='temp_f').text
rh = soup.find(name='relative_humidity').text
dp = soup.find(name='dewpoint_f').text
#precip_1hr_in = soup.find(name='precip_1hr_in').text
visibility_mi = soup.find(name='visibility_mi').text

# Sometimes the wind_mph field does not exist, so we have to do some
# special handling.
wind_mph = soup.find(name='wind_mph')
if not wind_mph:
    wind_mph = '0'
else:
    wind_mph = wind_mph.text


lvals=[temp_f, rh, dp, visibility_mi, wind_mph]
vals = str( ':'.join(lvals) )
vals = 'N:' +vals

if args.verbose:
    print "Current temperature : " +temp_f +" F"
    print "Relative Humidity " +rh +"%"
    print "Dewpoint " +str(dp) +" F"
    #print "precip_1hr_in = " +precip_1hr_in
    print "visibility_mi = " +visibility_mi 
    print "wind_mph = " +wind_mph +" mph" 
    print "vals = " +vals


rrdtool.update(filename, '--template', tmpl, vals)

if args.syslog:
    syslog.closelog()
sess.close()

