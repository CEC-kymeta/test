#!/usr/bin/python
import argparse
import sys, os
import requests
import rrdtool

#curl --user user:pass -X GET --header "Accept: application/json" "http://192.168.44.2/v1/status/system/sensors"

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--ip_addr', dest='ip', action='store', default="192.168.44.2",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-f', '--filename', dest='filename', action='store', default="kant_status.rrd",
        help='Filename for status data: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-T', '--filename_tracking', dest='filename_tracking', action='store', default="kant_tracking.rrd",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-t', '--timeout', dest='timeout', action='store', type=int, default=5,
        help="sets 'page_load' and 'script' timeouts in selenium. default: %(default)s")
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')
args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

filename = os.path.join(args.work_dir, args.filename)

url = "http://" +args.ip
if args.verbose:
    print("url = " +url)
    print( "filename = " +filename)

s = requests.Session()
s.auth = ('admin', '2Cfg^Ant')
s.verify = False
#s.headers.update({'x-test': 'true'})
#r = s.get(url, verify=False)
#print_resp(r)


#r = s.get(url +"/#/status", verify=False)
tmpl = 'int_temp:int_hum:aperture_i:aperture_v:in_i:in_v:status:pfwarn:q1temp:q2temp:q3temp:q4temp' 
try:
    r = s.get(url +"/v1/status/system/sensors", verify=False, headers={'Accept':'application/json'}, timeout=args.timeout)
except:
    if args.verbose:
        print( "s.get(url) error - probably timeout?"); 
    vals = 'N:0:0:0:0:0:0:0:0:0:0:0:0'
    rrdtool.update(filename, '--template', tmpl, vals)
    sys.exit()


if args.verbose:
    print( 'r.json() = ' +str(r.json()) )
    #print_resp(r)

#
#r.json() = {u'status': u'ok', u'internal_temp': u'31.113', 
#            u'quad1_temp': u'24.840', u'quad4_temp': u'25.505', 
#            u'aperture_current': u'0.011', u'input_current': u'0.380', 
#            u'quad2_temp': u'26.234', u'power_fail_warning': False, u'message': u'', 
#            u'quad3_temp': u'27.954', u'internal_humidity': u'18.325', u'input_voltage': u'29.625', 
#            u'aperture_voltage': u'20.200'}

# Note: lvals and tmpl are where the json keys from the response are mapped to 
#        the keys in the rrd database. The order they are listed in each of the 
#        declarations below creates the mapping. If new fields show up in the 
#        json response, that will not break the mapping. But when adding new
#        fields into the rrd database, be careful to update both tmpl AND lvals
#        as appropriate. Also, the rrd database will need to be re-initialized.
#        To do this, update the appropriate 'rrdtool create' command in init_rrd.sh
j = r.json()
if 'ok' in str(j['status']):
    status = '1'
else:
    status = '0'
if 'Fail' in str(j['power_fail_warning']):
    pfwarn = '0'
else:
    pfwarn = '1'
lvals = [ str(j['internal_temp']),    str(j['internal_humidity']),
          str(j['aperture_current']), str(j['aperture_voltage']),
          str(j['input_current']),    str(j['input_voltage']),
          status, pfwarn,
          str(j['quad1_temp']),    str(j['quad2_temp']),
          str(j['quad3_temp']),    str(j['quad4_temp']) ]
vals = ':'.join(lvals)
vals = 'N:' +vals
if args.verbose:
    print( 'vals = ' +vals)

rrdtool.update(filename, '--template', tmpl, vals)



############### DEBUG ################
#args.verbose = 1

s = requests.Session()
s.auth = ('admin', '2Cfg^Ant')
s.verify = False

tmpl = 'agc:pl_es_n0:r_es_n0:pl_sync' 
try:
    r = s.get(url +"/v1/status/tracking", verify=False, headers={'Accept':'application/json'}, timeout=args.timeout)
except:
    if args.verbose:
        print( "s.get(url) error - probably timeout?"); 
    vals = 'N:0:0:0:0'
    rrdtool.update(args.filename_tracking, '--template', tmpl, vals)
    sys.exit()

j = r.json()
if args.verbose:
    print("tracking")
    print(r.content)
    print(j)
    print("")

if 'True' in str(j['data_4']):
    pl_sync = '1'
else:
    pl_sync = '0'
lvals = [ str(j['data_1']),    str(j['data_2']),
          str(j['data_3']), pl_sync ]
vals = ':'.join(lvals)
vals = 'N:' +vals
if args.verbose:
    print( 'vals = ' +vals)
rrdtool.update(args.filename_tracking, '--template', tmpl, vals)
