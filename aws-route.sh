#!/bin/bash
#
# setup (add) or teardown (del) a route to a specific IP address
act="add"
ip="54.186.205.190"
gw="192.168.44.1"
method="i"
verbose=0
run_cmd=true

# Grab the first listed non-loopback (ethernet) device
#dev=$(ip a|grep BROADCAST|grep -v LOOPBACK|grep -v "NO-CARRIER"|head -n 1|cut -f 2 -d ' '|cut -f 1 -d ':')
dev="eth0:4"      # The was the setup on Russ' desktop.

usage() {
    echo ""
    echo "Usage: $0 [-d|a] [-i IP_ADDR] [-g GATEWAY] [-e DEVICE] [-m iproute|dchpcd] [-v] [-h] [-s]" 
    echo "add or delete a route to a specific IP address."
    echo "-d   delete the route"
    echo "-a   Add a route (default)"
    echo "-i   the IP address of the static route [$ip]"
    echo "-g   the Gateway of the static route [$gw]"
    echo "-e   the device of the static route (eth0, eth1:4, etc) [$dev]"
    echo "-m   the method of setting the route. [$method]"
    echo "        [i]proute: use an 'ip route' command, which won't last through a reboot"
    echo "        [d]hcpcd: modify dhcpcd config, with *will* last through a reboot."
    echo "-s   skip actually running the command (dry-run)"
    echo "-v   verbose: turn on some debug messages"
    echo "Note: this script requires root privelages"
    echo ""
    exit
}

while getopts "adi:g:e:m:vsh" arg; do
    case "${arg}" in
        a) act="add" ;;
        d) act="del" ;;
        i) ip=$OPTARG ;;
        g) gw=$OPTARG ;;
        e) dev=${OPTARG} ;;
        m) method=${OPTARG::1} ;;   # only grab the first letter
        v) verbose=1 ;;
        s) run_cmd=false ;;
        h)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))


if [ $verbose -ne 0 ]; then
    echo "act = $act"
    echo "ip = $ip"
    echo "dev = $dev"
    echo "gw = $gw"
    echo "method = $method"
fi

cmd="ip route $act $ip via $gw dev $dev"
if [ $verbose -ne 0 ]; then
    echo "cmd = $cmd"
fi

if [ "$method" == "d" ]; then
    #dhcpcd_dir=$(find /usr/lib -name "dhcpcd*" -type d)
    dhcpcd_dir="/lib/dhcpcd"
    #dhcpcd_dir=""
    #if [ -d "/usr/lib/dhcpcd" ] ; then
    #    dhcpcd_dir="/usr/lib/dhcpcd"
    #fi
    #if [ -d "/usr/lib/dhcpcd5" ] ; then
    #    dhcpcd_dir="/usr/lib/dhcpcd5"
    #fi
    #if [ -z "$dhcpcd_dir" ] ; then
    #    echo "could not find dhcpcd directory."
    #    exit
    #fi

    hooks_dir="$dhcpcd_dir/dhcpcd-hooks"
    if [ ! -d "$hooks_dir" ]; then
        #echo "creating $hooks_dir directory."
        #if $run_cmd; then
        #    sudo mkdir "$hooks_dir"
        #fi
        echo "can not find  $hooks_dir directory."
        exit
    fi

    hook_file="40-modem-route"
    #hook_file="40-vpnroute"
    cmd="\"$cmd\""
    if (! grep -q -s "$ip" "$hooks_dir/$hook_file"); then
        if $run_cmd; then
            if [ $verbose -ne 0 ]; then
                echo "running: sudo /bin/bash -c \"echo $cmd >> $hooks_dir/$hook_file\""
            fi
            sudo /bin/bash -c "echo $cmd >> $hooks_dir/$hook_file"
            #sudo service dhcpcd restart
        fi
    fi
    if [ $verbose -ne 0 ]; then
        echo "running: sudo systemctl restart dhcpcd"
    fi
    sudo systemctl restart dhcpcd
else
    if $run_cmd; then
        if [ $verbose -ne 0 ]; then
            echo "runnding: sudo $cmd"
        fi
        sudo $cmd
    fi
fi


