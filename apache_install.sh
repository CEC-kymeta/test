#!/bin/bash


# Install Apache mod_proxy, and set it up to proxy connections to the satellite
# modem and the Kymeta antenna.
sudo a2enmod proxy proxy_http ssl
sudo a2enmod proxy_ajp rewrite deflate headers
sudo a2enmod proxy_balancer proxy_connect proxy_html
hname=$(hostname)
keydir=/etc/apache2/keys
sudo mkdir -p "$keydir"

if [ ! -f "$keydir/host.key" ]; then
   sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout "$keydir/host.key" -out "$keydir/host.crt" -subj "/C=US/ST=Colorado/L=Westminster/O=trimble/CN=$hname"
fi

#TODO: edit etc/apache2/000-default.conf with correct hostname
#TODO: detect if port 443 is already enabled and modify 'Listen 443 line of
#      etc/apache2/000-default.conf accordingly.
sudo cp etc/apache2/000-default.conf /etc/apache2/sites-enabled/

sudo systemctl restart apache2

