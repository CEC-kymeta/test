#!/bin/bash

# Note: this script has only been tested on Raspian Jessie and 18.04, but should
# work for most linux systems

# This is an install script to install dependencies and get a bunch of other 
# stuff setup on a new Ubuntu (16) box

usage() {
    echo "Usage: $0 [-d] [-m]" 
    echo ""
    echo "-d   skip dependencies (ie no apt-get install)"
    echo "-o   overwrite files by processing *.in files"
    exit 1;
}

source "rrd_vars.sh"        # contains aws_iperf, program and directory variables

skip_deps=false
overwrite=false
while getopts "doh" arg; do
    case "${arg}" in
        d)
            skip_deps=true
            ;;
        o)
            overwrite=true
            ;;
        h)
            usage
            exit
            ;;
    esac
done
shift $((OPTIND-1))

#echo "$skip_deps"
#echo "$overwrite"
#exit

systype=$(lsb_release -d | cut -f 2 | cut -f 1 -d ' ')
echo "systype = $systype"
if [ "$systype" = "Ubunutu" ]; then
    osver=$(lsb_release -r| cut -f 2)
    echo "osver = $osver"
    if [ "$osver" = "18.04" ]; then
        sudo cp "etc/netplan/01-network-manager-all.yaml" "/etc/netplan/"
    else
        echo "Unknown osver ($osver) for $systype. Not setting up networking config"
    fi
elif [ "$systype" = "Raspian" ]; then
    echo "Not setting up networking config for $systype."
    # Stuff to put in /etc/dhcpcd.conf
    # This was used when there was *not* a Meraki
    #interface eth0
    #static ip_address=192.168.44.5
    #static routers=
    #static domain_name_servers=
    #static domain_search=

    # Stuff to put in /etc/network/interfaces
    # These should work for a kymeta box, with a Meraki
    #auto eth0
    #allow-hotplug eth0
    #iface eth0 inet static
    #	address 192.168.128.5
    #        netmask 255.255.255.0
    #        gateway 192.168.128.1
    #
    #auto eth0:1
    #iface eth0:1 inet static
    #	address 192.168.44.5
    #	netmask 255.255.255.0
    #
    #auto eth0:2
    #iface eth0:2 inet static
    #	address 192.168.1.5
    #	netmask 255.255.255.0
    #
    ##iface eth0:3 inet static
    ##	address 10.243.14.140
    ##        netmask 255.255.255.0
    ##        gateway 10.243.14.129
    ##iface eth0 inet dhcp
else
    echo "Unknown systype ($systype). Not setting up networking config"
fi


#---------------------------------------
disp_warn () {
#---------------------------------------
    #echo ""
    echo -e "\033[40;33;1mWarning: \033[0m$1"
    #echo ""
}

### Install Depenencies
if command -v aptitude &>/dev/null; then 
    pkgcmd='aptitude -y'
    pkgupg='aptitude -y full-upgrade'
else
    pkgcmd='apt-get -y'
    pkgupg='apt-get -y dist-upgrade'
fi
if ! $skip_deps; then
    echo "Upating packages with apt/aptitude/apt-get..." 
    sudo $pkgcmd update
    sudo $pkgupg
    sudo $pkgcmd install rrdtool apache2 iperf gnuplot-nox gawk bc
    #sudo /etc/init.d/apache2 start

    #   libqtwebkit4 libwebkitgtk-dev libqtwebkit-dev python-webkit 
    sudo $pkgcmd install python-pip python-requests python-rrdtool python-lxml python-psutil python-pyvirtualdisplay
    sudo $pkgcmd install ipython

    # For apache mod_proxy
    sudo $pkgcmd install libxml2-dev

    sudo $pkgcmd install iceweasel xvfb 
    sudo $pkgcmd install python-xvfbwrapper python-selenium
    pip install --user beautifulsoup4 germanium

    m=$(uname -m)
    if [ "$m" == "armv7l" ]; then
        gecko_tgz="geckodriver-v0.19.0-arm7hf.tar.gz"
    elif [ "$m" == "x86_64" ]; then
        gecko_tgz="geckodriver-v0.19.0-linux64.tar.gz"
    else
        echo "Unknown architecture"
        exit
    fi
    if [ ! -f "$gecko_tgz" ]; then
        wget https://github.com/mozilla/geckodriver/releases/download/v0.19.0/$gecko_tgz
    fi
    if [ ! -f geckodriver ]; then
        tar xfz $gecko_tgz
    fi

    #if [ ! -f "/usr/bin/phantomjs" ]; then
    #    src_dir=`pwd`
    #    cd /tmp
    #    wget -P /tmp https://github.com/aeberhardo/phantomjs-linux-armv6l/archive/master.zip 
    #    unzip master.zip
    #    cd phantomjs-linux-armv6l-master
    #    tar xfj phantomjs-linux-armv6l-master.tar.bz2
    #    cd ..
    #    sudo mv phantomjs-linux-armv6l-master /opt
    #    sudo ln -s /opt/phantomjs-1.9.0-linux-armv6l/bin/phantomjs /usr/bin/phantomjs
    #fi
    sudo $pkgcmd install phantomjs

fi

# Install Apache modules, including mod_proxy, and set it up to proxy 
# connections to the satellite modem and the Kymeta antenna.
./apache_install.sh

# TODO: disable most of the CRON entries in syslog:
# https://serverfault.com/questions/31334/how-can-i-prevent-cron-from-filling-up-my-syslog
#echo "EXTRA_OPTS="-L 4" >> /etc/default/cron

### WWW directory Access
# The `pi` user will need to be added to the `www-data` group, and the 
# `/var/www/html/` directory permissions need to be modified.
# 
# Note: the user will have to logout and login again to make the permissions 
#       change effective.
user=`whoami`
sudo usermod $user -aG www-data
html_dir="/var/www/html/"
sudo chown root:www-data "$html_dir"
sudo chmod 775 "$html_dir"

if [ ! -d "html" ]; then
    echo "Creating symbolic link for html dir"
    ln -s "$html_dir" html
fi
sudo cp -a 24hr.html 30day.html 5day.html "$html_dir"
sudo cp -a kymeta_rrd.html "$html_dir/index.html"

if [ ! -f "psmon.rrd" ]; then
    echo "Initializing Databases"
    ./init_rrd.sh
fi

tmpcrontab="/tmp/crontab"
crontab -l > "$tmpcrontab"
if (grep -1 "no crontab for" "$tmpcrontab"); then
    rm "$tmpcrontab"
    disp_warn "No existing crontab file. User intevention required. Follow the prompts"
    crontab -e
fi
crontab -l > "$tmpcrontab"
if (! grep -q "update_rrd" "$tmpcrontab"); then
    src_dir=`pwd`
    echo "Updating crontab"
    echo "*/2  *     *   *  *   $src_dir/update_rrd.sh 0 $src_dir 2>&1 | /usr/bin/logger -e -t update_rrd"  >> "$tmpcrontab"
    echo "*/4  *     *   *  *   /usr/bin/nice -n 15 $src_dir/graph_rrd.sh -t minute -w $src_dir  2>&1 | /usr/bin/logger -e -t graph_rrd"   >> "$tmpcrontab"
    echo "*/30 *     *   *  *   /usr/bin/nice -n 15 $src_dir/graph_rrd.sh -t hour -w $src_dir  2>&1 | /usr/bin/logger -e -t graph_rrd"   >> "$tmpcrontab"
    echo "0    *     *   *  *   /usr/bin/nice -n 15 $src_dir/graph_rrd.sh -t month -w $src_dir  2>&1 | /usr/bin/logger -e -t graph_rrd"   >> "$tmpcrontab"
    echo "*/5  *     *   *  *   /usr/bin/nice -n 10 $src_dir/weather.py -w $src_dir 2>&1" >> "$tmpcrontab"
    echo "*/15  *     *   *  *   /usr/bin/nice -n 19 $src_dir/modem_temp_germanium.py -w $src_dir 2>&1" >> "$tmpcrontab"
    echo "5,20,35,50  *     *   *  *   /usr/bin/nice -n 19 $src_dir/ant_requests.py -w $src_dir 2>&1" >> "$tmpcrontab"
    echo "0   1     *   *  *   $src_dir/psmon_kill.sh 2>&1 | /usr/bin/logger -e -t kill_rrd"    >> "$tmpcrontab"
    echo "59   *     *   *  *   /usr/bin/rsync -a /var/www/html/* aws-iperf:/var/www/html/kypi/ 2>&1 | /usr/bin/logger -e -t rsync_aws"
    crontab "$tmpcrontab"
    rm "$tmpcrontab"
fi


#TODO: if there is not an ssh key, then generate one

# SSH Config
#Need to add the following to the `/home/test/.ssh/config` file:
#config="$HOME/.ssh/config"
#if (! grep -q "^Host 192.168.\*" $config); then
#    echo ""
#    echo "Adding Host 192.168.* to $config"
#    echo " " >> $config
#    echo "Host 192.168.*,dut" >> $config
#    echo "User root" >> $config
#    echo "KexAlgorithms +diffie-hellman-group1-sha1" >> $config
#    echo "StrictHostKeyChecking no" >> $config
#    echo "UserKnownHostsFile /dev/null" >> $config
#    echo "Port 22000" >> $config
#fi


#TODO: Modify weather.py for the correct URL for our location


# Convenience: add some aliases to /etc/hosts
if (! grep -q "kmodem" /etc/hosts); then
    echo ""
    echo "Note: adding aliases to /etc/hosts"
    echo "" | sudo tee -a /etc/hosts
    echo "192.168.44.1    kmodem" | sudo tee -a /etc/hosts
    echo "192.168.44.2    kant" | sudo tee -a /etc/hosts
    #echo "192.168.44.5    kypi" | sudo tee -a /etc/hosts
    echo "$aws_iperf    aws-iperf" | sudo tee -a /etc/hosts
fi

# Check for a route to the iperf server. If it does not exist, then add one.
# Note: this is only necessary if there are 2 ethernet/wireless interfaces, and
# the one through the kymeta antenna is not the default one for internet connections.
#routes=$(ip route)
#if (! echo $routes |grep -q $aws_iperf); then
#    echo "no route to $aws_iperf found, adding one"
#    ./aws-route.sh
#fi

cp buttondown.css github-pandoc.css "$HOME"
cp .bashrc "$HOME"
cp .vimrc "$HOME"
cp -r vimfiles "$HOME/.vim/"
cp .gitconfig "$HOME"

echo ""
echo "Install complete"
echo ""

# vim:expandtab:sw=4:ts=4:tw=0
