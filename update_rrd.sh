#!/bin/bash
#
DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
workdir=${2:-'.'} 

#echo "DEBUG = $DEBUG"

if [ "$DEBUG" == "-h" -o "$DEBUG" == "h" -o "$workdir" == "-h" -o "$workdir" == "h" ]; then
    echo "Usage: update_rrd.sh [debug level] [workdir]"
    exit
fi

### change to the script directory
cd $workdir
source "rrd_vars.sh"
 
#------------------------------
get_ping_data() {
#------------------------------
    hosttoping=$1
    ping_cmd="/bin/ping -q -n -c 3"
    local output=$($ping_cmd "$hosttoping" 2>&1)
    if [ $DEBUG -ne 0 ]; then
        echo "ping_cmd = $ping_cmd $hosttoping"
        echo "output = $output"
    fi
    local method=$(echo "$output" | $gawk '
        BEGIN {pl=100; rtt=0.1}
        /packets transmitted/ {
            match($0, /([0-9]+)% packet loss/, datapl)
            pl=datapl[1]
        }
        /min\/avg\/max/ {
            match($4, /(.*)\/(.*)\/(.*)\/(.*)/, datartt)
            rtt=datartt[2]
        }
        END {print pl ":" rtt}
        ')
    RETURN_DATA=$method
    if [ $DEBUG -ne 0 ] ; then
        echo "get_ping_data: $hosttoping method = $method"
    fi
}

 
### collect the data & update the databases

get_ping_data kant
$rrdtool update kant_ping.rrd --template pl:rtt N:$RETURN_DATA
 
get_ping_data aws-iperf
$rrdtool update aws_ping.rrd --template pl:rtt N:$RETURN_DATA
pl=$(echo $RETURN_DATA | cut -d : -f 1)
if [ "100" = "$pl" ]; then
    z="-z"  # Force zero data option
else
    z=""
fi
$workdir/update_iperf.py -w $workdir -f iperf.rrd -v $DEBUG $z
$workdir/update_latency_files.py -w $workdir -f latency_files.rrd -v $DEBUG $z
 
#$workdir/ant_requests.py -w $workdir -v $DEBUG  #-f kant_status.rrd -T kant_tracking.rrd 

get_ping_data kmodem
$rrdtool update kmodem_ping.rrd --template pl:rtt N:$RETURN_DATA
if [ "100" = "$pl" ]; then
    z="-z"  # Force zero data option
else
    z=""
fi
#$workdir/modem_temp_germanium.py -w $workdir -f kmodem_status.rrd -v $DEBUG $z

#$workdir/weather.py -w $workdir -v $DEBUG

$workdir/psmon.sh $DEBUG $workdir
$workdir/uptime.py -v $DEBUG -w $workdir -f uptime.rrd
$workdir/pi_temp.sh $DEBUG $workdir

#Note: Currently it takes about 28s to run all the updates (through update_latency_files)
#      on an old-ish Ubuntu CPU. And over 1 minute on a Raspberry Pi.

# vim:expandtab:sw=4:ts=4:tw=0
