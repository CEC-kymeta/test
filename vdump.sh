#!/bin/bash
#
#DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages

# This is a script to open a dump of an rrd database in your favorite editor

filename=$(basename "$1")
ext="${filename##*.}"
f="${filename%.*}"

editor=${2:-"vim"}

rrdtool dump $f.rrd > /tmp/$f.xml
$editor /tmp/$f.xml
 
