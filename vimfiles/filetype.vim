" Vim support file to detect file types
"
augroup filetypedetect

" Lingo files
au BufNewFile,BufRead *.ls	setf lingo

" Teraterm MACRO files
au BufNewFile,BufRead *.ttl	setf Teraterm

" Vim Session Script 
au BufNewFile,BufRead *.vims	setf vim

" Vim Session Script 
au BufNewFile,BufRead *.jl	setf julia

" Robot Framework
au BufNewFile,BufRead *.robot	setf robot

augroup END

