" Vim syntax file
" Language:	Matlab
" Maintainer:	Preben 'Peppe' Guldberg <peppe-vim@wielders.org>
"		Original author: Mario Eusebio
" Last Change:	30 May 2003

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn keyword matlabStatement		return
syn keyword matlabLabel			case switch default 
syn keyword matlabConditional		else elseif end if otherwise endif endswitch
syn keyword matlabConditional		try catch end_try_catch 
syn keyword matlabConditional		unwind_protect unwind_protect_cleanup end_unwind_protect
syn keyword matlabRepeat		do until for while endfor endwhile

syn keyword matlabTodo			contained  TODO

" If you do not want these operators lit, uncommment them and the "hi link" below
syn match matlabArithmeticOperator	"[-+]"
syn match matlabArithmeticOperator	"\.\=[*/\\^]"
syn match matlabArithmeticOperator	"="
syn match matlabRelationalOperator	"[=~]="
syn match matlabRelationalOperator	"[<>]=\="
syn match matlabLogicalOperator		"[&|~]"
syn match matlabLogicalOperator		"&&\|||"
syn match octaveRelationalOperator	"[=!]="
syn match octaveLogicalOperator		"!"

" Octave also supports ++, --, +=, -=, *=, /=
syn match octaveArithmeticOperator	"[-+\*/]="
syn match octaveArithmeticOperator	"++\|--"

syn match matlabLineContinuation	"\.\{3}"

"syn match matlabIdentifier		"\<\a\w*\>"

" String
syn region matlabString			start=+'+ end=+'+	oneline
syn region octaveString			start=+"+ end=+"+	oneline

" If you don't like tabs
syn match matlabTab			"\t"

" Standard numbers
syn match matlabNumber		"\<\d\+[ij]\=\>"
" floating point number, with dot, optional exponent
syn match matlabFloat		"\<\d\+\(\.\d*\)\=\([edED][-+]\=\d\+\)\=[ij]\=\>"
" floating point number, starting with a dot, optional exponent
syn match matlabFloat		"\.\d\+\([edED][-+]\=\d\+\)\=[ij]\=\>"

" Transpose character and delimiters: Either use just [...] or (...) aswell
syn match matlabDelimiter		"[][]"
"syn match matlabDelimiter		"[][()]"
syn match matlabTransposeOperator	"[])a-zA-Z0-9.]'"lc=1

syn match matlabSemicolon		";"

"syn match matlabComment			"%.*$"	contains=matlabTodo,matlabTab
syn region  matlabBlockComment		start="%{" end="%}" contains=matlabTodo,matlabTab,matlabSingleComment
syn match matlabSingleComment		"%\({\|}\)\@!.*$"	contains=matlabTodo,matlabTab  " matches % not followed by { or }
syn match octaveSingleComment		"#.*$"	contains=matlabTodo,matlabTab  " Octave, a matlab clone supports '#' for starting comments


syn keyword matlabOperator		break continue zeros round ones cross dot
syn keyword matlabOperator		divergence curl gradient factor gcd lcm 
syn keyword matlabOperator		ceil floor size clear zeros eye
syn keyword matlabOperator		mean std cov var median mode range lqr
syn keyword matlabOperator		cell mod 

syn keyword matlabFunction		error eval function vargin nargin nargout endfunction
syn keyword matlabFunction		persistent global 
syn keyword matlabFunction		citation argv quit atexit isa length
syn keyword matlabFunction		isnull isempty isnumeric isfloat isreal iscomplex 
syn keyword matlabFunction		ismatrix isvector isrow iscolumn isscalar issymmetric
syn keyword matlabFunction		ishermitian isdefinite islogical isprime
syn keyword matlabFunction		bitset bitget bitmax bitand bitor bitxor bitcmp bitshift
syn keyword matlabFunction		char strvcat strcat cstrcat mat2str num2str int2str
syn keyword matlabFunction		strcmp strncmp strcmpi strncmpi
syn keyword matlabFunction		strtrim strtrunc findstr strchr index rindex strfind strjoin
syn keyword matlabFunction		strmatch strtok strsplit ostrsplit strread strrep substr
syn keyword matlabFunction		regexp regexpi tregexprep regexptranslate 
syn keyword matlabFunction		bin2dec dec2bin dec2hex dec2base base2dec num2hex hex2num
syn keyword matlabFunction		str2double strjust str2num toascii tolower toupper
syn keyword matlabFunction		isalnum isalpha isletter islower isupper isdigit isxdigit
syn keyword matlabFunction		ispunct isspace iscntrl isgraph isprint isascii isstrprop
syn keyword matlabFunction		struct isstruct nfields fieldnames isfield setfield getfield
syn keyword matlabFunction		rmfield orderfields substruct struct2cell
syn keyword matlabFunction		celldisp iscell cell num2cell mat2cell cellslices cellindexmat 
syn keyword matlabFunction		cellstr iscellstr cell2mat cell2struct
syn keyword matlabFunction		unique ismember union intersect setdiff setxor powerset
syn keyword matlabFunction		who whos exist clear pack type which what
syn keyword matlabFunction		addpath genpath rmpath savepath path pathdef pathsep source
syn keyword matlabFunction		fputs puts fgetl fgets fskipl fread fwrite feof ftell fseek frewind
syn keyword matlabFunction		rand randi randn rande randp randg randperm
syn keyword matlabFunction		cart2pol pol2cart cart2sph sph2cart
syn keyword matlabFunction		balance cond det eig givens planerot inv linsolve matrix_type
syn keyword matlabFunction		norm null orth mgorth pinv rank rcond trace rref svd
syn keyword matlabFunction		meansq skewness kurtosis moment quantile prctile statistics
syn keyword matlabFunction		normpdf normcdf norminv
syn keyword matlabFunction		poisspdf poisscdf poissinv
syn keyword matlabFunction		conv convn deconv conv2 polygcd residue
syn keyword matlabFunction		imread imwrite iminfo imformats imshow image imagesc
syn keyword matlabFunction		iscolormap gray2ind ind2gray rgb2ind colormap rgbplot
syn keyword matlabFunction		autumn bone colorcube cool copper flag gray hot hsv
syn keyword matlabFunction		jet lines ocean pink prism rainbow spring summer white winter
syn keyword matlabFunction		wavread wavwrite
syn keyword matlabFunction		time now ctime gmtime mktime asctime localtime strftime
syn keyword matlabFunction		clock date etime cputime is_leap_year tic toc 

syn keyword matlabImplicit		abs arg acos atan asin cos cosh exp log
syn keyword matlabImplicit		prod sum cumsum cumprod sumsq
syn keyword matlabImplicit		log10 log2 pow2 max min sign sin sinh sqrt tan reshape
syn keyword matlabImplicit		asec acsc acot tanh asinh acosh atanh asech acsch acoth atan2
syn keyword matlabImplicit		conj real imag
syn keyword matlabImplicit		printf fprintf sprintf scanf fscanf sscanf
syn keyword matlabImplicit		plot plotyy semilogx semilogy loglog bar barh
syn keyword matlabImplicit		hist stemleaf print stairs stem stem3 scatter
syn keyword matlabImplicit		pareto rose contour controurf contourc contour3
syn keyword matlabImplicit		errorbar semilogxerr semilogerry loglogerr polar
syn keyword matlabImplicit		pie pie3 quiver quiver3 compass feather pcolor area
syn keyword matlabImplicit		axis caxis xlim ylim fplot ezplot ezcontour ezcontourf ezpolar
syn keyword matlabImplicit		rectangle mesh meshc meshz hidden surf surfc surfl surfnorm
syn keyword matlabImplicit		plot3 ngrid meshgrid view ezplot3
syn keyword matlabImplicit		title legend text xlabel ylabel box grid colorbar subplot
syn keyword matlabImplicit		figure drawnow refresh close clf cla axes 
syn keyword matlabImplicit		line patch fill surface gcf gca isaxes isfigure
syn keyword matlabImplicit		findfigs findall reset hold tics
syn keyword matlabImplicit		any all xor diff isinf isnan isfinite find lookup
syn keyword matlabImplicit		reshape resize diag vec tril 
syn keyword matlabImplicit		e pi i Inf NaN eps realmax realmin
syn keyword matlabImplicit		movefile rename copyfile unlink link symlink readlink 
syn keyword matlabImplicit		mkdir rmdir mkfifo umask stat isdir readdir glob
syn keyword matlabImplicit		cd ls dir pwd matlabroot
syn keyword matlabImplicit		pause sleep system mex mexext 

" Signal Processing
syn keyword matlabFunction		fft ifft fft2 ifft2 fftn ifftn fftw fftconv fftfilt
syn keyword matlabFunction		filter filter2 freqz freqz_plot sinc unwrap
syn keyword matlabFunction		arch_fit arch_rnd arch_test arma_rnd autoreg_matrix
syn keyword matlabFunction		bartlett blackman detrend diffpara durbinlevinson
syn keyword matlabFunction		fftshift ifftshift fractdiff hamming hanning hurst
syn keyword matlabFunction		pchip periodogram sinetone sinewave spectral_adf spectral_xdf
syn keyword matlabFunction		spencer stft synthesis yulewalker


" Octave only?
syn keyword matlabFunction		logspace linspace log1p reallog realpow cbrt nthroot
syn keyword matlabFunction		chol cholinv chol2inv cholupdate cholinsert choldelete cholshift
syn keyword matlabFunction		hess lu luupdate svd_driver housh krylov
syn keyword matlabFunction		qr qrupdate qrinsert qrdelete qrshift
syn keyword matlabFunction		qz qzhess schur rsf2csf subspace
syn keyword matlabFunction		bsxfun arrayfun spfun cellfun structfun accumarray accumdim
syn keyword matlabFunction		fsolve fzero fminbnd fminunc fminsearch
syn keyword matlabFunction		spdiags speye spones sprand sprandn sprandsym full spalloc 
syn keyword matlabFunction		sparse spconvert issparse nnz nonzeros nzmax spstats spy
syn keyword matlabFunction		etree etreeplot gplot treeplot treelayout sparse_auto_mutate
syn keyword matlabFunction		amd ccolamd colperm csymamd dmperm symamd symrcm
syn keyword matlabFunction		normest nonenormest condest spparms sprank symbfact
syn keyword matlabFunction		spaugment eigs svds pcg pcr luinc
syn keyword matlabFunction		quad quad_options quadv quadl quadcc trapz cumtrapz colloc
syn keyword matlabFunction		dblquad triplequad isode isode_options glpk qp pqpnonneg
syn keyword matlabFunction		daspk daspk_options dassl dassl_options dasrt dasrt_options
syn keyword matlabFunction		ols gls lsqnonneg optimset optimget
syn keyword matlabFunction		center zscore histc nchoosek perms ranks run_count runlength
syn keyword matlabFunction		probit logit clogclog mahalanbis spearman kendall 
syn keyword matlabFunction		qqplot ppplot logistic_regression
syn keyword matlabFunction		betapdf betacdf betainv binpdf bincdf bininv 
syn keyword matlabFunction		cauchy_pdf cauchy_cdf cauchy_inv 
syn keyword matlabFunction		chi2pdf chi2cdf chi2inv 
syn keyword matlabFunction		discrete_pdf discrete_cdf discrete_inv 
syn keyword matlabFunction		empirical_pdf empirical_cdf empirical_inv 
syn keyword matlabFunction		exppdf expcdf expinv fpdf fcdf finv 
syn keyword matlabFunction		gampdf gamcdf gaminv geopdf geocdf geoinv 
syn keyword matlabFunction		hygepdf hygecdf hygeinv laplace_pdf laplace_cdf laplace_inv 
syn keyword matlabFunction		logistic_pdf logistic_cdf logistic_inv 
syn keyword matlabFunction		lognpdf logncdf logninv nbinpdf nbincdf nbininv 
syn keyword matlabFunction		stdnormal_pdf stdnormal_cdf stdnormal_inv
syn keyword matlabFunction		unidpdf unidcdf unidinv tpdf tcdf tinv
syn keyword matlabFunction		wblpdf wblcdf wblinv
syn keyword matlabFunction		anova bartlett_test cor_test f_test_regression
syn keyword matlabFunction		chisquare_test_homogeneity chisquare_test_independence
syn keyword matlabFunction		polyval polyvalm roots polyeig compan mpoles
syn keyword matlabFunction		polyfit splinefit mkpp unmkpp ppval ppder ppint ppjumps
syn keyword matlabFunction		poly polyout polyreduce
syn keyword matlabFunction		interp1 interpft spline interp2 interp3 interpn bicubic
syn keyword matlabFunction		delaunay delaunay3 delaunayn triplot trimesh trisurf
syn keyword matlabFunction		tetramesh tsearch tsearchn dsearch dsearchn
syn keyword matlabFunction		voronoi voronoin polyarea rectint linpolygon
syn keyword matlabFunction		convhull convhulln
syn keyword matlabFunction		griddata griddata3 griddatan 
syn keyword matlabFunction		contrast gmap40 brighten spinmap whitebg cmunique cmpermute
syn keyword matlabFunction		rgb2hsv hsv2rgba rgb2ntsc ntsc2rgb
syn keyword matlabFunction		lin2mu mu2lin loadaudio saveaudio playaudio record setaudio
syn keyword matlabFunction		waitbar desktop guidata guihandles isguirunning uiwait
syn keyword matlabFunction		uiresume waitfor 
syn keyword matlabFunction		usleep datenum datestr datevec addtodate
syn keyword matlabFunction		calendar weekday eomday datetick

syn keyword matlabImplicit		fliplr flipud flipdim rot90 rotdim cat horzcat vertcat
syn keyword matlabImplicit		permute ipermute circshift shift sort sortrows issorted
syn keyword matlabImplicit		sind cosd tand secd cscd cotd asind acosd 
syn keyword matlabImplicit		atand atan2d asecd acscd acotd
syn keyword matlabImplicit		getpref setpref addpref rmpref ispref prefdir preferences
syn keyword matlabImplicit		dump_prefs
syn keyword matlabImplicit		fnmatch file_in_path filesep fullfile tilde_expand
syn keyword matlabImplicit		canonicalize_file_name make_absolute_filename
syn keyword matlabImplicit		is_absolute_filename is_rooted_relative_filename
syn keyword matlabImplicit		P_tmpdir tempdir tempname recycle
syn keyword matlabImplicit		bunzip2 gzip gunzip tar untar zip unzip unpack bzip2
syn keyword matlabImplicit		gethostname unix dos perl python popen pclose popen2
syn keyword matlabImplicit		EXEC_PATH WCONTINUE WCOREDUMP WEXITSTATUS WIFCONTINUED
syn keyword matlabImplicit		WIFSIGNALED WIFSTOPPED WIFEXITED WNOHANG WSTOPSIG WTERMSIG
syn keyword matlabImplicit		WUNTRACED SIG OCTAVE_HOME OCTAVE_VERSION
syn keyword matlabImplicit		fork exec pipe dup2 waitpid fcntl kill
syn keyword matlabImplicit		getpgrp getpid geteuid getuid getegid getgid
syn keyword matlabImplicit		getenv putenv ls_command getrusage md5sum
syn keyword matlabImplicit		getpwent getpwuid getpwnam setpwent endpwent
syn keyword matlabImplicit		getgrent getgrgid getgrnam setgrent endgrent
syn keyword matlabImplicit		computer uname nproc ispc isunix ismac isieee isdeployed
syn keyword matlabImplicit		ver version compare_versions license octave_config_info
syn keyword matlabImplicit		msgbox errordlg helpdlg inputdlg questdlg warndlg pkg
syn keyword matlabImplicit		mkoctfile test assert fail
syn keyword matlabImplicit		speed demo example rundemos runtests iskeyword
syn keyword matlabImplicit		add_input_event_hook remove_input_event_hook
syn keyword matlabImplicit		missing_function_hook program_name program_invocation_name


syn match matlabError	"-\=\<\d\+\.\d\+\.[^*/\\^]"
syn match matlabError	"-\=\<\d\+\.\d\+[eEdD][-+]\=\d\+\.\([^*/\\^]\)"

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_matlab_syntax_inits")
  if version < 508
    let did_matlab_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink matlabTransposeOperator	matlabOperator
  HiLink matlabOperator			Operator
  HiLink matlabLineContinuation		Special
  HiLink matlabLabel			Label
  HiLink matlabConditional		Conditional
  HiLink matlabRepeat			Repeat
  HiLink matlabTodo			Todo
  HiLink matlabString			String
  HiLink matlabDelimiter		Identifier
  HiLink matlabTransposeOther		Identifier
  HiLink matlabNumber			Number
  HiLink matlabFloat			Float
  HiLink matlabFunction			Function
  HiLink matlabError			Error
  HiLink matlabImplicit			matlabStatement
  HiLink matlabStatement		Statement
  HiLink matlabSemicolon		SpecialChar
"  HiLink matlabComment			Comment
  HiLink matlabSingleComment		Comment
  HiLink matlabBlockComment		Comment

  HiLink matlabArithmeticOperator	matlabOperator
  HiLink matlabRelationalOperator	matlabOperator
  HiLink matlabLogicalOperator		matlabOperator

"
  HiLink octaveLogicalOperator		matlabOperator
  HiLink octaveRelationalOperator	matlabOperator
  HiLink octaveArithmeticOperator	matlabOperator
  HiLink octaveString			String

"optional highlighting
  "HiLink matlabIdentifier		Identifier
  "HiLink matlabTab			Error

  delcommand HiLink
endif

" If matchit.vim is installed, this will allow using the  %  command to jump 
" between opening and closing block comments markers.
let b:match_words="%{:%}"

let b:current_syntax = "matlab"

"EOF	vim: ts=8 noet tw=100 sw=8 sts=0
