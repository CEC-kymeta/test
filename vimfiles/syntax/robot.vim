" Vim syntax file
" Language:	Robot Framework
" Maintainer:	
" Updated:	2017
"

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" Highlight erroneous whitespace:
    let robot_highlight_space_errors = 1

syntax clear

syn region   robotSection start="^\*\*\*" end="\*\*\*" oneline
syn region   robotVariable start="[\$@&%]{"ms=e+1 end="}"me=s-1

syn region  robotString start=/\w*}\s\{2,\}/ms=e+1 end="$"me=s-1

"syn region  robotTest start=/^/ms=e+1 end=/\s\{2,\}/me=s-1 oneline
"syn region  robotTest start=/^/ms=e+1 end=/$/me=s-1 oneline
"syn match robotTest "^\w\(\w\+\s\=\)\+\s\{2,\}"
syn match robotTest "^\w\(\w\+\s\=\)\+$" contains=robotKeyword
syn match robotTest "^\w\+[:-\s]"

syn match robotTest "^\w.*\s\{2,\}"
"syn match robotTest "^\w.*$"
"syn match robotTest "^\w.*$"

"syn match robotTest "^\(\%[\w:-_]\+\s\=\)\+\s\{2,\}"
"syn match robotTest "^\(\%[\w:-_]\+\s\=\)\+$"
"syn match robotTest "^\(\w\+\s\=\)\+:"



syn region robotStatement start="\["ms=e+1 end="\]"me=s-1
syn keyword robotKeyword        Library Resource Variables
syn keyword robotKeyword        Given When Then And But
syn match robotKeyword        "Test Setup"
syn match robotKeyword        "Test Teardown"
syn match robotKeyword        "Suite Setup"
syn match robotKeyword        "Suite Teardown"
"syn keyword robotOperator       \*\*\*
syn keyword robotOperator       ${ { }
syn match   robotOperator       /[\[\]]/ contained
"syn match   robotComment       "#.*$" contains=robotTodo,@Spell
syn match   robotComment        "#.*$" contains=robotTodo
syn keyword robotTodo           TODO FIXME XXX contained

syn match robotTag "^\s*\[.*\]" contains=robotKeyword,robotKeywordDoc,robotOperator

syn keyword robotKeywordDoc     Documentation contained
syn match   robotDoc            "^Documentation\s.*$" contains=robotKeywordDoc
syn region  robotDoc            start="^\s*\[Documentation\]" end="$" contains=robotKeywordDoc,robotTag
syn match   robotDoc            "^\s*\.\.\..*$"

if exists("robot_highlight_space_errors")
  " trailing whitespace
  syn match   robotSpaceError   display excludenl "\S\s\+$"ms=s+1
  " mixed tabs and spaces
  syn match   robotSpaceError   display " \+\t"
  syn match   robotSpaceError   display "\t\+ "
endif


if version <= 508
command -nargs=+ HiLink hi link <args>
else
command -nargs=+ HiLink hi def link <args>
endif

" The default methods for highlighting.  Can be overridden later
HiLink robotSection             StorageClass
HiLink robotStatement           Statement
HiLink robotKeyword             Statement
HiLink robotKeywordDoc          Statement
HiLink robotTag                 Statement
HiLink robotVariable            Function
HiLink robotTest                Function
HiLink robotString              String
"HiLink pythonEscape            Special
HiLink robotOperator            Operator
"HiLink robotSpecial             Special
HiLink robotComment             Comment
HiLink robotDoc                 Comment
HiLink robotTodo                Todo
HiLink robotSpaceError          Error

delcommand HiLink

let b:current_syntax = "robot"
set foldmethod=indent
set nofoldenable
set shiftwidth=4

" vim: ts=8
