#!/bin/bash
#
## change directory to the rrdtool script dir
#DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
#workdir=${2:-'.'} 

DEBUG=0
workdir='.'
timescale="minute"

while getopts "hVt:w:" arg; do
  case $arg in
    h)
      echo "Usage: graph_rrd.sh [-V] [-w workdir] [-t minute|hour|month]"
      exit
      ;;
    w)
      workdir=$OPTARG
      ;;
    t)
      timescale=$OPTARG
      ;;
    V)
      DEBUG=1
      ;;
  esac
done


cd "$workdir"
source "rrd_vars.sh"


if [ $DEBUG -ne 0 ]; then
   echo "workdir = $workdir"
   echo "timescale = $timescale"
fi

# Some useful resources:
# https://oss.oetiker.ch/rrdtool/gallery/index.en.html
# http://rrdtool.vandenbogaerdt.nl/tutorial/graph.php
# https://oss.oetiker.ch/rrdtool/doc/rrdgraph.en.html
#

lw=2
h=100
short=60
w=785
blue='0000FF'
dblu='00007F'
red='FF0000'
grn='00FF00'
dgrn='00AF00'
blk='000000'
dred='8F0000'
orng='F3A605'
lgry='E8E8E8'
yel='DADA00'
pur='7F00FF'

echo_debug () {
   result="$1"
   if [ $DEBUG -ne 0 ]; then
      echo "$result"
   fi
}
echo_result () {
   result="$1"
   if [ $DEBUG -ne 0 ]; then
      echo "$result"
   else
      echo "$result" | grep -v "[0-9]x[0-9]"
   fi
}

read -d '' nl <<- \EOF

EOF

read -d '' config <<- \EOF
   --slope-mode 
   --font DEFAULT:7: 
   --vertical-label latency(ms) 
   --lower-limit 0 
   --rigid 
EOF
nl=$'\n'
config="-w $w -h $h${nl}$config ${nl}-c BACK#$lgry -c CANVAS#$lgry -c MGRID#$blk -c GRID#$blk"
#--alt-y-grid \
#echo "config(array) = ${config[@]}"
if [ $DEBUG -ne 0 ]; then
   echo ""
   echo "config = $config"
   echo ""
fi

#   COMMENT:"pkt loss\\:" 
read -d '' pl_legend <<- \EOF
   AREA:PLNone#108010A0:0%
     AREA:PL10#A0D000A0:1-10%
     AREA:PL25#E0E000A0:10-25%
     AREA:PL50#F08000A0:25-50%
    AREA:PL100#FF0000A0:50-100%\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t
EOF
if [ $DEBUG -ne 0 ]; then
   echo "pl_legend = $pl_legend"
   echo ""
fi

read -d '' config_pl <<- \EOF
   CDEF:scaled_pl=packetloss,0.01,* \
   CDEF:PLNone=packetloss,0,0,LIMIT,UN,UNKN,INF,IF 
   CDEF:PL10=packetloss,1,10,LIMIT,UN,UNKN,INF,IF 
   CDEF:PL25=packetloss,10,25,LIMIT,UN,UNKN,INF,IF 
   CDEF:PL50=packetloss,25,50,LIMIT,UN,UNKN,INF,IF 
   CDEF:PL100=packetloss,50,100,LIMIT,UN,UNKN,INF,IF 
EOF
if [ $DEBUG -ne 0 ]; then
   echo "config_pl = $config_pl"
   echo ""
fi

#read -d '' gp_rtt <<- \EOF
read -a gp_rtt <<- \EOF
   GPRINT:roundtrip:LAST:Cur\\:%5.2lf \
   GPRINT:roundtrip:AVERAGE:Avg\\:%5.2lf \
   GPRINT:roundtrip:MAX:Max\\:%5.2lf \
   GPRINT:roundtrip:MIN:Min\\:%5.2lf\\t\\t\\n \
EOF
if [ $DEBUG -ne 0 ]; then
   echo "gp_rtt = ${gp_rtt[@]}"
   echo ""
fi

read -a gp_pl <<- \EOF
   GPRINT:packetloss:LAST:Cur\\:%5.2lf \
   GPRINT:packetloss:AVERAGE:Avg\\:%5.2lf \
   GPRINT:packetloss:MAX:Max\\:%5.2lf \
   GPRINT:packetloss:MIN:Min\\:%5.2lf\\t\\t\\n \
EOF
if [ $DEBUG -ne 0 ]; then
   echo "gp_pl = ${gp_pl[@]}"
   echo ""
fi

#    --x-grid HOUR:2:DAY:1:DAY:1:86400:%A 
#    --start end-7d --end now 
read -d '' x_minute <<- \EOF
    --start -86400 --end now 
    --x-grid MINUTE:10:HOUR:1:MINUTE:120:0:%R 
EOF
if [ $DEBUG -ne 0 ]; then
   echo "x_minute = $x_minute"
   echo ""
fi

read -d '' x_hour <<- \EOF
    --x-grid HOUR:2:DAY:1:DAY:1:86400:%A 
    --start end-7d --end now 
EOF
if [ $DEBUG -ne 0 ]; then
   echo "x_hour = $x_hour"
   echo ""
fi

#    --x-grid HOUR:2:DAY:1:DAY:1:86400:%A 
read -d '' x_month <<- \EOF
    --start end-30d --end now 
EOF
if [ $DEBUG -ne 0 ]; then
   echo "x_month = $x_hour"
   echo ""
fi

if [ $timescale == "minute" ]; then
   x_ts=$x_minute
   str_ts="(24 hours)"
   ext_ts=""
elif [ $timescale == "hour" ]; then
   x_ts=$x_hour
   str_ts="(5 days)"
   ext_ts="_hr"
elif [ $timescale == "month" ]; then
   x_ts=$x_month
   str_ts="(30 days)"
   ext_ts="_mo"
else
   echo "ERROR: Unknown timescale option"
   exit
fi

#################  iperf ########################
echo_debug "============== AWS ping $str_ts =============="
result=$($rrdtool graph $htmldir/aws_ping$ext_ts.png \
$config \
--lower-limit -0.1 --upper-limit 2500 --rigid \
--right-axis 0.04:0 \
--right-axis-label "Packet Loss (%)" \
--title "AWS (iperf server) Pings $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:roundtrip=aws_ping.rrd:rtt:MAX \
DEF:packetloss=aws_ping.rrd:pl:MIN \
CDEF:scaled_pl=packetloss,25,* \
AREA:roundtrip#$blue:"latency (ms)" \
${gp_rtt[@]} \
LINE1:scaled_pl#$red:"packet loss" \
${gp_pl[@]} ) 
echo_result "$result"

#COMMENT:"pkt loss\:" \
#$pl_legend)

echo_debug "============== iperf $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/iperf$ext_ts.png \
$config \
--vertical-label "Kb/s" \
--lower-limit -0.1 --upper-limit 2500 --rigid \
--title "Throughput $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:rate=iperf.rrd:rate:AVERAGE \
LINE$lw:rate#$blue:"rate (Kb/s)" \
GPRINT:rate:LAST:" Cur\\:%5.2lf" \
GPRINT:rate:AVERAGE:Avg\\:%5.2lf \
GPRINT:rate:MAX:Max\\:%5.2lf \
GPRINT:rate:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

#################  Latency Files ########################
echo_debug "============== latency files $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/latency_files$ext_ts.png \
$config \
--vertical-label "sec" \
--title "Time to Download $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:1k=latency_files.rrd:1k:AVERAGE \
DEF:10k=latency_files.rrd:10k:AVERAGE \
DEF:100k=latency_files.rrd:100k:AVERAGE \
DEF:1M=latency_files.rrd:1M:AVERAGE \
LINE$lw:1k#$blue:"1kByte file Delay (sec)" \
GPRINT:1k:LAST:"  Cur\\:%5.2lf" \
GPRINT:1k:AVERAGE:Avg\\:%5.2lf \
GPRINT:1k:MAX:Max\\:%5.2lf \
GPRINT:1k:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:10k#$grn:"10kByte file delay (sec)" \
GPRINT:10k:LAST:" Cur\\:%5.2lf" \
GPRINT:10k:AVERAGE:Avg\\:%5.2lf \
GPRINT:10k:MAX:Max\\:%5.2lf \
GPRINT:10k:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:100k#$orng:"100kByte file delay (sec)" \
GPRINT:100k:LAST:"Cur\\:%5.2lf" \
GPRINT:100k:AVERAGE:Avg\\:%5.2lf \
GPRINT:100k:MAX:Max\\:%5.2lf \
GPRINT:100k:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:1M#$dred:"1MByte file delay (sec)" \
GPRINT:1M:LAST:"  Cur\\:%5.2lf" \
GPRINT:1M:AVERAGE:Avg\\:%5.2lf \
GPRINT:1M:MAX:Max\\:%5.2lf \
GPRINT:1M:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

#################  kModem ########################
echo_debug "============== kModem $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/kmodem_ping$ext_ts.png \
$config \
--lower-limit -0.1 --upper-limit 10 --rigid \
--right-axis 10:0 \
--right-axis-label "Packet Loss (%)" \
--title "Modem Pings $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:roundtrip=kmodem_ping.rrd:rtt:MAX \
DEF:packetloss=kmodem_ping.rrd:pl:MIN \
CDEF:scaled_pl=packetloss,1,* \
AREA:roundtrip#$blue:"latency (ms)" \
${gp_rtt[@]} \
LINE1:scaled_pl#$red:"packet loss" \
${gp_pl[@]} ) 
echo_result "$result"

echo_debug "============== kModem Status $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_status$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Status Val" \
--lower-limit -0.1 --upper-limit 1 --rigid \
--title "Modem Status $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:network=kmodem_status.rrd:network:MIN \
LINE$lw:network#$dred:"network" \
GPRINT:network:LAST:Cur\\:%5.2lf \
GPRINT:network:AVERAGE:Avg\\:%5.2lf \
GPRINT:network:MAX:Max\\:%5.2lf \
GPRINT:network:MIN:Min\\:%5.2lf\\t\\t )
echo_result "$result"

echo_debug "============== kModem Tx/Rx $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_txrx$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Status Val" \
--lower-limit -0.1 --upper-limit 1 --rigid \
--title "Modem Status $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:sat_rx=kmodem_status.rrd:satellite_rx:MIN \
DEF:sat_tx=kmodem_status.rrd:satellite_tx:MIN \
LINE$lw:sat_rx#$grn:"sat RX" \
GPRINT:sat_rx:LAST:Cur\\:%5.2lf \
GPRINT:sat_rx:AVERAGE:Avg\\:%5.2lf \
GPRINT:sat_rx:MAX:Max\\:%5.2lf \
GPRINT:sat_rx:MIN:Min\\:%5.2lf\\t\\t \
LINE$lw:sat_tx#$orng:"Sat TX" \
GPRINT:sat_tx:LAST:Cur\\:%5.2lf \
GPRINT:sat_tx:AVERAGE:Avg\\:%5.2lf \
GPRINT:sat_tx:MAX:Max\\:%5.2lf \
GPRINT:sat_tx:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kModem Temperature $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_temp$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Temperature (C)" \
--lower-limit 0 \
--rigid \
--title "Modem Internal Temperature (C) $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:tavg=kmodem_status.rrd:temperature:AVERAGE \
LINE$lw:tavg#$blue:"temp avg" \
GPRINT:tavg:LAST:Cur\\:%5.2lf \
GPRINT:tavg:AVERAGE:Avg\\:%5.2lf \
GPRINT:tavg:MAX:Max\\:%5.2lf \
GPRINT:tavg:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

#echo_debug "============== kModem Uptime $str_ts =============="
#result=$($rrdtool graph $htmldir/kmodem_uptime$ext_ts.png \
#-w $w -h $short -a PNG \
#-c BACK#$lgry -c CANVAS#$lgry \
#--slope-mode \
#--font DEFAULT:7: \
#--vertical-label "Uptime (Sec)" \
#--lower-limit 0 \
#--rigid \
#--title "Modem Uptime $str_ts" \
#--watermark "`date`" \
#$x_ts \
#DEF:uptime=kmodem_status.rrd:uptime:AVERAGE \
#LINE$lw:uptime#$blue:"uptime" \
#GPRINT:uptime:LAST:Cur\\:%5.2lf \
#GPRINT:uptime:AVERAGE:Avg\\:%5.2lf \
#GPRINT:uptime:MAX:Max\\:%5.2lf \
#GPRINT:uptime:MIN:Min\\:%5.2lf\\t\\t\\n )
#echo_result "$result"

#echo_debug "============== kModem Network Uptime $str_ts =============="
#result=$($rrdtool graph $htmldir/kmodem_netupt$ext_ts.png \
#-w $w -h $short -a PNG \
#-c BACK#$lgry -c CANVAS#$lgry \
#--slope-mode \
#--font DEFAULT:7: \
#--vertical-label "Uptime (Sec)" \
#--lower-limit 0 \
#--rigid \
#--title "Network Uptime $str_ts" \
#--watermark "`date`" \
#$x_ts \
#DEF:network=kmodem_status.rrd:network_upt:MAX \
#LINE$lw:network#$dred:"network" \
#GPRINT:network:LAST:Cur\\:%5.2lf \
#GPRINT:network:AVERAGE:Avg\\:%5.2lf \
#GPRINT:network:MAX:Max\\:%5.2lf \
#GPRINT:network:MIN:Min\\:%5.2lf\\n )
#echo_result "$result"

echo_debug "============== kModem CPU $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_cpu$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "CPU (%)" \
--lower-limit -1 --upper-limit 100 \
--rigid \
--title "Modem CPU $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:cpu_max=kmodem_status.rrd:cpu:MAX \
DEF:cpu_avg=kmodem_status.rrd:cpu:AVERAGE \
DEF:cpu_min=kmodem_status.rrd:cpu:MIN \
LINE$lw:cpu_avg#$blue:"CPU Avg" \
GPRINT:cpu_avg:LAST:Cur\\:%5.2lf \
GPRINT:cpu_avg:AVERAGE:Avg\\:%5.2lf \
GPRINT:cpu_avg:MAX:Max\\:%5.2lf \
GPRINT:cpu_avg:MIN:Min\\:%5.2lf\\t\\t )
echo_result "$result"

echo_debug "============== kModem Frequency $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_f$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Frequency (GHz)" \
--title "Modem Frequency $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:rx_freq=kmodem_status.rrd:downlink_freq:AVERAGE \
LINE$lw:rx_freq#$grn:"Downlink Freq" \
GPRINT:rx_freq:LAST:"Cur\\:%5.4lf GHz\\n" )
echo_result "$result"

echo_debug "============== kModem Symbol Rate $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_ksym$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Symbol Rate$nl(ksym/s)" \
--lower-limit 0 \
--rigid \
--title "Modem Symbol Rate $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:symbol_rate=kmodem_status.rrd:symbol_rate:AVERAGE \
LINE$lw:symbol_rate#$grn:"RX Symbol Rate" \
GPRINT:symbol_rate:LAST:"Cur\\:%5.3lf ksymb/s\n" )
echo_result "$result"

echo_debug "============== kModem Rolloff & Beam $str_ts =============="
result=$($rrdtool graph $htmldir/kmodem_rollbeam$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Rolloff (%)" \
--right-axis-label "BEAM (#)" \
--right-axis 0.1:0 \
--lower-limit -1 --upper-limit 100 \
--rigid \
--title "Modem Rolloff and Beam Num $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:rolloff=kmodem_status.rrd:rolloff:AVERAGE \
DEF:beam_id=kmodem_status.rrd:beam_id:AVERAGE \
CDEF:scaled_beam=beam_id,10,* \
LINE$lw:rolloff#$blue:"Rolloff" \
GPRINT:rolloff:LAST:"Cur\\:%5.1lf%%" \
GPRINT:rolloff:AVERAGE:"Avg\\:%5.1lf%%" \
GPRINT:rolloff:MAX:"Max\\:%5.1lf%%" \
GPRINT:rolloff:MIN:"Min\\:%5.1lf%%\\t\\n" \
LINE$lw:scaled_beam#$grn:"Beam ID" \
GPRINT:beam_id:LAST:"Cur\\:%3.0lf\n" )
echo_result "$result"

echo_debug "============== kModem RF Power $str_ts =============="
#--lower-limit -200 --upper-limit 200 \
result=$($rrdtool graph $htmldir/kmodem_pwr$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Power (dBm)" \
--right-axis-label "SNR (dB)" \
--right-axis 1:0 \
--rigid \
--title "RF Power $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:rx_comp_pwr=kmodem_status.rrd:rx_comp_pwr:AVERAGE \
DEF:rx_snr=kmodem_status.rrd:rx_snr:AVERAGE \
DEF:tx_pwr=kmodem_status.rrd:tx_pwr:AVERAGE \
LINE$lw:rx_comp_pwr#$blue:"RX comp Pwr" \
GPRINT:rx_comp_pwr:LAST:"Cur\\:%5.1lf dBm" \
GPRINT:rx_comp_pwr:AVERAGE:"Avg\\:%5.1lf dBm" \
GPRINT:rx_comp_pwr:MAX:"Max\\:%5.1lf dBm" \
GPRINT:rx_comp_pwr:MIN:"Min\\:%5.1lf dBm\n" \
LINE$lw:rx_snr#$grn:"RX SNR     " \
GPRINT:rx_snr:LAST:"Cur\\:%5.1lf dB" \
GPRINT:rx_snr:AVERAGE:"Avg\\:%5.1lf dB" \
GPRINT:rx_snr:MAX:"Max\\:%5.1lf dB" \
GPRINT:rx_snr:MIN:"Min\\:%5.1lf dB\n" \
LINE$lw:tx_pwr#$dred:"TX Pwr     " \
GPRINT:tx_pwr:LAST:"Cur\\:%5.1lf dBm" \
GPRINT:tx_pwr:AVERAGE:"Avg\\:%5.1lf dBm" \
GPRINT:tx_pwr:MAX:"Max\\:%5.1lf dBm" \
GPRINT:tx_pwr:MIN:"Min\\:%5.1lf dBm\n" )
echo_result "$result"

#
# ------------ Antenna (ODU) ------------
# 
echo_debug "============== kAnt $str_ts ============================="
result=$($rrdtool graph $htmldir/kant_ping$ext_ts.png \
$config \
--lower-limit -0.1 --upper-limit 10 --rigid \
--right-axis 10:0 \
--right-axis-label "Packet Loss (%)" \
--title "kant ping $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:roundtrip=kant_ping.rrd:rtt:MAX \
DEF:packetloss=kant_ping.rrd:pl:MIN \
CDEF:scaled_pl=packetloss,1,* \
AREA:roundtrip#$blue:"latency(ms)" \
${gp_rtt[@]} \
LINE1:scaled_pl#$red:"packet loss" \
${gp_pl[@]} ) 
echo_result "$result"

echo_debug "============== kAnt Internal Temp $str_ts ==============="
result=$($rrdtool graph $htmldir/kant_int_temp$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Temperature (C)" \
--lower-limit 0 \
--rigid \
--title "kant Internal Temperature (C) $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:tmax=kant_status.rrd:int_temp:MAX \
DEF:tmin=kant_status.rrd:int_temp:MIN \
DEF:tavg=kant_status.rrd:int_temp:AVERAGE \
LINE$lw:tmax#$dred:"temp max" \
GPRINT:tmax:LAST:Cur\\:%5.2lf \
GPRINT:tmax:AVERAGE:Avg\\:%5.2lf \
GPRINT:tmax:MAX:Max\\:%5.2lf \
GPRINT:tmax:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:tmin#$blue:"temp min" \
GPRINT:tmin:LAST:Cur\\:%5.2lf \
GPRINT:tmin:AVERAGE:Avg\\:%5.2lf \
GPRINT:tmin:MAX:Max\\:%5.2lf \
GPRINT:tmin:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:tavg#$orng:"temp avg" \
GPRINT:tavg:LAST:Cur\\:%5.2lf \
GPRINT:tavg:AVERAGE:Avg\\:%5.2lf \
GPRINT:tavg:MAX:Max\\:%5.2lf \
GPRINT:tavg:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kAnt Quad Temp $str_ts ==============="
result=$($rrdtool graph $htmldir/kant_quad_temp$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Temperature (C)" \
--lower-limit 0 \
--rigid \
--title "kant Quad Temperature (C) $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:q1max=kant_status.rrd:int_temp:MAX \
DEF:q2max=kant_status.rrd:int_temp:MAX \
DEF:q3max=kant_status.rrd:int_temp:MAX \
DEF:q4max=kant_status.rrd:int_temp:MAX \
LINE$lw:q1max#$red:"q1 max" \
GPRINT:q1max:LAST:Cur\\:%5.2lf \
GPRINT:q1max:AVERAGE:Avg\\:%5.2lf \
GPRINT:q1max:MAX:Max\\:%5.2lf \
GPRINT:q1max:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:q2max#$blue:"q2 max" \
GPRINT:q2max:LAST:Cur\\:%5.2lf \
GPRINT:q2max:AVERAGE:Avg\\:%5.2lf \
GPRINT:q2max:MAX:Max\\:%5.2lf \
GPRINT:q2max:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:q3max#$grn:"q3 max" \
GPRINT:q3max:LAST:Cur\\:%5.2lf \
GPRINT:q3max:AVERAGE:Avg\\:%5.2lf \
GPRINT:q3max:MAX:Max\\:%5.2lf \
GPRINT:q3max:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:q4max#$blk:"q4 max" \
GPRINT:q4max:LAST:Cur\\:%5.2lf \
GPRINT:q4max:AVERAGE:Avg\\:%5.2lf \
GPRINT:q4max:MAX:Max\\:%5.2lf \
GPRINT:q4max:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"


echo_debug "============== kAnt Internal Humidity $str_ts ==========="
result=$($rrdtool graph $htmldir/kant_int_hum$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Humidity" \
--lower-limit 0 \
--rigid \
--title "kant Internal Humidity $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:hmax=kant_status.rrd:int_hum:MAX \
DEF:hmin=kant_status.rrd:int_hum:MIN \
DEF:havg=kant_status.rrd:int_hum:AVERAGE \
LINE$lw:hmax#$dred:"humidity max" \
GPRINT:hmax:LAST:Cur\\:%5.2lf \
GPRINT:hmax:AVERAGE:Avg\\:%5.2lf \
GPRINT:hmax:MAX:Max\\:%5.2lf \
GPRINT:hmax:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:hmin#$blue:"humidity min" \
GPRINT:hmin:LAST:Cur\\:%5.2lf \
GPRINT:hmin:AVERAGE:Avg\\:%5.2lf \
GPRINT:hmin:MAX:Max\\:%5.2lf \
GPRINT:hmin:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:havg#$orng:"humidity avg" \
GPRINT:havg:LAST:Cur\\:%5.2lf \
GPRINT:havg:AVERAGE:Avg\\:%5.2lf \
GPRINT:havg:MAX:Max\\:%5.2lf \
GPRINT:havg:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kAnt Aperture $str_ts ===================="
#--upper-limit 50 \
result=$($rrdtool graph $htmldir/kant_aperture$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Voltage (V)" \
--right-axis 0.01:0 \
--right-axis-label "Current (A)" \
--lower-limit 0 \
--rigid \
--title "kant Aperature Current, Voltage $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:aimax=kant_status.rrd:aperture_i:MAX \
DEF:aimin=kant_status.rrd:aperture_i:MIN \
CDEF:scaled_aimax=aimax,100,* \
CDEF:scaled_aimin=aimin,100,* \
DEF:avmax=kant_status.rrd:aperture_v:MAX \
DEF:avmin=kant_status.rrd:aperture_v:MIN \
LINE$lw:avmax#$red:"Vmax" \
GPRINT:avmax:LAST:Cur\\:%5.2lf \
GPRINT:avmax:AVERAGE:Avg\\:%5.2lf \
GPRINT:avmax:MAX:Max\\:%5.2lf \
GPRINT:avmax:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:avmin#$blue:"Vmin" \
GPRINT:avmin:LAST:Cur\\:%5.2lf \
GPRINT:avmin:AVERAGE:Avg\\:%5.2lf \
GPRINT:avmin:MAX:Max\\:%5.2lf \
GPRINT:avmin:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:scaled_aimax#$dred:"Imax" \
GPRINT:aimax:LAST:Cur\\:%5.2lf \
GPRINT:aimax:AVERAGE:Avg\\:%5.2lf \
GPRINT:aimax:MAX:Max\\:%5.2lf \
GPRINT:aimax:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:scaled_aimin#$dblu:"Imin" \
GPRINT:aimin:LAST:Cur\\:%5.2lf \
GPRINT:aimin:AVERAGE:Avg\\:%5.2lf \
GPRINT:aimin:MAX:Max\\:%5.2lf \
GPRINT:aimin:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kAnt Input $str_ts ===================="
#--upper-limit 50 \
result=$($rrdtool graph $htmldir/kant_in$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Voltage (V)" \
--right-axis 0.01:0 \
--right-axis-label "Current (A)" \
--lower-limit 0 \
--rigid \
--title "kant Input Current, Voltage $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:aimax=kant_status.rrd:aperture_i:MAX \
DEF:iimax=kant_status.rrd:in_i:MAX \
DEF:iimin=kant_status.rrd:in_i:MIN \
CDEF:scaled_iimax=iimax,100,* \
CDEF:scaled_iimin=iimin,100,* \
DEF:ivmax=kant_status.rrd:in_v:MAX \
DEF:ivmin=kant_status.rrd:in_v:MIN \
LINE4:ivmin#$blue:"Vmin" \
GPRINT:ivmin:LAST:Cur\\:%5.2lf \
GPRINT:ivmin:AVERAGE:Avg\\:%5.2lf \
GPRINT:ivmin:MAX:Max\\:%5.2lf \
GPRINT:ivmin:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE1:ivmax#$red:"Vmax" \
GPRINT:ivmax:LAST:Cur\\:%5.2lf \
GPRINT:ivmax:AVERAGE:Avg\\:%5.2lf \
GPRINT:ivmax:MAX:Max\\:%5.2lf \
GPRINT:ivmax:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE4:scaled_iimin#$dblu:"Imin" \
GPRINT:iimin:LAST:Cur\\:%5.2lf \
GPRINT:iimin:AVERAGE:Avg\\:%5.2lf \
GPRINT:iimin:MAX:Max\\:%5.2lf \
GPRINT:iimin:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE1:scaled_iimax#$dred:"Imax" \
GPRINT:iimax:LAST:Cur\\:%5.2lf \
GPRINT:iimax:AVERAGE:Avg\\:%5.2lf \
GPRINT:iimax:MAX:Max\\:%5.2lf \
GPRINT:iimax:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kant Status $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/kant_status$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "power fail warn" \
--right-axis 100:0 \
--right-axis-label "status (% good)" \
--lower-limit 0 \
--rigid \
--title "kant status, power fail $str_ts" \
--watermark "`date`" \
$x_ts \
--alt-y-grid \
DEF:kstatus=kant_status.rrd:status:AVERAGE \
CDEF:kstatus_percent=kstatus,100,* \
DEF:pfwarn=kant_status.rrd:pfwarn:AVERAGE \
CDEF:Sgood=kstatus,0.95001,1,LIMIT,UN,UNKN,INF,IF \
CDEF:Sunkn=kstatus,.10001,0.95,LIMIT,UN,UNKN,INF,IF \
CDEF:Spoor=kstatus,0,0.1,LIMIT,UN,UNKN,INF,IF \
COMMENT:"Status\:" \
AREA:Sgood#$grn:"96-100% (good)" \
AREA:Sunkn#$orng:11-95% \
AREA:Spoor#$red:"0-10% (poor)" \
GPRINT:kstatus_percent:LAST:Cur\\:%5.0lf%%\\t\\t\\t\\t\\t\\t\\t\\n \
LINE$lw:pfwarn#$blue:"power fail warning (% of reports)" \
GPRINT:pfwarn:LAST:Cur\\:%5.2lf \
GPRINT:pfwarn:AVERAGE:Avg\\:%5.2lf \
GPRINT:pfwarn:MAX:Max\\:%5.2lf \
GPRINT:pfwarn:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

#################  Kant Tracking RRD ########################

echo_debug "============== kant Tracking: AGC $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/kant_agc$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "AGC" \
--rigid \
--title "kant AGC $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:agc=kant_tracking.rrd:agc:AVERAGE \
LINE$lw:agc#$blue:"AGC" \
GPRINT:agc:LAST:Cur\\:%5.2lf \
GPRINT:agc:AVERAGE:Avg\\:%5.2lf \
GPRINT:agc:MAX:Max\\:%5.2lf \
GPRINT:agc:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== kant Tracking: pl_sync $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/kant_pl_sync$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "PL Sync" \
--lower-limit 0 --upper-limit 1 \
--rigid \
--title "kant PL Sync $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:pl_sync=kant_tracking.rrd:pl_sync:AVERAGE \
LINE$lw:pl_sync#$blue:"PL Sync" \
GPRINT:pl_sync:LAST:Cur\\:%5.2lf \
GPRINT:pl_sync:AVERAGE:Avg\\:%5.2lf \
GPRINT:pl_sync:MAX:Max\\:%5.2lf \
GPRINT:pl_sync:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"


echo_debug "============== kant Tracking: Es/N0 $str_ts =============="
result=$($rrdtool graph $htmldir/kant_esn0$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Es/N0" \
--title "kant Es/N0 $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:pl_es_n0=kant_tracking.rrd:pl_es_n0:AVERAGE \
DEF:r_es_n0=kant_tracking.rrd:r_es_n0:AVERAGE \
LINE$lw:pl_es_n0#$blue:"PL Es/N0" \
GPRINT:pl_es_n0:LAST:Cur\\:%5.2lf \
GPRINT:pl_es_n0:AVERAGE:Avg\\:%5.2lf \
GPRINT:pl_es_n0:MAX:Max\\:%5.2lf \
GPRINT:pl_es_n0:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:r_es_n0#$grn:"R Es/N0" \
GPRINT:r_es_n0:LAST:" Cur\\:%5.2lf" \
GPRINT:r_es_n0:AVERAGE:Avg\\:%5.2lf \
GPRINT:r_es_n0:MAX:Max\\:%5.2lf \
GPRINT:r_es_n0:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Server PS Monitor $str_ts =============="
## Graph for last 24 hours 
result=$($rrdtool graph $htmldir/psmon$ext_ts.png \
-w 785 -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Num Processes" \
--lower-limit -1 \
--watermark "`date`" \
$x_ts \
DEF:driver=psmon.rrd:driver:MAX \
DEF:chrome=psmon.rrd:chrome:MAX \
DEF:xvfb=psmon.rrd:xvfb:MAX \
DEF:latency=psmon.rrd:latency:MAX \
DEF:ant=psmon.rrd:ant:MAX \
DEF:modem=psmon.rrd:modem:MAX \
LINE$lw:driver#$blue:"chromedriver           " \
GPRINT:driver:LAST:"Cur\\:%5.2lf" \
GPRINT:driver:AVERAGE:Avg\\:%5.2lf \
GPRINT:driver:MAX:Max\\:%5.2lf \
GPRINT:driver:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:chrome#$grn:"Browsers               " \
GPRINT:chrome:LAST:"Cur\\:%5.2lf" \
GPRINT:chrome:AVERAGE:Avg\\:%5.2lf \
GPRINT:chrome:MAX:Max\\:%5.2lf \
GPRINT:chrome:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:xvfb#$dred:"Xvfb                   " \
GPRINT:xvfb:LAST:"Cur\\:%5.2lf" \
GPRINT:xvfb:AVERAGE:Avg\\:%5.2lf \
GPRINT:xvfb:MAX:Max\\:%5.2lf \
GPRINT:xvfb:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:latency#$yel:"update_latency_files.py" \
GPRINT:latency:LAST:"Cur\\:%5.2lf" \
GPRINT:latency:AVERAGE:Avg\\:%5.2lf \
GPRINT:latency:MAX:Max\\:%5.2lf \
GPRINT:latency:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:ant#$pur:"ant_requests.py        " \
GPRINT:ant:LAST:"Cur\\:%5.2lf" \
GPRINT:ant:AVERAGE:Avg\\:%5.2lf \
GPRINT:ant:MAX:Max\\:%5.2lf \
GPRINT:ant:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:modem#$orng:"modem_temp_*.py        " \
GPRINT:modem:LAST:"Cur\\:%5.2lf" \
GPRINT:modem:AVERAGE:Avg\\:%5.2lf \
GPRINT:modem:MAX:Max\\:%5.2lf \
GPRINT:modem:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Server Memory $str_ts =============="
result=$($rrdtool graph $htmldir/psmon_mem$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Avail Mem (kB)" \
--lower-limit -1 \
--watermark "`date`" \
$x_ts \
DEF:mem=psmon.rrd:mem:AVERAGE \
LINE$lw:mem#$blue:"Avail Mem (kB)" \
GPRINT:mem:LAST:Cur\\:%5.2lf \
GPRINT:mem:AVERAGE:Avg\\:%5.2lf \
GPRINT:mem:MAX:Max\\:%5.2lf \
GPRINT:mem:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Server CPU $str_ts =============="
result=$($rrdtool graph $htmldir/psmon_cpu$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "CPU (%)" \
--lower-limit 0 --upper-limit 100 --rigid \
--watermark "`date`" \
$x_ts \
DEF:cpu=psmon.rrd:cpu:AVERAGE \
LINE$lw:cpu#$blue:"CPU (%)" \
GPRINT:cpu:LAST:Cur\\:%5.2lf \
GPRINT:cpu:AVERAGE:Avg\\:%5.2lf \
GPRINT:cpu:MAX:Max\\:%5.2lf \
GPRINT:cpu:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Server Uptime $str_ts =============="
result=$($rrdtool graph $htmldir/uptime$ext_ts.png \
-w $w -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Uptime (Sec)" \
--lower-limit 0 \
--rigid \
--watermark "`date`" \
$x_ts \
DEF:uptime=uptime.rrd:uptime:AVERAGE \
LINE$lw:uptime#$blue:"uptime" \
GPRINT:uptime:LAST:Cur\\:%5.2lf \
GPRINT:uptime:AVERAGE:Avg\\:%5.2lf \
GPRINT:uptime:MAX:Max\\:%5.2lf \
GPRINT:uptime:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Weather temps $str_ts =============="
result=$($rrdtool graph $htmldir/weather_t$ext_ts.png \
-w 785 -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Temp (F)" \
--title "Outdoor Temps $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:temp_f=weather.rrd:temp_f:MAX \
DEF:dp_f=weather.rrd:dp_f:MAX \
LINE$lw:temp_f#$grn:"Temp (F)" \
GPRINT:temp_f:LAST:"     Cur\\:%5.2lf" \
GPRINT:temp_f:AVERAGE:Avg\\:%5.2lf \
GPRINT:temp_f:MAX:Max\\:%5.2lf \
GPRINT:temp_f:MIN:Min\\:%5.2lf\\t\\t\\n \
LINE$lw:dp_f#$blue:"Dew Point (F)" \
GPRINT:dp_f:LAST:"Cur\\:%5.2lf" \
GPRINT:dp_f:AVERAGE:Avg\\:%5.2lf \
GPRINT:dp_f:MAX:Max\\:%5.2lf \
GPRINT:dp_f:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Weather RH $str_ts =============="
result=$($rrdtool graph $htmldir/weather_rh$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "RH (%)" \
--title "Outdoor Relative Humidity $str_ts" \
--lower-limit -1 --upper-limit 100 --rigid \
--watermark "`date`" \
$x_ts \
DEF:rh=weather.rrd:rh:MAX \
LINE$lw:rh#$blue:"Relative Humidity (%%)" \
GPRINT:rh:LAST:"Cur\\:%5.2lf" \
GPRINT:rh:AVERAGE:Avg\\:%5.2lf \
GPRINT:rh:MAX:Max\\:%5.2lf \
GPRINT:rh:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Weather Visibility $str_ts =============="
result=$($rrdtool graph $htmldir/weather_vis$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Visibility (mi)" \
--title "Visibility $str_ts" \
--lower-limit -1 \
--watermark "`date`" \
$x_ts \
DEF:vis_mi=weather.rrd:vis_mi:MAX \
LINE$lw:vis_mi#$blue:"Visibility (mi)" \
GPRINT:vis_mi:LAST:"Cur\\:%5.2lf" \
GPRINT:vis_mi:AVERAGE:Avg\\:%5.2lf \
GPRINT:vis_mi:MAX:Max\\:%5.2lf \
GPRINT:vis_mi:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Weather Wind $str_ts =============="
result=$($rrdtool graph $htmldir/weather_wind$ext_ts.png \
-w 785 -h $short -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Wind (mph)" \
--title "Wind $str_ts" \
--lower-limit -1 \
--watermark "`date`" \
$x_ts \
DEF:wind_mph=weather.rrd:wind_mph:MAX \
LINE$lw:wind_mph#$blue:"Wind (mph)" \
GPRINT:wind_mph:LAST:"Cur\\:%5.2lf" \
GPRINT:wind_mph:AVERAGE:Avg\\:%5.2lf \
GPRINT:wind_mph:MAX:Max\\:%5.2lf \
GPRINT:wind_mph:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Pi Temperature $str_ts =============="
result=$($rrdtool graph $htmldir/pi_temp$ext_ts.png \
-w $w -h $h -a PNG \
-c BACK#$lgry -c CANVAS#$lgry \
--slope-mode \
--font DEFAULT:7: \
--vertical-label "Temperature (C)" \
--lower-limit 0 \
--rigid \
--title "Pi Processor Temperature (C) $str_ts" \
--watermark "`date`" \
$x_ts \
DEF:tavg=pi_temp.rrd:temperature:AVERAGE \
LINE$lw:tavg#$blue:"temp avg" \
GPRINT:tavg:LAST:Cur\\:%5.2lf \
GPRINT:tavg:AVERAGE:Avg\\:%5.2lf \
GPRINT:tavg:MAX:Max\\:%5.2lf \
GPRINT:tavg:MIN:Min\\:%5.2lf\\t\\t\\n )
echo_result "$result"

echo_debug "============== Log Events $str_ts =============="
htmldir=$htmldir $workdir/plot_logs.plt 

echo_debug "============== Check index.html $str_ts =============="
if [ ! -f "$htmldir/index.html" ]; then
    rpkhtml=$(realpath "$workdir/kymeta_rrd.html")
    echo "rpkhtml = $rpkhtml"
    ln -s  $rpkhtml "$htmldir/index.html"
fi

