#!/bin/bash
#
 
DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
workdir=${2:-'.'} 

if [ "$DEBUG" == "-h" -o "$DEBUG" == "h" -o "$workdir" == "-h" -o "$workdir" == "h" ]; then
    echo "Usage: pi_temp.sh [debug level] [workdir]"
    exit
fi

source "$workdir/rrd_vars.sh"
t=$(/opt/vc/bin/vcgencmd measure_temp|awk -F"[=']" '{print $2}')

if [ $DEBUG -ne 0 ]; then
    echo "t = $t"
    echo "cmd = $rrdtool update $workdir/pi_temp.rrd --template temperature N:$t"
fi

$rrdtool update $workdir/pi_temp.rrd --template temperature "N:$t"
 
