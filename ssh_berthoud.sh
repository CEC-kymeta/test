#!/bin/bash

# This script does the following:
# 1. setup a new interface, with a new MAC address, that can then use DHCP 
#    to get an IP Address.
# 2. Setup a route to the Trimble engineering network (192.168.128.x), using
#    this new interface
# 3. Setup an SSH tunnel to kypi (the Pi connected to the Kymeta antenna), 
#    through Geoffrey's Pi.

# Create the interface
#sudo ip link add dev macvlan0 link eth0:6 type macvlan
sudo ip link add dev macvlan0 link eth0 type macvlan
sudo ifconfig macvlan0 up

# Acquire an IP address
sudo dhclient macvlan0

# Set up a route to the engineering network
vlanip=$(ifconfig macvlan0 |grep "172.16."|cut -d : -f 2|cut -d " " -f 1)
sudo ip r add 192.168.128/24 via 172.16.0.1 src $vlanip
sudo ip r add 192.168.160/24 via 172.16.0.1 src $vlanip

# Collect some debug info
#nmcli device show macvlan0
#cat /var/lib/dhcp/dhclient.leases

# Setup  the SSH tunnel
KYPI=192.168.128.144
JCMPI=192.168.128.253
ssh -NL 22000:$KYPI:22 pi@$JCMPI &
ssh -X -p 22000 pi@localhost

# https://www.netfilter.org/documentation/HOWTO/NAT-HOWTO-6.html
# https://www.linuxquestions.org/questions/linux-security-4/iptables-dnat-snat-and-masquerading-264649/
#
# on Pi:
# sudo iptables -t nat -A OUTPUT -d 192.168.160.7 -j DNAT --to-destination 192.168.44.2
# sudo iptables -t nat -A PREROUTING -d 192.168.160.7 -j DNAT --to-destination 192.168.44.2
# sudo iptables -t nat -A POSTROUTING -d 192.168.160.7 -j SNAT --to-destination 192.168.44.2
# sudo iptables -t nat -A POSTROUTING -j MASQUERADE
# sudo iptables -t nat -A INPUT -d 192.168.160.7
# sudo iptables -t nat -L --line-numbers
# sudo iptables -t nat -D PREROUTING 1
