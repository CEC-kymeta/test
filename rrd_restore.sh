#!/bin/bash
#
## change directory to the rrdtool script dir
#DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
#workdir=${2:-'.'} 
#set -x
shopt -s extglob
#shopt

DEBUG=0
workdir='.'
timescale="minute"
force=""

while getopts "hVt:w:f" arg; do
  case $arg in
    h)
      echo "Usage: graph_rrd.sh [-V] [-w workdir] [-t minute|hour|month]"
      exit
      ;;
    w)
      workdir=$OPTARG
      ;;
    t)
      timescale=$OPTARG
      ;;
    f)
      force="-f"
      ;;
    V)
      DEBUG=1
      ;;
  esac
done


workdir=$(readlink -m "$PWD/$workdir")
source "rrd_vars.sh"


if [ $DEBUG -ne 0 ]; then
   echo "workdir = $workdir"
   echo "timescale = $timescale"
fi

archive_name=$(find "$workdir" -name "rrd_dump*tgz" | tail -n 1)
if [ $DEBUG -ne 0 ]; then
   echo "archive_name = $archive_name"
fi
tar xvfz "$archive_name" -C "$workdir"

cd "$workdir"

for f in ${rrd_list[@]}
do
   if [ -f "$f" ]; then
      echo "restoring ${f}.xml to $f"
      src="$workdir/${f}.xml"
      dst="$workdir/$f"
      result=$($rrdtool restore "$force" "$src" "$dst")
   fi
done
