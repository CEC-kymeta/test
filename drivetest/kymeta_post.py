#!/usr/bin/python

# This is a script to convert site survey (drive test) data (.csv format, 
# collected by kymeta_drive.py) into a KML file,

import csv
import argparse
import os
import re
import shutil


# TODO: color code based on SNR. From an email from Nicklas at Kymeta:
# "In the ASM GUI there is a field called PL Es/No - this is a good indication of the SNR.
# Everything over 1 is a good value. Below -3 is bad. Between -2 and 1 is marginal."

listfile = 'kymeta_post_list.txt'

parser = argparse.ArgumentParser(description="Post process results from SStool.sh")
parser.add_argument('-V', '--verbose', dest='verbose', action='store_true', 
        help='Print more messages along the way')
parser.add_argument('-t', '--add_timestamp', dest='add_timestamp', action='store_true', 
        help='Add a timestamp to the output filenames')
parser.add_argument('-f', '--filename', dest='filename', action='store', default=None,
        help='process FILE instead of pulling a list from ' +listfile)
parser.add_argument('-l', '--label', dest='label', action='store', default=None,
        help='Label to append to data file names')
parser.add_argument('-o', '--outdir', dest='outdir', action='store', default="./results",
        help='Output data directory [%(default)s]')
options = parser.parse_args()

#----------------------------------------------------------------
def read_survey_data(filename, verbose=True):
    """
    Read filename for site survey data, and return a data structure
    """
#----------------------------------------------------------------
    records = []
    gps_re = re.compile("(\d+)(\d{2}\.\d+)")

    print ("Reading file " +filename)
    f = open(filename,'rb')
    rows = csv.reader(f, delimiter=',')
    line_num = 1
    for row in rows:
        if options.verbose:
            print("row = " +str(row))

        if row[0] == 'Hour': 
            line_num = line_num +1 
            continue

        empty_field = False
        for field in row:
            if len(field) == 0:
                print("Found an EMPTY field aborting this row")
                empty_field = True
                break
        if empty_field:
            line_num = line_num +1 
            continue

        try:
            # records contains a list of [Hour, Minute, Second,
            # latitude, longitude, altitude, gps_fix, gps_fix_quality, status,
            # RX_SNR, RX_status, TX_status, network']
            hour = row[0]
            minute = row[1]
            second = row[2]
            lat = row[3]
            lon = row[4]
            altitude = row[5]
            gps_fix = row[6]
            gps_fix_quality = row[7]
            status = row[8]
            #RX_SNR = float(row[9])
            RX_SNR = row[9]
            RX_status = row[10]
            TX_status = row[11]
            network = row[12]
            print ("len(row) = " +str(len(row)) )
            if len(row) >= 14:
                sat_lon = row[13]
            else:
                sat_lon = 0

            time = hour+":"+minute+":"+second
            if verbose:
                print ("  Time: ",time)
                print ("  RX SNR: ", RX_SNR)
                print ("  RX Status: ", RX_status)
                print ("  TX Status: ", TX_status)
                print ("  Network: ", network)
            records.append([lat, lon, RX_SNR, time, gps_fix, gps_fix_quality, status, RX_status, TX_status, network, sat_lon])
        except ValueError:
            if verbose:
                print ("problems (ValueError) with line:", line_num)
            line_num = line_num + 1
        except AttributeError:
            if verbose:
                print ("AttributeError with line:", line_num)
            line_num = line_num + 1
        except IndexError:
            print ("Unable to parse CSV line", row)
        line_num = line_num + 1
    f.close()

    print ("Total measurement points: ", len(records))
    return (records)

#----------------------------------------------------------------
def write_kml_val(fp, field, val):
#----------------------------------------------------------------
    fp.write('               <Data name = "' +field +'">\n')
    fp.write('                  <value>%s</value>\n' % val )
    fp.write('               </Data>\n')

#----------------------------------------------------------------
def write_kml(records, output_file, survey_info):
#----------------------------------------------------------------
    """
    records is a dictionary of lists, each list is a list-of-lists.
    each first-level list represents a data sample that was scraped
    from the kymeta device Web GUI.

    Each record contains:
    lat, lon, RX_SNR, time, gps_fix, gps_fix_quality,
    status, RX_status, TX_status, network

    output_file is the filename to which output will be directed

    survey_info is a list with the latitude/longitude of the
    file, as well as any comments that we want to appear in the top level
    KML folder.
    """
    start_lat = survey_info[0]
    start_lon = survey_info[1]
    file_descriptor = survey_info[2]
    style_name = {}
    print ("Beginning to write", output_file)
    print ("Latitude", start_lat)
    print ("Longitude", start_lon)
    print ("File descriptor: ", file_descriptor)
    #	nicknames = device_map[0]
    #	print ("Nicknames: ", nicknames)
    #	icons = device_map[1]
    style_name = {}
    outfile = open (output_file,'w')
    outfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    outfile.write('<kml xmlns="http://www.opengis.net/kml/2.2">\n')
    outfile.write('<Document>\n')
    
    # At this point, we are defining the styles in the KML file.
    outfile.write('<Style id="default"> \n')
    outfile.write('   <IconStyle> \n')
    outfile.write('       <Icon> \n')
    outfile.write('          <href>images/%s</href> \n' % 'circle.png')
    outfile.write('       </Icon> \n')
    outfile.write('       <scale>0.5</scale> \n')
    outfile.write('   </IconStyle> \n')
    outfile.write('   <BalloonStyle> \n')
    outfile.write('      <text>Time:$[timestamp]\n'
                   +'RX_SNR= $[RX_SNR] dB\n'
                   +'RX_Status= $[RX_Status]\n'
                   +'network= $[network]\n'
                   +'Sat Longitude= $[sat_lon]\n')

    outfile.write('      </text> \n')
    outfile.write('   </BalloonStyle> \n')
    outfile.write('   <LabelStyle> \n')
    outfile.write('      <color>3fffffff</color> \n')
    outfile.write('      <colormode>3fffffff</colormode> \n')
    outfile.write('      <scale>0.5</scale> \n')
    outfile.write('   </LabelStyle> \n')
    outfile.write('</Style> \n')

    # Now we create a folder for the data
    outfile.write('   <Folder>\n')
    outfile.write('      <name>%s</name>\n' % file_descriptor)
    outfile.write('            <LookAt>\n')
    outfile.write('               <longitude>%f</longitude>\n' % start_lon)
    outfile.write('               <latitude>%f</latitude>\n' % start_lat)
    outfile.write('               <altitude>0</altitude>\n')
    outfile.write('               <heading>0</heading>\n')
    outfile.write('               <tilt>0</tilt>\n')
    outfile.write('               <range>1500</range>\n')
    outfile.write('               <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>\n')
    outfile.write('            </LookAt>\n')

    # The first thing in the folder is a placemark for the AP
    # Location
    outfile.write('      <Placemark>\n')
    outfile.write('      <name>%s</name>\n' % file_descriptor)
    outfile.write('            <Point>\n')
    outfile.write('               <coordinates> %f, %f, 0 </coordinates>\n' % (start_lon, start_lat))
    outfile.write('            </Point>\n')
    outfile.write('            <Style>\n')
    outfile.write('                <IconStyle>\n')
    outfile.write('                   <Icon>\n')

    outfile.write('                      <href>http://maps.google.com/mapfiles/kml/paddle/ltblu-blank.png</href>\n')
    outfile.write('                   </Icon>\n')
    outfile.write('                </IconStyle>\n')
    outfile.write('            </Style>\n')
    outfile.write('      </Placemark>\n')

    # Now we begin to write the actual data
    outfile.write('      <Folder>\n')
    outfile.write('         <name>Sample Data</name>\n')
    count = 0
    for r in records:
        if options.verbose:
            print("r = " +str(r))
        latitude = float(r[0])
        longitude = float(r[1])
        RX_SNR = r[2]
        r_time = r[3]
        gps_fix = r[4]
        gps_fix_quality = r[5]
        status = r[6]
        RX_status = r[7]
        TX_status = r[8]
        network = r[9]
        sat_lon = r[10]
        if options.verbose:
            print("RX_SNR = " +RX_SNR)
            print("time = " +r_time)
            print("RX_status = " +RX_status)
            print("TX_status = " +TX_status)
            print("network = " +network)

        if network == 'WAITING_FOR_RX_LOCK' or RX_status == 'Waiting for NCR Lock':
            icon_color = '3f0000ff' #red color code
        elif network == 'IN_ACQUISITION' or network == 'DETECTED':
            icon_color = '5f00ffff' #yellow color code
        elif network == 'IN_NETWORK':
            icon_color = '7f00ff00' #green color code
        else:  # Unknown state
            icon_color = '7f7f7f00' #grey color code

        outfile.write('         <Placemark>\n')
        name_string = "            <name> Record %d </name>\n" % count
        outfile.write(name_string)
        timestamp_string = "            <timestamp> %s </timestamp>\n" % r_time
        outfile.write(timestamp_string)
        #style_string = "            <styleUrl> #%s </styleUrl>\n" % style_name[MAC_addr]
        style_string = "            <styleUrl> #%s </styleUrl>\n" % 'default'
        outfile.write(style_string)
        outfile.write('            <ExtendedData>\n')
        write_kml_val(outfile, 'RX_SNR', RX_SNR)
        write_kml_val(outfile, 'description', file_descriptor)
        write_kml_val(outfile, 'timestamp', r_time)
        write_kml_val(outfile, 'network', network)
        write_kml_val(outfile, 'RX_Status', RX_status)
        write_kml_val(outfile, 'TX_Status', TX_status)
        write_kml_val(outfile, 'GPS Fix', gps_fix)
        write_kml_val(outfile, 'GPS Quality', gps_fix_quality)
        write_kml_val(outfile, 'sat_lon', sat_lon)
        write_kml_val(outfile, 'devicename', 'Default Device ')
        outfile.write('            </ExtendedData>\n')

        outfile.write('            <Style>\n')
        outfile.write('               <IconStyle>\n')
        outfile.write('                 <color>%s</color>\n' % icon_color)
        outfile.write('               </IconStyle>\n')
        outfile.write('            </Style>\n')
        outfile.write('            <Point>\n')
        fmt_string = "               <coordinates> %f, %f, 0 </coordinates>\n"
        vals = (longitude, latitude)
        outstring = fmt_string % vals
        outfile.write(outstring);
        outfile.write('            </Point>\n')
        outfile.write('         </Placemark>\n')
        count = count + 1

    outfile.write('      </Folder>\n')
    outfile.write('   </Folder>\n')
    outfile.write('</Document>\n')
    outfile.write('</kml>\n')
    outfile.close()


#----------------------------------------------------------------
def proc_data(survey_file, label):
#----------------------------------------------------------------
    if not label:
        label = ""
    survey_data = read_survey_data(survey_file, options.verbose)
    if len(survey_data) > 0:
        if options.add_timestamp:
            ofile_basename = label +'_' +survey_data[0][3].replace(':','-')
        else:
            ofile_basename = label 

        print ("-----------------------------------------------------------")
        print survey_file +': ' +str(survey_data[0])
        survey_info = [ float(survey_data[0][0]),                        # Latitude of first record
                    float(survey_data[0][1]),                        # Longitude of first record
                    label +'_' +survey_data[0][3].replace(':','-') ] # Text label based on input filename and first time stamp
        print ("survey_info: " +str(survey_info))
        print ("-----------------------------------------------------------")

        kml_outfile = options.outdir +"/%s.kml" % ofile_basename
        write_kml(survey_data, kml_outfile, survey_info)

#------------------------------------------------
# main()
#------------------------------------------------
if not os.path.exists(options.outdir):
    os.makedirs(options.outdir)

# Note: the images/circle.png is required for the KML output
if not os.path.exists(options.outdir+"/images"):
    if os.path.exists("./images"):
        print ("Copying images/ to " +options.outdir)
        shutil.copytree("./images", options.outdir+"/images") 
    else:
        print ("Warning: could not fine images/ skipping copy to " +options.outdir)

if options.filename:
    proc_data(options.filename, options.label)
else:
    f = open(listfile, 'rb')
    try:
        survey_list = csv.reader(f, delimiter=',')
        for line in survey_list:
            if len(line) == 0:
                continue
            elif line[0][0] == '#':
                continue
            else:
                if options.verbose:
                    print(line)
                print("\n")

            survey_file = line[0].strip()
            label = line[1].strip()
            proc_data(survey_file, label)
    finally:
        f.close()

