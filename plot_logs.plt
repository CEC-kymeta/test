#!/usr/bin/gnuplot
#
# Note: this script REQUIRES the environment variable htmldir
#       for example: `htmldir=html ./plot_logs.plt`
#
f=",8"
set datafile separator ","
#set terminal png xe8e8e8 x222222 size 900,300
set terminal png size 900,220
set title "Events" font f
#set ylabel "Requests per second"
#set xlabel "Date" font f
set xdata time
set timefmt "%Y/%m/%d:%H:%M:%S"
set format x "%a"
set yrange [ 0 : 10]
#set key left top
unset key
set grid
set xtics font f
#set ytics font f
unset ytics

#print TODAY
#len=`cat log.csv|wc|awk -e '{print $1}'`
#TODAY="`date +%Y/%m/%d`"
#TODAY="`date +%Y/%m/%d --date="+1 day"`"
TODAY="`date +%Y/%m/%d:%H:%M:%S`"
#print TODAY
#MINUS8="`date +%Y/%m/%d --date="-8 day"`"
MINUS30D="`date +%Y/%m/%d:%H:%M:%S --date="-30 day"`"
MINUS8D="`date +%Y/%m/%d:%H:%M:%S --date="-8 day"`"
MINUS25H="`date +%Y/%m/%d:%H:%M:%S --date="-25 hour"`"
#print MINUS8
#print MINUS25H
set xrange [MINUS8D:TODAY]

#pick the red color hex string out
red(colorstring)= colorstring[2:3]
#pick the green color hex string out
green(colorstring)=colorstring[4:5]
#pick the blue color hex string out
blue(colorstring)=colorstring[6:7]
#convert a hex string to its dec format
hex2dec(hex)=gprintf("%0.f",int('0X'.hex))
#calculate the rgb value from the r,g,b weight
rgb(r,g,b) = 65536*int(r)+256*int(g)+int(b)
#convert the hex color string to its rgb value.
hex2rgbvalue(color)=rgb(\
                        hex2dec(red(color)),\
                        hex2dec(green(color)),\
                        hex2dec(blue(color))\
                        ) 
set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb hex2rgbvalue("#e8e8e8") behind

#plot '-' using 0:1:(fulltime(1)):xticlabels(2):yticlabels(parttime(1)) \
#         with labels point pt 7 left offset 1,1 font ",7"

htmldir=system("echo $htmldir")
set output htmldir."/log_hr.png"

plot "log.csv" using 1:2 with impulses lw 2, \
     "log.csv" using 1:2:3 with labels rotate left offset 0,0.75 font ",9"


set output htmldir."/log_mo.png"
set xrange [MINUS30D:TODAY]
#set xtics MINUS25H, 2*3600, TODAY
set xtics 24*3600
set format x "%H:%M"
replot

set output htmldir."/log.png"
set xrange [MINUS25H:TODAY]
#set xtics MINUS25H, 2*3600, TODAY
set xtics 2*3600
set format x "%H:%M"
replot
