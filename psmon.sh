#!/bin/bash
#
 
DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
workdir=${2:-'.'} 

if [ "$DEBUG" == "-h" -o "$DEBUG" == "h" -o "$workdir" == "-h" -o "$workdir" == "h" ]; then
    echo "Usage: psmon.sh [debug level] [workdir]"
    exit
fi

source "$workdir/rrd_vars.sh"
 
driver_n=$(pgrep -c chromedriver)
chrome_n=$(pgrep -c chromium-browse)
firefox_n=$(pgrep -c firefox)
browser_n=$(($chrome_n + $firefox_n))
xvfb_n=$(pgrep -c Xvfb)
latency_n=$(pgrep -cf update_latency_files.py)
ant_n=$(pgrep -cf ant_requests.py)
modem_sel_n=$(pgrep -cf modem_temp_selenium.py)
modem_ger_n=$(pgrep -cf modem_temp_germanium.py)
modem_n=$(($modem_sel_n + $modem_ger_n))

# Sometimes the 'Logger' portions of failed commands in crontab will pile up.
# Here is a preliminiary version of what monitoring it might look like. This 
# probably needs to be made more restrictive before implementing a push to the
# database, or adding to psmon_kill.sh
logger_n=$(pgrep -c Logger)

mem=$(free | head -n 2|tail -n 1|awk -e {'print $7'})
cpu=$(uptime|awk -e {'print $10'}|cut -d ',' -f 1)
idle=$(top -b | head -n 3 | tail -n 1|awk -e {'print $8'})
cpu=$(echo "100.0 - $idle"|bc)

#vals="$driver_n:$chrome_n:$xvfb_n:$mem"
vals="$driver_n:$browser_n:$xvfb_n:$mem:$cpu:$latency_n:$ant_n:$modem_n"
Nvals="N:$vals"
if [ $DEBUG -ne 0 ]; then
    echo "driver_n = $driver_n"
    echo "chrome_n = $chrome_n"
    echo "firefox_n = $firefox_n"
    echo "browser_n = $browser_n"
    echo "xvfb_n = $xvfb_n"
    echo "latency_n = $latency_n"
    echo "ant_n = $ant_n"
    echo "modem_sel_n = $modem_sel_n"
    echo "modem_ger_n = $modem_ger_n"
    echo "modem_n = $modem_n"
    echo "vals = $vals"
    echo "mem = '$mem'"
    echo "cpu = $cpu"
    echo "cmd = $rrdtool update $workdir/psmon.rrd --template driver:chrome:xvfb:mem:cpu N:$vals"
    echo "Logger count = $logger_n"
fi


$rrdtool update $workdir/psmon.rrd --template driver:chrome:xvfb:mem:cpu:latency:ant:modem "N:$vals"
 
