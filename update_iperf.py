#!/usr/bin/python
import argparse
import sys, os
import rrdtool
import subprocess as sp
import syslog
import psutil

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--ip_addr', dest='ip', action='store',
        help='IP address of Kymeta antenna default: %(default)s',
        default="54.186.205.190")
parser.add_argument('-f', '--filename', dest='filename', action='store', default="iperf.rrd",
        help='Filename for status data: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-u', '--units', dest='units', action='store', default="kbs",
        help='The units used to store the rate in the database. Valide args are mbs, kbs. default: %(default)s')
parser.add_argument('-s', '--skip_syslog', dest='syslog', action='store_false', default=True,
        help='Disable logging to syslog')
parser.add_argument('-z', '--force_zero', dest='force_zero', action='store_true', default=False,
        help='update the RRD database with some all zero data')
parser.add_argument('-t', '--time', dest='time', action='store', type=int,
        help='Amount of time to run iperf default: %(default)s',
        default=1)
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0, 
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level = 1.')
args = parser.parse_args()

if args.verbose_on:
    args.verbose = max(1, args.verbose)

out_zero  = "------------------------------------------------------------\n"
out_zero += "Client connecting to lucifer, TCP port 5001\n"
out_zero += "TCP window size: 85.0 KByte (default)\n"
out_zero += "------------------------------------------------------------\n"
out_zero += "[  3] local 10.7.56.252 port 40688 connected with 10.8.246.142 port 5001\n"
out_zero += "[ ID] Interval       Transfer     Bandwidth\n"
out_zero += "[  3]  0.0-10.0 sec  0 GBytes   0 Mbits/sec\n"

tmpl = 'rate' 


if args.syslog:
    syslog.openlog('update_iperf')
############### DEBUG ################
#args.verbose = 1

#print(psutil.pids())
script_name = sys.argv[0].split('/')[-1]
if args.verbose:
    print "script_name = " +script_name 
for pid in psutil.pids():
    try:
        p = psutil.Process(pid)
    except psutil.NoSuchProcess:
        if args.verbose:
            print("Note: caught NoSuchProcess exception for pid " +str(pid) )
        continue

    #print p.name()
    #if p.name() == "python.exe":
    #    print("Called By Python:"+ str(p.cmdline()) )
    if p.name() == script_name:
        if os.getpid() == p.pid:
            if args.verbose:
                print("Found myself: " +str(p.cmdline()) )
        else:
            msg = "Found existing update_iperf (pid = " +str(p.pid) +"), skipping this one (" +str(os.getpid()) +")"
            if args.verbose:
                print(msg)
            if args.syslog:
                syslog.syslog(msg)
                syslog.closelog()
            sys.exit()
        
# A little bit of hosts lookup
if args.ip == "lucifer":
    args.ip = "10.8.246.142"
elif args.ip == "kant":
    args.ip = "192.168.44.2"
elif args.ip == "kmodem":
    args.ip = "192.168.44.1"
elif args.ip == "aws":
    args.ip = "54.186.205.190"

filename = os.path.join(args.work_dir, args.filename)
if args.verbose:
    print( "filename = " +filename)
    print( "ip = " +args.ip)

if args.force_zero:
    if args.syslog:
        syslog.syslog( "iperf forcing zero value update" )
    if args.verbose:
        print ( "iperf forcing zero value update" )
    rate = '0'

else:
    
    out = ""
    cmd = ["iperf", "-c", args.ip, "-t", str(args.time)]
    if args.verbose:
        print( "cmd = " +str(cmd) )
    try:
        out = sp.check_output(["iperf", "-c", args.ip, "-t", str(args.time)])
    except sp.CalledProcessError, e:
        if args.syslog:
            syslog.syslog( "iperf return code: " +str(e.output) )
            syslog.syslog( "iperf output:\n", +e.output )
        if args.verbose:
            print( "iperf (error) output:\n", e.output )
        out = out_zero
    except Exception as ex:
        print("Exception has been thrown. " + str(ex))

    # Note: out = ... [  3]  0.0-10.2 sec  8.12 MBytes  6.71 Mbits/sec \n
    if args.verbose:
        print("out: ")
        print(out)
        print("")

    if len(out.split('\n')) < 2:
        msg = "Error processing iperf return of \"" +out +'"'
        print(msg)
        if args.syslog:
            syslog.syslog(msg)
        out = out_zero

    last = out.split('\n')[-2]
    if args.verbose:
        print("last = " +last)
    rate = last.split()[-2]
    units = last.split()[-1]
    if args.verbose:
        print("rate = " +rate)
        print("units = " +units)

    # Always report the rate in Mb/s
    if str.lower(args.units) == "mbs":
        if 'gbits' in units.lower():
            mult = 1e3
        elif 'mbits' in units.lower():
            mult = 1.0
        elif 'kbits' in units.lower():
            mult = 1e-3
        elif 'bits' == units.lower():
            mult = 1e-6
        rate = str(float(rate) * mult)
    else:
        if 'gbits' in units.lower():
            mult = 1e6
        elif 'mbits' in units.lower():
            mult = 1e3
        elif 'kbits' in units.lower():
            mult = 1
        elif 'bits' == units.lower():
            mult = 1e-3
        rate = str(float(rate) * mult)

vals = 'N:' +str(rate)
if args.verbose:
    if str.lower(args.units) == "mbs":
        print("rate = " +rate +" Mb/s")
    else:
        print("rate = " +rate +" Kb/s")
    print("vals = " +vals)


rrdtool.update(filename, '--template', tmpl, vals)

if args.syslog:
    syslog.closelog()
