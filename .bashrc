# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# This was an attempt to get vim colors looking better in xfce (new Xubuntu install on Felix)
if [ -e /lib/terminfo/x/xterm-256color ] && [ "$COLORTERM" == "xfce4-terminal" ]; then
    export TERM=xterm-256color
fi

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	   color_prompt=no
    fi
fi
#echo "color_prompt=$color_prompt"

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]: \[\033[01;34m\]\w\[\033[00m\]\n\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h: \w\n\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    egbin=`command -v egrep` &>/dev/null
    if command -v egrep &>/dev/null; then 
       #alias egrep='egrep --color=auto'
       #echo "found $egbin"
       alias egrep="$egbin --color=auto"
    fi
fi

#-----------------------------------------------------
function path()
#-----------------------------------------------------
{
	(IFS=":"; for p in $PATH; do echo $p; done)
}

#-----------------------------------------------------
addpath ()
# Function to add a dir to the end of PATH unless arg2 is "front"
{
	if [ -n $1 ]
	then
      egbin=`which egrep`
		if ! echo $PATH | "$egbin" -q "(^|:)$1($|:)"
		then
			if [ "$2" = "front" ]; then
				PATH=$1:${PATH}
			else
				PATH=${PATH}:$1
			fi
			export PATH
			#echo "PATH is now:"
			#echo $PATH
		fi
	fi
}

#add_ldpath ()
#{
#	if [ -n $1 ]
#	then
#		if ! echo $LD_LIBRARY_PATH | /bin/egrep -q "(^|:)$1($|:)"
#		then
#			if [ "$2" = "front" ]; then
#				LD_LIBRARY_PATH=$1:${LD_LIBRARY_PATH}
#			else
#				LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$1
#			fi
#			export LD_LIBRARY_PATH
#		fi
#	fi
#}


#rmpath ()
#{
#	if [ -x $HOME/bin/rmpath.py ]
#	then
#		# If no args given , then rmpath.py will just remove duplicity
#		# from PATH and will always remove '.'.
#		PATH=$($HOME/bin/rmpath.py $@)
#		export PATH
#		echo "PATH is now:"
#		echo $PATH
#	fi
#}


# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
#alias l='ls -CF'
alias lt='ls -lrth'
alias ltd='ls -lrthd'
# List by groups: 
#  - group directories first 
#  - LOC_COLLATE=C forces case to be important, which groups .xxxx and _xxxx 
#    files. setting the environment variable before issuing the command makes 
#    that variable change only for the command being issued.
#  - force hidden files, except .. and . to be displayed with -A
alias lg='LC_COLLATE=C ls -A --group-directories-first'     
alias hgrep="history|grep"
alias agrep="alias|grep"
alias ha="history -a"
alias hax="history -a; exit"
alias resh='source ~/.bashrc'
alias epnl='echo $PATH|tr ":" "\n"' # epnl = Echo Path New Line


if command -v dmesg &>/dev/null; then 
   alias dmesgt='dmesg -e --color=always| tail'
fi

if command -v systemctl &>/dev/null; then 
   alias sc='systemctl'
   alias ssc='sudo systemctl'
fi

# For crontab rsync
# See: https://serverfault.com/questions/92683/execute-rsync-command-over-ssh-with-an-ssh-agent-via-crontab
#if command -v keychain &>/dev/null; then 
#   keychain --quiet --clear $HOME/.ssh/id_rsa
#fi


if command -v google-drive-ocamlfuse &>/dev/null; then 
   alias gdo='google-drive-ocamlfuse'

   function gdom()
   # Example useage: gdom CEC_Radios
   # This would mount label CEC_Radios to $HOME/CEC_Radios
   {
      google-drive-ocamlfuse -label $1 $HOME/$1
   }

   function gdoum()
   # Example useage: gdoum ~/CEC_Radios
   # This would UNmount $HOME/CEC_Radios
   {
      fusermount -u $HOME/$1
   }

   alias gdols='mount | grep google-drive-ocamlfuse'
fi

if command -v ssh &>/dev/null; then 
   # turn on, check or turn off ssh proxies in ~/.ssh/config
   # based on https://starkandwayne.com/blog/setting-up-an-ssh-tunnel-with-ssh-config/
   alias proxy-on='ssh -fN'
   alias proxy-check='ssh -O check'
   alias proxy-off='ssh -O cancel'     # cancel seems to be the preferred command for forwarding, rather than exit
   # Example usage: 
   #   proxy-on hostname
   #   proxy-check hostname
   #   proxy-off hostname
   #
   # Example .ssh/config entry:
   #   Host kypi
   #   User pi
   #   ForwardX11  yes
   #   IdentityFile ~/.ssh/id_rsa
   #   LocalForward 9001 192.168.44.1:80
   #   LocalForward 9002 192.168.44.2:80
   #   ControlMaster auto
   #   ControlPath /tmp/ssh-%r@%h:%p
fi

if command -v git &>/dev/null; then 
   #alias gitlog='git log --reverse --pretty'
   alias gitlog='git log --pretty --decorate'
   alias gitl='git log --oneline --decorate -n 30'
   alias gitci='git commit -a'
   alias gitpu='git push origin master'
fi
if command -v gitk &>/dev/null; then 
   alias gitka='gitk --all'
fi

if command -v ansible-playbook &>/dev/null; then 
   alias pb='ansible-playbook'
fi

if command -v docker &>/dev/null; then
   alias dkps='docker ps'
   alias dkls='docker container ls'
   alias dkc='docker container'
   alias dk='docker'
   #-----------------------------------------------------
   function dksh()
   # search the list of docker containers for a string in $1,
   # and then start a bash shell in the first container that 
   # matches
   #-----------------------------------------------------
   {
      #docker exec -it $(sudo docker ps|grep $1|awk '{print $NF}') /bin/bash
      docker exec -it -u ${USER} $(sudo docker ps|grep $1|head -n 1|awk '{print $NF}') /bin/bash -l
   }
fi

# List all functions, sorting function names that begin with '_' first.
alias lsfunc='declare -F|sort -t_ -k1,1'

if command -v svn &>/dev/null; then 
   alias svnlog='svn log -r 1:HEAD -v'
   alias svnignore='svn propedit svn:ignore'
   export SVN_EDITOR="vim"

   #-----------------------------------------------------
   function svnlogmsg()
   # This function edits the log message for a revision
   # Note that the repo/hooks/pre-revprop-change must exist and be executable
   #      (there should be a pre-revprop-change.templ that can be used)
   #      For details: http://stackoverflow.com/questions/304383/how-do-i-edit-a-log-message-that-i-already-committed-in-subversion
   #-----------------------------------------------------
   {
      svn propedit svn:log --revprop -r $1 --editor-cmd vim
   }
fi


addpath $HOME/projects/git-plus

if command -v openssl &>/dev/null; then 
   alias md5='openssl dgst'
fi

if command -v mtr &>/dev/null; then 
   alias mtr='mtr -o LSD'
fi
if command -v autojump &>/dev/null; then 
   alias aj='. /usr/share/autojump/autojump.sh'
fi

if command -v grive &>/dev/null; then 
   if [ -d "$HOME/gdrive" ]; then
      alias gds='cd "$HOME/gdrive" && grive; cd -'
   fi
fi
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
# This will cause a little popup notification
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

gt_cmd=`command -v gnome-terminal`
if [ -e "$gt_cmd" ]; then
   alias gt="$gt_cmd"
fi
alias resist="wine ~/bin/RESIST.EXE &> /dev/null"

ink_cmd=`command -v inkscape`
if [ -e "$ink_cmd" ]; then
   alias ink="$ink_cmd"
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
# Allow tab completion to ignore case
bind "set completion-ignore-case on"


addpath "/opt/arm/gcc-arm-none-eabi-4_9-2015q3/bin"
addpath "/opt/openocd/0.9.0-201505190955/bin/" "front"


#alias smath="/opt/SMathStudioDesktop.0_89_8.Mono/SMathStudio_Desktop.exe &"
#alias smath="/opt/SMathStudioDesktop.0_92.Mono/SMathStudio_Desktop.exe &"
#smath_bin="/opt/SMathStudioDesktop.0_89_8.Mono/SMathStudio_Desktop.exe"
smath_bin="/opt/SMathStudioDesktop.0_92.Mono/SMathStudio_Desktop.exe"
[[ -s $smath_bin ]] && alias smath="$smath_bin &"
draftsight_bin="/opt/dassault-systemes/draftsight/bin/DraftSight"
[[ -s $draftsight_bin ]] && alias draftsight="$draftsight_bin &"

if [ -d "/opt/modeltech" ] ; then
   export set MODEL_PATH="/opt/modeltech"
   export set LM_LICENSE_FILE="$MODEL_PATH/license.dat"
   alias vsim="$MODEL_PATH/win32/vsim.exe"
   #alias vlog="$MODEL_PATH/win32/vlog.exe"
   #alias vcom="$MODEL_PATH/win32/vcom.exe"
   #alias vlib="$MODEL_PATH/win32/vlib.exe"
   #alias vmake="$MODEL_PATH/win32/vmake.exe"
   #alias vmap="$MODEL_PATH/win32/vmap.exe"
   alias vlog="$MODEL_PATH/linux/vlog"
   alias vcom="$MODEL_PATH/linux/vcom"
   alias vlib="$MODEL_PATH/linux/vlib"
   alias vmake="$MODEL_PATH/linux/vmake"
   alias vmap="$MODEL_PATH/linux/vmap"
   alias modelsim="$MODEL_PATH/win32/modelsim.exe &> /dev/null"

   # set PATH so it includes Modeltech's Modelsim
   addpath "$MODEL_PATH/win32"
   addpath "$MODEL_PATH/linux"
fi

addpath "$HOME/bin"
addpath "$HOME/scripts"       # These are scripts from github

if command -v gcalctool &>/dev/null; then
   #echo "found gcalctool"
   alias calc="gcalctool"
elif command -v gnome-calculator &>/dev/null; then 
   #echo "found gnome-calculator"
   alias calc="gnome-calculator"
fi

if command -v ssh &>/dev/null; then
   alias sshnc='ssh -o StrictHostKeyChecking=no -o CheckHostIP=no'
   alias sshrm='ssh-keygen -f ~/.ssh/known_hosts -R'
   alias scpnc='scp -o StrictHostKeyChecking=no -o CheckHostIP=no'
fi
   
#alias groove='java -jar /opt/GrooveDown/GrooveDown.jar &'

if command -v thunderbird  &>/dev/null; then
   # Start thunderbird with logging turned on. Instead of IMAP; SMTP, ALL or possibly other options exist
   alias tbird_log='NSPR_LOG_MODULES=IMAP:5 NSPR_LOG_FILE=/tmp/thuderbird_`date "+%Y%m%d_%H%M%S"`.log thunderbird &'
fi

if command -v octave-cli &>/dev/null; then
   alias oct=octave-cli
fi

mvim_bin="/usr/local/bin/mvim"
[[ -s $mvim_bin ]] && alias gvim="$mvim_bin"
#[[ -s $mvim_bin ]] && export SVN_EDITOR="$mvim_bin"

export SVN_EDITOR="vim"

#---------------------------------------------------------------------
#        Xilinx Stuff
#---------------------------------------------------------------------
function setise () {
   Xrev=13.4      # 14.7      # Set to either 13.1 or 13.4 (or other if another version installed at a later time
   export XILINX=/opt/Xilinx/$Xrev/ISE_DS/
   #XPATH=/opt/Xilinx/$Xrev/ISE_DS/ISE/bin/lin      # For 32bit Linux Distros
   XPATH=$XILINX/ISE/bin/lin64      # For 64bit Linux Distros
   EDKPATH=$XILINX/EDK/bin/lin64    # For 64bit Linux Distros
   addpath $XPATH                   # set PATH so it includes Xilinx
   export XILINXD_LICENSE_FILE=1234@lucifer;/home/russell/.Xilinx/Xilinx.lic     # Setup Licensing
   alias ise_b="$XPATH/ise -intstyle silent &> /dev/null &" # send stderr and stdout to /dev/null, and put ise in the background
   alias impact_b="$XPATH/impact &> /dev/null &" # send stderr and stdout to /dev/null, and put impact in the background

   function impact_f() {
      # send stderr and stdout to /dev/null, and put impact in the background, 
      # and pass it a file name as a command line argument
      $XPATH/impact -ipf $1 &> /dev/null & 
   }
   export -f impact_f
   #
   export XIL_CS_JRE=/usr
   #alias Xsettings="source $XILINX/settings32.sh"
   alias Xsettings="source $XILINX/settings64.sh"
   alias chipscope="$XPATH/unwrapped/analyzer"
   alias chipscope_b="$XPATH/unwrapped/analyzer &> /dev/null &"
}

#Vrev=2015.4
Vrev=2015.2
#Vrev=2014.4
#export XILINX_ENV_SCRIPT=/opt/Xilinx/Vivado/$Vrev/settings64.sh
#alias vivado_b="vivado &> /dev/null &"
#XPATH=$XILINX

#TODO: add /opt/Xilinx/Vivado/2015.4/ids_lite/ISE/bin/lin64/ to path for things like:
# xtclsh, xst, xinfo, ngcbuild, ngc2edif and more 

#if [ -d "$XPATH" ] ; then
#    #PATH="$PATH:$XPATH:$EDKPATH"
#    export PATH="$PATH:$XPATH"
#fi

function setvivado () {
   # Setup Xilinx environment 

   #hls=`ls /opt/Xilinx/Vivado|head -n 1`
   #xs_rev=${1:-$hls}
   xs_rev=${1:-$Vrev}
   echo xs_rev=$xs_rev

   XILINX_ENV_SCRIPT=/opt/Xilinx/Vivado/$xs_rev/settings64.sh
   if [[ $PATH != *"$xs_rev"* ]] && [ -f $XILINX_ENV_SCRIPT ] ; then
      source $XILINX_ENV_SCRIPT &> /dev/null          # Note: this Xilinx script dorks with $LD_LIBRARY_PATH
      export XILINXD_LICENSE_FILE=1234@lucifer
   fi
   alias vivado_b="vivado &> /dev/null &"
}

#---------------------------------------------------------------------


#---------------------------------------------------------------------
#        Ettus and GnuRadio Stuff
#---------------------------------------------------------------------
addpath "/opt/uhd/bin"

if command -v usrp_x3xx_fpga_burner &>/dev/null; then
   alias fpga_burner=usrp_x3xx_fpga_burner 
fi

#---------------------------------------------------------------------

if [ -d /usr/local/lib/python2.7/dist-packages ] ; then
   alias setgnr="export PYTHONPATH=/usr/local/lib/python2.7/dist-packages"
fi



# TODO convert oo to a function, so that & can be added after the filename
# TODO define open based on OS detection, then use it for all oo needs??
if [ -e "/usr/bin/xdg-open" -a -e "/usr/bin/exo-open" ] ; then
   alias open="/usr/bin/xdg-open"
   alias oo="open"
elif [ -e "/usr/bin/gnome-open" ] ; then
   alias open="/usr/bin/gnome-open"
   alias oo="open"
elif [ -e "/Applications/OpenOffice.app/" ]  ; then
   #alias oo="/Applications/OpenOffice.app/"
   # on the mac, we don't need to call the actual open office application,
   # we can use the open command to figure it out.
   alias oo="open"
fi
if command -v libreoffice &>/dev/null; then
   alias lo="libreoffice"
fi

#if [ -e "/opt/openoffice4/program/soffice" ] ; then
#   alias oo="/opt/openoffice4/program/soffice"
#elif [ -e "/usr/bin/soffice" ] ; then
#   alias oo="/usr/bin/soffice"
#elif [ -e "/Applications/OpenOffice.app/" ]  ; then
#   #alias oo="/Applications/OpenOffice.app/"
#   # on the mac, we don't need to call the actual open office application,
#   # we can use the open command to figure it out.
#   alias oo="open"
#fi
#if command -v libreoffice &>/dev/null; then
#   alias lo="libreoffice"
#fi

if command -v firestarter &>/dev/null; then
   alias sfsb="sudo firestarter &> /dev/null &" # send stderr and stdout to /dev/null, and put in the background
   alias sfs="sudo firestarter"
   alias sfsp="sudo firestarter --stop"
   alias sfss="sudo firestarter --start"
fi

#if command -v "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport" 2>/dev/null; then
#   alias airport="/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport"
#fi 
if command -v xload &>/dev/null; then
   alias xload="nohup xload -update 1 -scale 4 -hl red &"
fi

# Choose one of the programs below for making header files from .c files
alias mh="/opt/makeheaders/makeheaders"
#alias mh="/opt/cproto-4.7l/cproto"

alias dsf="date +%Y.%m.%d-%H:%M:%S"
alias vdah="gvim ~/Dropbox/auth-home.bfa"
alias vdaw="gvim ~/Dropbox/auth-work.bfa"
alias vdurl="gvim ~/Dropbox/url.txt"
alias vdtask="gvim ~/Dropbox/tasks.txt"
alias vds="if [ -s Session.vims ] ; then gvim -S Session.vims; else gvim -S Session.vim; fi"
alias vdsh="gvim ~/.bashrc"

if command -v gnome-activity-journal &>/dev/null; then
   alias journal="gnome-activity-journal"
fi
if command -v activity-log-manager &>/dev/null; then
   alias alm="activity-log-manager"
fi

etcher_dir="/opt/etcher-cli/"
addpath "$etcher_dir"
#if command -v "$etcher_dir/etcher" &>/dev/null; then
#   alias etcher="$etcher_dir/etcher"
#fi

# TODO: this doesn't seem to work
#if command -v /usr/share/autojump/autojump.sh &>/dev/null; then
#   . /usr/share/autojump/autojump.sh
#fi

function addurl () {
   urlf=${1:-"$HOME/Dropbox/url.txt"}
   tmpf=/tmp/addurl.txt

   #if command -v xclip &>/dev/null; then
   #alias addurl="xclip -o ~/Dropbox/url.txt"

   echo "URLs" > $tmpf
   echo "   - `xclip -selection clipboard -o`" >> $tmpf
   grep -v "^URLs" $urlf >> $tmpf

   mv $tmpf $urlf

   echo ""
   head $urlf
}

# Add a comand (lsd), which is similar to 'ls -l', but shows the size of the contents of a directory,
# rather than just 0 or the size of the inode for the directory.
function lsd () {
ls -l | gawk '
   substr($1,1,1)=="d"{
      ("du -bs " $NF) | getline size
      split(size,size_)
      sub($5,size_[1],$5)
   }
   { printf "%s %2s %s %s %10s %s %s %s %s\n",$1,$2,$3,$4,$5,$6,$7,$8,$9}'
}

if [[ "$unamestr" == 'Darwin' ]]; then
   function ltt () {
      ls -lrthG $1 | tail
   }
else
   function ltt () {
      ls -lrthG --color=always $1 | tail
   }
fi

function lf() {
   # Last file examples:
   # - Assuming the last modified file is a directory, list it in long format: lf ls -l
   # - Assuming the last modified file is a directory, list it in long format, reverse time sort: lf ls -lrt
   # - Just copy the last file to the clipboard: lf
   var_last_fname=$(unset CLICOLOR_FORCE; command ls -t | head -n 1|tr -d '\n')
   echo "var_last_fname: $var_last_fname"
   #echo "\$1: $1"

   if [ -z "$1" ]; then
      echo -n "$var_last_fname" | /usr/bin/xclip -i -selection clipboard
      echo "$var_last_fname copied to clipboard"
   else
   echo "$1 $var_last_fname ${@:2}"
   "$1" $var_last_fname "${@:2}"
   fi
}

#function myfunc()
#{
#    local  __resultvar=`ls -t | head -n 1`
#    local  myresult='some value'
#    if [[ "$__resultvar" ]]; then
#        eval $__resultvar="'$myresult'"
#    else
#        echo "$myresult"
#    fi
#}

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
   export LS_COLORS=`echo $LS_COLORS|sed 's/ow=34\;42:/ow=01\;34:/'`
   export CLICOLOR_FORCE=1
elif [[ "$unamestr" == 'FreeBSD' ]]; then
   export LS_COLORS=`echo $LS_COLORS|sed 's/ow=34\;42:/ow=01\;34:/'`
elif [[ "$unamestr" == 'Darwin' ]]; then
   export CLICOLOR=1
   export CLICOLOR_FORCE=1
   export LSCOLORS=GxFxCxDxBxegedabagaced
fi
export GREP_COLORS='ms=01;31:mc=01;31:sl=:cx=:fn=35:ln=32:bn=32:se=36'
export HISTTIMEFORMAT='%F %T '



######################################################
# ssh stuff for Lucifer
# From: http://askubuntu.com/questions/362280/enter-ssh-passphrase-once
#

#SSH_ENV=$HOME/.ssh/environment
#
## start the ssh-agent
#function start_agent {
#   echo "Initializing new SSH agent..."
#   # spawn ssh-agent
#   /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
#   echo succeeded
#   chmod 600 "${SSH_ENV}"
#   . "${SSH_ENV}" > /dev/null
#   /usr/bin/ssh-add
#}
#
#if [ -f "${SSH_ENV}" ]; then
#   . "${SSH_ENV}" > /dev/null
#   ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
#      start_agent;
#   }
#else
#   start_agent;
#fi

# Alternatively, this might work:
# eval `gnome-keyring-daemon --start`
#
######################################################


if command -v nautilus &>/dev/null; then
   # Gnome Desktop
   alias finder="`command -v nautilus` &>/dev/null &"
elif command -v thunar &>/dev/null; then
   # XFCE Desktop
   alias finder="`command -v thunar` &>/dev/null &"
elif command -v finder &>/dev/null; then
   # TODO: Darwin
   alias finder=`command -v finder`
fi





######################################################
function bgp() {
# background a new preocess:
#  1. start a process, with all of its arguments ( $@ )
#  2. pipe both stdout and stderr to /dev/null, ( &> /dev/null )
#  3. background the process                    ( & )
   $@ &> /dev/null &
}




######################################################
function md()
# markdown processing into HTML
#
# This function looks for two possible css files to use with pandoc to create
# an HTML file from a markdown file
#
# The css files can be found at:
# - https://gist.github.com/ryangray/1882525
# - https://gist.github.com/Dashed/6714393
#
# Example: md README.md
######################################################
{
   filename=$(basename "$1")
   extension="${filename##*.}"
   base="${filename%.*}"
   #echo "arg 1 = $1"
   #if [ -f $1 ] ; then
   #   echo "found $1"
   #fi
   #echo "# = $#"
   #outfile="/tmp/mdv.html"
   #outfile="`basename $1 .md`.html"
   outfile=$base.html

   if [ "$extension" = "md" ]; then
      if [ -f buttondown.css ] ; then
         css=buttondown.css
         css="--css $css"
      elif [ -f ~/buttondown.css ] ; then
         css=~/buttondown.css
         css="--css $css"
      elif [ -f ~/github-pandoc.css ] ; then
         css=~/github-pandoc.css
         css="--css $css"
      fi
      #echo "css = $css"

      if command -v pandoc &>/dev/null; then
         echo pandoc -f markdown_github $css $1 -o $outfile
         pandoc -f markdown_github $css $1 -o $outfile
      elif command -v markdown &>/dev/null; then
         markdown $1 > $outfile
      else
         echo "mdv(): can't find pandoc or markdown commands"
      fi
   elif [ "$extension" = "rst" ]; then
      if command -v pandoc &>/dev/null; then
         echo pandoc -f rst $1 -o $outfile
         pandoc -f rst $1 -o $outfile
      fi
   else
      echo "Unknown input document type. Not processing it."
   fi
}

######################################################
function mdv()
# markdown view: convert a markdown file to HTML, and view in firefox
# Example: mdv README.md
######################################################
{
   #outfile="/tmp/mdv.html"
   outfile="`basename $1 .md`.html"
   md $1 
   if [[ "$unamestr" == 'Darwin' ]]; then
      open $outfile &
   else
      firefox $outfile &
   fi
   #if [[ "$unamestr" == 'Linux' ]]; then
   #elif [[ "$unamestr" == 'FreeBSD' ]]; then
}

######################################################
cl()
# cat last: find the most recent file and cat it.
# Example: cl "log/*.log"
# Example: cl # everything in the current directory
######################################################
{
   #echo "arg 1 = $1"
   #echo "# = $#"
   if [ $# -ne 1 ] ; then
      files="."
   else
      files=$1
   fi
   #echo "files = $files"
   fname=`ls -rt $files|tail -n 1`
   if [ -d "$files" ] ; then
      #echo "found dir"
      fname=$files/$fname
   fi
   echo "showing last file: $fname"
   cat $fname
}

######################################################
cp_p()
# This function displays a progress bar (similar to wget)
# when copying files. Use similar to cp. Taken from:
# https://chris-lamb.co.uk/2008/01/24/can-you-get-cp-to-give-a-progress-bar-like-wget/
######################################################
{
   #if command -v bar &>/dev/null; then
   #elif command -v pv &>/dev/null; then
   #else
   #fi

   #strace -q -ewrite cp -- "${1}" "${2}" 2>&1 \
   strace -q -ewrite cp -a -- "${1}" "${2}" 2>&1 \
      | awk '{
   count += $NF
   if (count % 10 == 0) {
      percent = count / total_size * 100
      printf "%3d%% [", percent
      for (i=0;i<=percent;i++)
         printf "="
         printf ">"
         for (i=percent;i<100;i++)
            printf " "
            printf "]\r"
         }
      }
      END { print "" }' total_size=$(stat -c '%s' "${1}") count=0
}

if command -v port &>/dev/null; then
   # ------------- MACPORTS -------------------
   # TODO: add pkga, if there even is such an equivalent in macports
   alias pkgi="sudo port install"
   alias pkgs="port search"
   alias pkgS="port info"
   alias pkgu="sudo port selfupdate"      
   alias pkgg="sudo port upgrade outdated" 
   alias pkgd="port uninstall"
   alias pkg="sudo port"
elif command -v aptitude &>/dev/null; then
   # ------------- APTITUDE -------------------
   alias pkga="sudo add-apt-repository"
   alias pkgi="sudo aptitude install"
   alias pkgs="aptitude search"
   alias pkgS="aptitude show"
   alias pkgu="sudo aptitude update"
   alias pkgg="sudo aptitude safe-upgrade"
   alias pkgd="sudo aptitude purge"
   alias pkg="sudo aptitude"
elif command -v apt-get &>/dev/null; then
   # ------------- APT-GET -------------------
   alias pkga="sudo add-apt-repository"
   alias pkgi="sudo apt-get install"
   alias pkgs="apt-cache search"
   alias pkgS="apt-cache show"
   alias pkgu="sudo apt-get update"
   alias pkgg="sudo apt-get upgrade"
   alias pkgd="sudo apt-get purge"
   alias pkg="sudo apt-get"
fi

if command -v teamviewer &>/dev/null; then
   #-----------------------------------------------------
   function tmv()
   # A quick and dirty way to control the teamviewer daemon
   #
   # Examples:
   #     tmv stop
   #     tmv start
   #     tmv enable  # have the daemon start on boot
   #     tmv disable  # do NOT start on boot
   #     tmv status
   #-----------------------------------------------------
   {
      sudo systemctl $1 teamviewerd.service
   }
fi

if command -v apt-get &>/dev/null; then
   #-----------------------------------------------------
   function apt-history()
   # This function looks for install/upgrade/remove/rollback entries from dpkg (and apt-get)
   #http://redclay.altervista.org/wiki/doku.php?id=projects:old-projects
   #-----------------------------------------------------
   {
      case "$1" in
        install)
           cat /var/log/dpkg.log | grep 'install '
           ;;
        upgrade|remove)
           cat /var/log/dpkg.log | grep $1
           ;;
        rollback)
           cat /var/log/dpkg.log | grep upgrade | \
               grep "$2" -A10000000 | \
               grep "$3" -B10000000 | \
               awk '{print $4"="$5}'
           ;;
        *)
           cat /var/log/dpkg.log
           ;;
      esac
   }
fi

#-----------------------------------------------------
function locd()
# A function to search the local directory for a file, using the locate command
# Examples:
#    'locd foo*'  - finds all files in the current directory that start with foo
#    'locd ~/some/dir "*foo*"' - finds all files in ~/some/dir/ (and subdirectories) containing "foo"
#-----------------------------------------------------
{

   set -f
   echo "1 = $1"
   echo "2 = $2"
   echo "# = $#"
   if [ $# -ne 2 ]; then
      dir=`pwd`
      glob="$1"
   else 
      dir="$1"
      glob="$2"
   fi
   echo "dir = $dir"
   echo "glob = $glob"

   set +f
   locate "$dir/$glob"
}


#-----------------------------------------------------
function fdg()
# Find grep: push the results of a find through grep
# Example:
#     'fdg foo': finds foo in all files
#     'fdg "*" foo': finds foo in all files
#     'fdg ~/mydir "*" foo': finds foo in all files in directory mydir
#-----------------------------------------------------
{
   set -f

   echo "1 = $1"
   echo "2 = $2"
   echo "3 = $3"
   echo "# = $#"
   if [ "$#" -eq 1 ]; then
      dir="."
      names="*"
      what="$1"
   elif [ "$#" -eq 2 ]; then
      dir="."
      names="$1"
      what="$2"
   else
      dir="$1"
      names="$2"
      what="$3"
   fi
   echo "dir = $dir"
   echo "names = $names"
   echo "what = $what"

   set +f
   echo "command = find $dir -type f -name $names -exec grep -il $what {} +"
   find "$dir" -type f -name "$names" -exec grep -il "$what" {} +
}

#-----------------------------------------------------
function rfgrep()
# Find grep: push the results of a find through grep, for .robot (RobotFramework) files
# Example:
#     'rfgrep "close all connections"'
#     'rfgrep common "close all connections"'
#-----------------------------------------------------
{
   set -f

   #echo "1 = $1"
   #echo "2 = $2"
   #echo "# = $#"
   if [ "$#" -eq 2 ]; then
      dir="$1"
      what="$2"
   else
      dir="."
      what="$1"
   fi
   echo "dir = $dir"
   echo "what = $what"

   set +f
   echo "command = find $dir -type f -name *.robot -exec grep -i \"$what\" {} +"
   find "$dir" -type f -name "*.robot" -exec grep -i "$what" {} +
}

#-----------------------------------------------------
function cpmt()
# Copy new files (mnemonic: copy modtime)
# Example:
#   'cpn searchdir destdir days regex': 
#     finds all files from the last N_days in searchdir matching regex, and 
#     copies them to the destdir.
#  'cpn searchdir': 
#     finds all files from the last (1) day in searchdir matching *, and 
#     copies them to '.'
#-----------------------------------------------------
{
   set -f

   #echo "1 = $1"
   #echo "2 = $2"
   #echo "3 = $3"
   #echo "# = $#"
   destdir="."
   days="-1"
   regex="none"
   if [ "$#" -eq 1 ]; then
      searchdir="$1"
   elif [ "$#" -eq 2 ]; then
      searchdir="$1"
      destdir="$2"
   elif [ "$#" -eq 3 ]; then
      searchdir="$1"
      destdir="$2"
      days="-$3"
   else
      searchdir="$1"
      destdir="$2"
      days="-$3"
      regex="$4"
   fi
   echo "searchdir = $searchdir"
   echo "destdir = $destdir"
   echo "days = $days"
   echo "regex = $regex"

   set +f

   if [ "$#" -eq 4 ]; then
      files=`find $searchdir -type f -mtime $days -name "$regex"`
   else
      files=`find $searchdir -type f -mtime $days`
   fi
   echo "files = $files"

   for f in $files
   do
      #echo "command = cp -a $f $destdir"
      cp -a $f $destdir
   done
}

#-----------------------------------------------------
function dul()
# find the largest 10 directories and files of $1
# Example dul "Dropbox/*"
{
   #du -a "$1" | sort -n -r | head -n 10
   du -hsx $1 | sort -rh | head -10
}

#-----------------------------------------------------
function vdwh()
# gvim on the result of a which x
{
   gvim `which $1`
}

#-----------------------------------------------------
function llwh()
# ll on the result of a which x
{
   ll `which $1`
}

#-----------------------------------------------------
function dfst()
# Get a summary of differences in two directories
# Example dfst somedir another_dir
{
   dira=$1
   dirb=$2
   diff $dira $dirb -ur |diffstat -tb

   #files=`diff $dira $dirb -ur |diffstat -tb|tail -n +2|cut -f 4 -d ,`
   ##echo $files
   #for f in $files
   #do
   #   base=${f##*/}
   #   DIR=${f%/*}
   #   #echo $DIR, $base
   #   if [[ $dira == *"$DIR"* ]] ; then
   #      wc=`wc $dira/$base|awk '{print $1}'` 
   #      echo "$wc,$dira/$base"
   #   fi
   #   if [[ $dirb == *"$DIR"* ]] ; then
   #      #wc $dirb/$base 
   #      wc=`wc $dirb/$base|awk '{print $1}'` 
   #      echo "$wc,$dirb/$base"
   #   fi
   #done
}

#-----------------------------------------------------
psgrep ()
# ps piped into grep with some nice cleanup to show headers, and not the grep
{
   needle=$1
   #pscmd="ps axo user,pid,ppid,pcpu,pmem,vsz,rss,tty,tpgid,sess,pgrp,stat,time,comm"
   pscmd="ps aux"
   $pscmd | grep "USER\s*PID\|$needle"
}

#-----------------------------------------------------
nkill ()
{
   # nkill = name kill, which is an expanded flavor of killall. Killall only looks
   # at the name of the executable. Often things will be run as a python or other
   # script (ie "python script_name.py"), so `nkill script_name` would kill the
   # process.

   name=$1
   #echo "looking for process descriptions with $name"
   #echo found the following PIDs: `ps aux|grep "$name"|grep -v grep|awk '{print $2}'`
   ps aux|grep "$name"|grep -v grep|awk '{print $2}'|xargs kill
}


#-----------------------------------------------------
#function tab()
## Opens a new tab in the OS X terminal
#{
#  osascript 2>/dev/null <<EOF
#    tell application "System Events"
#      tell process "Terminal" to keystroke "t" using command down
#    end
#    tell application "Terminal"
#      activate
#      do script with command "cd \"$PWD\"; $*" in window 1
#    end tell
#EOF
#}

########################################
# Macports
########################################
if [ -d /opt/local/bin ] ; then
   export PATH=/opt/local/bin:/opt/local/sbin:$PATH
fi
if [ -d /opt/local/share/man ] ; then
   export MANPATH=/opt/local/share/man:$MANPATH
fi

# The line below is part of a failed attempt at getting better history storage
alias mti='history -c; history -r $HIST_NAME'

# TODO: if sourcing this .bashrc too many times (about 8 or so), then the sourcing takes a reallly long time.

# Make sure to "set modeline" in .vimrc to make the line below work. :help modeline for more info
# vim: ts=3:sw=3


