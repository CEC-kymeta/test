#!/bin/bash


# The code below removes the trailing '/' from the directory, if it exists,
# because the trailing '/' screws up the tar command.
#dir=`dirname $1`/`basename $1`
dir=${1:-"_obsolete"}
echo "dir = '$dir'"

if [ ! -d "$dir" ]; then
   echo "Could not find $dir, running mkdir"
   mkdir -p "$dir"
fi

ARCHIVE_EXCLUDES=${2:-"*.swp"}

#if [ -z "$1" ] || [ "$1" == "-h" ]; then
#   echo ''
#   echo 'usage: tardir name exclude'
#   echo '   creates name_YY-MM-DD_HH-mm-ss.tar.bz2 from name/* --exclude=exclude'
#   echo ''
#   exit
#fi
#archive_name=`date +${dir}_%Y-%m-%d_%H-%M-%S.tar.xv`
archive_name=`date +${dir}/rrd_%Y-%m-%d_%H-%M-%S.tar.bz2`

#echo "archive_name = $archive_name"
#echo "excludes = $ARCHIVE_EXCLUDES"

#files=( "kmodem_ping.rrd" "kmodem_status.rrd" "kant_ping.rrd" "kant_status.rrd" "kant_tracking.rrd" 
#       iperf.rrd latency_files.rrd psmon.rrd aws_ping.rrd "weather.rrd" )
#echo "files = ${files[@]}"

#tar cvfj $archive_name $dir/* --exclude="$ARCHIVE_EXCLUDES"
#echo "cmd: tar cvfj $archive_name ${files[@]}"
#tar cvfj $archive_name ${files[@]}

echo "cmd: tar cvfj $archive_name *.rrd"
tar cvfj $archive_name *.rrd

