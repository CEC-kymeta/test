#!/usr/bin/python
import argparse
import rrdtool
import syslog
import sys, os
import subprocess as sp

#---------------------------------------
def log_rrd(lvals):
#---------------------------------------
    if args.syslog:
        syslog.closelog()

    tmpl = 'uptime'

    vals = ':'.join(lvals)
    vals = 'N:' +str(vals)

    if args.verbose:
        print( 'tmpl = ' +tmpl)
        print( 'vals = ' +vals)
        print('rrdtool update Start')
    rrdtool.update(filename, '--template', tmpl, vals)

    if args.verbose:
        print('rrdtool update Done')



parser = argparse.ArgumentParser()
parser.add_argument('-f', '--filename', dest='filename', action='store', default="uptime.rrd",
        help='IP address of Kymeta antenna default: %(default)s')
parser.add_argument('-w', '--work_dir', dest='work_dir', action='store', default=".",
        help='Filename for tracking data: %(default)s')
parser.add_argument('-s', '--skip_syslog', dest='syslog', action='store_false', default=True,
        help='Disable logging to syslog')
#parser.add_argument('-z', '--force_zero', dest='force_zero', action='store_true', default=False,
#        help='update the RRD database with some all zero data')
parser.add_argument('-v', '--verbose', dest='verbose', action='store', default=0,
        type=int, metavar = 'N',
        help='Verbosity level. Anything other than 0 for debug info.')
parser.add_argument('-V', '--verbose_on', dest='verbose_on', action='store_true', 
        default=False,
        help='Set Verbosity level N = 1.')

args = parser.parse_args()
if args.verbose_on:
    args.verbose = max(1, args.verbose)

if args.syslog:
    syslog.openlog('uptime')

filename = os.path.join(args.work_dir, args.filename)

############### DEBUG ################
#args.verbose = 1
#args.syslog = True

if args.verbose:
    print("filename = " +filename)

out = ""
cmd = ["uptime", "-p"]
if args.verbose:
    print( "cmd = " +str(cmd) )

try:
    out = sp.check_output(cmd)
    ############### DEBUG ################
    #out = "up 2 days, 2 hours, 6 minutes"
    #out = "up 2 hours, 6 minutes"
    #out = "up 6 minutes"
    #out = "up 1 day, 23 hours"
    out = "up 1 day, 23 minutes"

except Exception as ex:
    msg = "Exception has been thrown. " + str(ex)
    if args.syslog:
        syslog.syslog( msg )
    print(msg)
    lvals = [ '0' ]

else:
    if args.verbose:
        print ("out: " +out)
    if len(out) == 0:
        uptime_val = '0'
    elif 'N/A' in out:
        uptime_val = '0'
    else:
        tsplit = out.split()
        #print "tsplit = " +str(tsplit)
        #print "len(tsplit) = " +str(len(tsplit))
        days = 0
        hours = 0
        minutes = 0
        if "day" in out:
            days = int(tsplit[1])

            if "hour" in tsplit[4]:
                hours = int(tsplit[3])
            if "minute" in tsplit[4]:
                minutes = int(tsplit[3])

            if len(tsplit) > 5:
                minutes = int(tsplit[5])

        elif "hour" in out:
            hours = int(tsplit[1])
            if len(tsplit) > 2:
                minutes = int(tsplit[3])
        else:
            minutes = int(tsplit[1])

        uptime_val  = days    * 60 * 60 * 24
        uptime_val += hours   * 60 * 60 
        uptime_val += minutes * 60 
        
    lvals = [ str(uptime_val) ]

if args.verbose:
    print( "days    = " +str(days)  )
    print( "hours   = " +str(hours) )
    print( "minutes = " +str(minutes) )
    print( "lvals = " +str(lvals) )

log_rrd(lvals)

