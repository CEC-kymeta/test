#!/bin/bash
#
# setup (add) or teardown (del) a route to a specific IP address
outdir=${1:-"latency_files"}
#ip=${2:-"54.186.205.190"}
#gw=${3:-"192.168.44.1"}
#dev=${4:-"eth0:4"}
#workdir="."

mkdir -p "$outdir"
#sizes=( 1k 10k 100k 1M 10M 100M )
sizes=( 1k 10k 100k 1M )
for s in "${sizes[@]}" ; do
    head -c $s </dev/urandom > "$outdir/$s.dat"
done

ls -lh "$outdir"
