#!/bin/bash
#
## change directory to the rrdtool script dir
#DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages
#workdir=${2:-'.'} 

DEBUG=0
workdir='.'
timescale="minute"

while getopts "hVt:w:" arg; do
  case $arg in
    h)
      echo "Usage: graph_rrd.sh [-V] [-w workdir] [-t minute|hour|month]"
      exit
      ;;
    w)
      workdir=$OPTARG
      ;;
    t)
      timescale=$OPTARG
      ;;
    V)
      DEBUG=1
      ;;
  esac
done


workdir=$(readlink -m "$PWD/$workdir")
source "rrd_vars.sh"


if [ $DEBUG -ne 0 ]; then
   echo "workdir = $workdir"
   echo "timescale = $timescale"
fi

cd "$workdir"

for f in ${rrd_list[@]}
do
   if [ -f "$f" ]; then
      echo "dumping $f to ${f}.xml"
      result=$($rrdtool dump "$f" > "${f}.xml")
   fi
done
archive_name=`date +rrd_dump_%Y-%m-%d_%H-%M-%S.tgz`
tar cvfz "$workdir/$archive_name" $workdir/*rrd.xml
