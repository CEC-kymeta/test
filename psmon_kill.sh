#!/bin/bash
#
### set the paths
workdir="."
source "$workdir/rrd_vars.sh"

pkillcmd="/usr/bin/pkill"
 
DEBUG=${1:-0}     # Use anything other than 0 as a commandline argument to create more debug messages

nkill ()
{
    # nkill = name kill, which is an expanded flavor of killall. Killall only looks
    # at the name of the executable. Often things will be run as a python or other
    # script (ie "python script_name.py"), so `nkill script_name` would kill the
    # process.

    name=$1
    procs=$(ps aux|grep "$name"|grep -v grep|awk '{print $2}')
    if [ $DEBUG -ne 0 ]; then
        echo "looking for process descriptions with $name"
        echo "found the following PIDs: $procs"
    fi
    if [ ! -z $procs ] ; then
        for p in ${procs[@]}; do
            kill $p
        done
    fi
}
 
#plist=( "chromedriver" "cromium-browse" "Xvfb" )
#for name in "${plist[@]}" do
#    nkill $name
#done

if [ $DEBUG -ne 0 ]; then
    $pkillcmd -c chromedriver
    $pkillcmd -c chromium-browse
    $pkillcmd -c Xvfb
    $pkillcmd -c firefox
    #$pkillcmd -c firefox-esr
else
    $pkillcmd chromedriver
    $pkillcmd chromium-browse
    $pkillcmd Xvfb
    $pkillcmd firefox
    #$pkillcmd firefox-esr
fi
nkill update_rrd.sh
nkill update_iperf.py
$pkillcmd iperf
nkill update_latency_files.py
nkill modem_temp_selenium.py
nkill modem_temp_germanium.py
nkill ant_requests.py
nkill weather.py

#vals="$driver_n:$chrome_n:$xvfb_n"
#if [ $DEBUG -ne 0 ]; then
#    echo "vals = $vals"
#    echo "cmd = $rrdtool update $workdir/psmon.rrd --template driver:chrome:xvfb N:$vals"
#fi
#
#
#$rrdtool update $workdir/psmon.rrd --template driver:chrome:xvfb N:$vals
 
#TODO: rm -rf /tmp/.org.chromium.Chromium.*
#TODO: rm -rf /tmp/.X*-lock
